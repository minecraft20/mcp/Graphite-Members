package net.minecraft.util;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.GZIPInputStream;

import javax.imageio.ImageIO;

import com.google.gson.*;
import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.event.ClickEvent;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class ScreenShotHelper extends Thread
{
    private static final Logger logger = LogManager.getLogger();
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");

    /** A buffer to hold pixel values returned by OpenGL. */
    private static IntBuffer pixelBuffer;

    /**
     * The built-up array that contains all the pixel values returned by OpenGL.
     */
    private static int[] pixelValues;

    /**
     * Saves a screenshot in the game directory with a time-stamped filename.  Args: gameDirectory,
     * requestedWidthInPixels, requestedHeightInPixels, frameBuffer
     */
    public static IChatComponent saveScreenshot(File p_148260_0_, int p_148260_1_, int p_148260_2_, Framebuffer p_148260_3_)
    {
        return saveScreenshot(p_148260_0_, (String)null, p_148260_1_, p_148260_2_, p_148260_3_);
    }

    /**
     * Saves a screenshot in the game directory with the given file name (or null to generate a time-stamped name).
     * Args: gameDirectory, fileName, requestedWidthInPixels, requestedHeightInPixels, frameBuffer
     */
    public static IChatComponent saveScreenshot(File p_148259_0_, String p_148259_1_, int p_148259_2_, int p_148259_3_, Framebuffer p_148259_4_)
    {
        try
        {
            File var5 = new File(p_148259_0_, "screenshots");
            var5.mkdir();

            if (OpenGlHelper.isFramebufferEnabled())
            {
                p_148259_2_ = p_148259_4_.framebufferTextureWidth;
                p_148259_3_ = p_148259_4_.framebufferTextureHeight;
            }

            int var6 = p_148259_2_ * p_148259_3_;

            if (pixelBuffer == null || pixelBuffer.capacity() < var6)
            {
                pixelBuffer = BufferUtils.createIntBuffer(var6);
                pixelValues = new int[var6];
            }

            GL11.glPixelStorei(GL11.GL_PACK_ALIGNMENT, 1);
            GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);
            pixelBuffer.clear();

            if (OpenGlHelper.isFramebufferEnabled())
            {
                GlStateManager.bindTexture(p_148259_4_.framebufferTexture);
                GL11.glGetTexImage(GL11.GL_TEXTURE_2D, 0, GL12.GL_BGRA, GL12.GL_UNSIGNED_INT_8_8_8_8_REV, pixelBuffer);
            }
            else
            {
                GL11.glReadPixels(0, 0, p_148259_2_, p_148259_3_, GL12.GL_BGRA, GL12.GL_UNSIGNED_INT_8_8_8_8_REV, pixelBuffer);
            }

            pixelBuffer.get(pixelValues);
            TextureUtil.func_147953_a(pixelValues, p_148259_2_, p_148259_3_);
            BufferedImage var7 = null;

            if (OpenGlHelper.isFramebufferEnabled())
            {
                var7 = new BufferedImage(p_148259_4_.framebufferWidth, p_148259_4_.framebufferHeight, 1);
                int var8 = p_148259_4_.framebufferTextureHeight - p_148259_4_.framebufferHeight;

                for (int var9 = var8; var9 < p_148259_4_.framebufferTextureHeight; ++var9)
                {
                    for (int var10 = 0; var10 < p_148259_4_.framebufferWidth; ++var10)
                    {
                        var7.setRGB(var10, var9 - var8, pixelValues[var9 * p_148259_4_.framebufferTextureWidth + var10]);
                    }
                }
            }
            else
            {
                var7 = new BufferedImage(p_148259_2_, p_148259_3_, 1);
                var7.setRGB(0, 0, p_148259_2_, p_148259_3_, pixelValues, 0, p_148259_2_);
            }

            File var12;

            if (p_148259_1_ == null)
            {
                var12 = getTimestampedPNGFileForDirectory(var5);
            }
            else
            {
                var12 = new File(var5, p_148259_1_);
            }

            //            ImageIO.write(var7, "png", var12);
            GL11.glReadBuffer(GL11.GL_FRONT);
            ByteBuffer buffer = BufferUtils.createByteBuffer(p_148259_2_ * p_148259_3_ * 4);
            GL11.glReadPixels(0, 0, p_148259_2_, p_148259_3_, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer );
            (new ScreenShotHelper(buffer,p_148259_2_,p_148259_3_,var12)).start();
            return new ChatBuilder()
                    .appendPrefix()
                    .appendText("Saved screenshot.").getMessage();
        }
        catch (Exception var11)
        {
            logger.warn("Couldn\'t save screenshot", var11);
            return new ChatBuilder().appendPrefix()
                    .appendText("Could not save screenshot.").getMessage();
        }
    }

    /**
     * Creates a unique PNG file in the given directory named by a timestamp.  Handles cases where the timestamp alone
     * is not enough to create a uniquely named file, though it still might suffer from an unlikely race condition where
     * the filename was unique when this method was called, but another process or thread created a file at the same
     * path immediately after this method returned.
     */
    private static File getTimestampedPNGFileForDirectory(File p_74290_0_)
    {
        String var2 = dateFormat.format(new Date()).toString();
        int var3 = 1;

        while (true)
        {
            File var1 = new File(p_74290_0_, var2 + (var3 == 1 ? "" : "_" + var3) + ".png");

            if (!var1.exists())
            {
                return var1;
            }

            ++var3;
        }
    }

    public void uploadImage(File file) {
        try {
            new ChatBuilder().appendPrefix()
                    .appendText("Uploading to ")
                    .appendText("my.mixtape.moe", ChatColor.GRAY)
                    .appendText("...").send();
            BufferedImage image = ImageIO.read(new File(file.getAbsolutePath()));
            ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
            ImageIO.write(image, "png", byteArray);
//            byte[] fileByes = byteArray.toByteArray();
//            String base64File = Base64.encodeBase64String(fileByes);
//            String postData = URLEncoder.encode("image", "UTF-8") + "=" + URLEncoder.encode(base64File, "UTF-8");
//
//            URL imgurApi = new URL("https://api.imgur.com/3/image");
//            HttpURLConnection connection = (HttpURLConnection) imgurApi.openConnection();
//            connection.setDoOutput(true);
//            connection.setDoInput(true);
//            connection.setRequestMethod("POST");
//            connection.setRequestProperty("Authorization", "Client-ID 9199fdef135c122");
//            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//            connection.connect();
//
//            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
//            outputStreamWriter.write(postData);
//            outputStreamWriter.flush();
//            outputStreamWriter.close();
//
//            StringBuilder stringBuilder = new StringBuilder();
//            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//            String line;
//            while ((line = rd.readLine()) != null) {
//                stringBuilder.append(line).append(System.lineSeparator());
//            }
//            rd.close();
//
//            Gson gson = new GsonBuilder().setPrettyPrinting().create();
//            JsonObject json = gson.fromJson(stringBuilder.toString(), JsonObject.class);

            String url = new MixtapeHelperIPastedOffInternet().upload(image);

//            String url = "http://i.imgur.com/" + json.get("data").getAsJsonObject().get("id").getAsString() + ".png";

            new ChatBuilder()
                    .appendPrefix()
                    .appendText("Uploaded to ")
                    .appendText(url, ChatColor.GRAY, new ClickEvent(ClickEvent.Action.OPEN_URL, url))
                    .appendText(".")
                    .send();


        } catch (IOException e) {
            new ChatBuilder()
                    .appendPrefix()
                    .appendText("Unable to upload screenshot.").send();
            e.printStackTrace();
        }
    }

    private ByteBuffer buffer;
    private int width;
    private int height;
    private File file;

    private ScreenShotHelper(ByteBuffer buffer, int width, int height, File file)
    {
        this.buffer = buffer;
        this.width = width;
        this.height = height;
        this.file = file;
    }

    public void run()
    {
        BufferedImage bufferedimage = new BufferedImage(width,height, BufferedImage.TYPE_INT_RGB);
        int[] bArray = ((DataBufferInt)bufferedimage.getRaster().getDataBuffer()).getData();
        for(int x = 0; x < width; x++)
            for(int y = 0; y < height; y++)
            {
                int color = buffer.getInt((x+(width*y)) * 4);
                color = ((color & 0xFF00) + ((color & 0xFF) << 16) + ((color & 0xFF0000) >> 16)); //Herp derp Java images are stored BGR internally.
                bArray[x+(width*(height-y-1))] = color;
            }
        try {
            ImageIO.write(bufferedimage, "png", file);
            this.uploadImage(file);
        } catch (IOException ex) {}
    }

    class MixtapeHelperIPastedOffInternet {

        private final String MIXTAPE_POST_URI = "https://mixtape.moe/upload.php";
        private final String tmpfiletype = "image/png";
        private String filename;
        private String extension;
        private final String boundary = "----";

        public String upload(BufferedImage imgToUpload) throws IOException{
            String fullname = "screenshot.png";
            extension = FilenameUtils.getExtension(fullname);
            filename = FilenameUtils.getBaseName(fullname);
            HttpURLConnection connection = connect();
            send(imageToBytes(imgToUpload),connection);
            String response = getResponse(connection);
            return parseResponse(response);
        }

        private HttpURLConnection connect() throws IOException{
            HttpURLConnection conn = null;
            URL url = null;
            try {
                url = new URL(MIXTAPE_POST_URI);
            } catch (MalformedURLException ex) {
                return null;
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
            } catch (IOException ex) {
                throw ex;
            }
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            try {
                conn.setRequestMethod("POST");
            } catch (ProtocolException ex) {
                throw ex;
            }
            conn.setRequestProperty("Connection", "keep-alive");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36");
            conn.setRequestProperty("Content-Type","multipart/form-data; boundary=----" + boundary);
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            return conn;
        }

        private void send(byte[] b, HttpURLConnection conn) throws IOException{
            String introline = "------"+boundary;
            String padder = String.format("Content-Disposition: form-data; name=\"files[]\"; filename=\"" + filename + "." + extension +"\"\r\nContent-type: " + tmpfiletype + "\r\n");
            String outroline = "------"+boundary+"--";

            ByteArrayInputStream bais = new ByteArrayInputStream(b);
            DataOutputStream outstream;
            try {
                outstream = new DataOutputStream(conn.getOutputStream());
                outstream.writeBytes(introline);
                outstream.writeBytes("\r\n");
                outstream.writeBytes(padder);
                outstream.writeBytes("\r\n");

                int i;
                while ((i = bais.read()) > -1){
                    outstream.write(i);

                }
                bais.close();

                outstream.writeBytes("\r\n");
                outstream.writeBytes("\r\n");
                outstream.writeBytes(outroline);
                outstream.flush();
                outstream.close();
            }catch(IOException ex){}
        }

        private String getResponse(HttpURLConnection conn) throws IOException{
            String response = "No response received, or something has gone wrong.";
            String charset = "UTF-8";
            InputStream gzippedResponse = conn.getInputStream();
            InputStream ungzippedResponse = new GZIPInputStream(gzippedResponse);
            Reader reader = new InputStreamReader(ungzippedResponse, charset);
            StringWriter writer = new StringWriter();
            char[] buffer = new char[10240];
            for (int length = 0; (length = reader.read(buffer)) > 0;) {
                writer.write(buffer, 0, length);
            }
            response = writer.toString();
            writer.close();
            reader.close();
            reader.close();
            return response;
        }


        /**
         * Parse the response to get the image link.
         * @param response the image link resulting from the upload
         */
        private String parseResponse(String response){
            JsonObject jsonObject = (JsonObject) new JsonParser().parse(response);
            return jsonObject.get("files").getAsJsonArray().get(0).getAsJsonObject().get("url").getAsString();
        }
        /**
         * Create a byte array from the dropped or pasted image.
         * @param img   the image to be converted to byte array
         * @return the resulting byte array
         */
        private byte[] imageToBytes(BufferedImage img){
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                ImageIO.write( img, "jpg", baos );
                baos.flush();
                byte[] imageInByte = baos.toByteArray();
                baos.close();
                return imageInByte;
            } catch (IOException ex) {}
            return null;
        }

        /**
         * Convert the file to byte array.
         * This could be done in the GUI though, so the uploader can just have ONE upload method,
         * which takes a byte array, instead of having 2 methods , one of which takes a file, the other a byte array.
         * Improvements needed.
         * @param f the file to be written to a byte array
         * @return byte array containing file's data
         * @throws java.io.FileNotFoundException if file is not found while writing to bytes
         */
        private byte[] fileToBytes(File f) throws FileNotFoundException, IOException{
            byte[] filebytes;
            try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                BufferedInputStream in = new BufferedInputStream(new FileInputStream(f));
                int read;
                byte[] buff = new byte[2048];
                while ((read = in.read(buff)) > 0){
                    out.write(buff, 0, read);
                }   filebytes = out.toByteArray();
                out.flush();
            }
            return filebytes;
        }

    }

}
