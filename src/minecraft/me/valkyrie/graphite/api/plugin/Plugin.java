package me.valkyrie.graphite.api.plugin;

import me.valkyrie.graphite.api.command.Command;
import me.valkyrie.graphite.api.module.Module;

import java.util.Arrays;

/**
 * Class used for the object representation of a plugin.
 *
 * @author Zeb.
 * @since 4/27/2017
 */
public abstract class Plugin {

    /**
     * The plugin's manifest.
     */
    protected PluginManifest pluginManifest;

    /**
     * If the plugin is running.
     */
    protected boolean running;

    public Plugin(){
        // Checks for plugin manifest.
        if(!getClass().isAnnotationPresent(PluginManifest.class)){
            // Prints error for developer.
            System.err.printf("Plugin class \"%s\" does not have a plugin manifest.", getClass().getSimpleName());

            return;
        }

        // Gets the plugin's manifest.
        pluginManifest = getClass().getAnnotation(PluginManifest.class);
    }

    /**
     * Creates all of the plugin's modules to be registered.
     *
     * @return all of the plugin's modules.
     */
    public Module[] formModules(){
        // The empty array of the modules.
        Module[] modules = new Module[pluginManifest.modules().length];

        // Iterates though all of the module classes.
        for(int i = 0; i < modules.length; i++){
            // Tries to create the modules.
            try{
                // Creates the module and put's it in the array.
                modules[i] = pluginManifest.modules()[i].newInstance();
            } catch(Exception ignored){
            }
        }

        // Returns the modules.
        return modules;
    }

    /**
     * Creates all of the plugin's commands.
     *
     * @return all of the plugin's commands.
     */
    public Command[] formCommands(){
        // The empty array of the commands.
        Command[] commands = new Command[pluginManifest.commands().length];

        // Iterates though all of the command classes.
        for(int i = 0; i < commands.length; i++){
            // Tries to create the modules.
            try{
                // Creates the command and put's it in the array.
                commands[i] = pluginManifest.commands()[i].newInstance();
            } catch(Exception ignored){
            }
        }

        // Returns the commands.
        return commands;
    }

    /**
     * @return the plugin's label.
     */
    public String getLabel(){
        return pluginManifest.label();
    }

    /**
     * Sets the plugin's running state.
     *
     * @param running the new plugin state.
     */
    public void setRunning(boolean running){
        this.running = running;

        if(running)
            enable();
        else
            disable();
    }

    /**
     * @return if the plugin is running.
     */
    public boolean isRunning(){
        return running;
    }

    /**
     * Called when the plugin is enabled.
     */
    public void enable(){
    }

    /**
     * Called when the plugin is disabled.
     */
    public void disable(){
    }

}
