package me.valkyrie.graphite.api.plugin;

import me.valkyrie.graphite.api.command.Command;
import me.valkyrie.graphite.api.module.Module;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to handle plugin information.
 *
 * @author Zeb.
 * @since 4/27/2017
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PluginManifest {

    /**
     * @return the plugin's name.
     */
    String label();

    /**
     * @return the plugin's version. (Please use "major.minor.patch" format.)
     */
    String version();

    /**
     * @return the plugin's author.
     */
    String author();

    /**
     * @return all of the plugin's modules.
     */
    Class<? extends Module>[] modules() default {};

    /**
     * @return all of the plugin's commands.
     */
    Class<? extends Command>[] commands() default {};

}
