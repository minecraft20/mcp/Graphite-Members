package me.valkyrie.graphite.api.plugin;

import com.google.gson.JsonObject;
import me.valkyrie.graphite.api.util.Jsonable;

/**
 * SHIT.
 *
 * @author Zeb.
 * @since 5/2/2017
 */
public class PluginInfo implements Jsonable {

    /**
     * The main class of the plugin.
     */
    private String mainClass;

    /**
     * @return the plugin's main class.
     */
    public String getMainClass(){
        return mainClass;
    }

    /**
     * @return the object in json format.
     */
    @Override
    public JsonObject toJson(){
        return null;
    }

    /**
     * Creates the object from json.
     *
     * @param json The json data.
     */
    @Override
    public void fromJson(JsonObject json){
        mainClass = json.get("main").getAsString();
    }

}
