package me.valkyrie.graphite.api.command;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Zeb on 4/21/2017.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandManifest {

    /**
     * @return the label of the command.
     */
    String label();

    /**
     * @return the command's aliases.
     */
    String[] aliases() default {};

}
