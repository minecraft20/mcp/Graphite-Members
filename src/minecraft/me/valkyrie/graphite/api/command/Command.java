package me.valkyrie.graphite.api.command;

import java.util.Arrays;

import me.valkyrie.graphite.api.helper.IHelper;

/**
 * Created by Zeb on 4/21/2017.
 */
public abstract class Command implements IHelper {

    /**
     * The command's manifest that stores basic information.
     */
    private CommandManifest commandManifest;

    public Command(){
        // Checks for the command manifest.
        if(!getClass().isAnnotationPresent(CommandManifest.class)){
            // Prints an error to let developer the command manifest is missing.
            System.err.printf("Command class \"%s\" does not have a CommandManifest annotated.", getClass().getSimpleName());

            // Returns so no fuckery is present.
            return;
        }

        // Get's the modules manifest.
        commandManifest = getClass().getAnnotation(CommandManifest.class);
    }

    /**
     * Executes the command.
     *
     * @param arguments The command arguments.
     * @param raw       The raw command message.
     */
    public abstract void execute(String[] arguments, String raw);

    /**
     * @return the command's label.
     */
    public String getLabel(){
        return commandManifest.label();
    }

    /**
     * Checks if the given input matches the command's name or aliases.
     *
     * @param input The given name.
     * @return if the given input matches the command's name or aliases.
     */
    public boolean matches(String input){
        // Returns true if name is exact match.
        if(input.equalsIgnoreCase(getLabel())) return true;

        // Checks aliases for match.
        return Arrays.stream(commandManifest.aliases()).anyMatch(input::equalsIgnoreCase);
    }

}
