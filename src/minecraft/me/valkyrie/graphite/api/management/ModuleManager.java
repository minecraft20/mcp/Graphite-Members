package me.valkyrie.graphite.api.management;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.impl.event.game.KeyPressEvent;
import me.valkyrie.graphite.impl.module.fight.AutoHeal;
import me.valkyrie.graphite.impl.module.fight.Knockback;
import me.valkyrie.graphite.impl.module.fight.killaura.Killaura;
import me.valkyrie.graphite.impl.module.self.AutoAccept;
import me.valkyrie.graphite.impl.module.self.retard.Retard;
import me.valkyrie.graphite.impl.module.visual.Freecam;
import me.valkyrie.graphite.impl.module.visual.NameProtect;
import me.valkyrie.graphite.impl.module.visual.NoBotRender;
import me.valkyrie.graphite.impl.module.visual.esp.Esp;
import me.valkyrie.graphite.impl.module.visual.search.Search;
import me.valkyrie.graphite.impl.module.visual.storageesp.StorageEsp;
import me.valkyrie.graphite.impl.module.world.*;
import me.valkyrie.graphite.impl.module.move.*;
import me.valkyrie.graphite.impl.module.move.speed.Speed;
import me.valkyrie.graphite.impl.module.self.Antihunger;
import me.valkyrie.graphite.impl.module.self.AutoRespawn;
import me.valkyrie.graphite.impl.module.self.Autotool;
import me.valkyrie.graphite.impl.module.self.QuickMunch;
import me.valkyrie.graphite.impl.module.visual.Brightness;
import me.valkyrie.graphite.impl.module.visual.nametags.Nametags;

/**
 * @author Zeb.
 * @since 4/21/2017
 */
public class ModuleManager implements EventHandler<KeyPressEvent> {

    /**
     * The directory of the module's.
     */
    private File moduleDirectory;

    /**
     * All the module's of the client.
     */
    private final Map<String, Module> modules = new HashMap<>();

    public ModuleManager(File parentDirectory){
        // Sets the client directory.
        this.moduleDirectory = new File(parentDirectory + File.separator + "modules" + File.separator);

        // Attempts to make the client directory.
        if(!moduleDirectory.exists())
            if(!moduleDirectory.mkdirs())
                System.exit(0);

        // Registers the module manager to the event system to handle key presses.
        EventManager.INSTANCE.register(this);
    }

    /**
     * Registers all of the modules.
     */
    public void register(){
        // Adds all the modules.
        this.put(new Sprint());
        this.put(new NoSlowdown());
        this.put(new QuickMunch());
        this.put(new Parkour());
        this.put(new Nuker());
        this.put(new Autotool());
        this.put(new Waterwalk());
        this.put(new Fastbreak());
        this.put(new Autofarm());
        this.put(new AutoAccept());
        this.put(new Freecam());
        this.put(new NoBotRender());
        this.put(new Longjump());
        this.put(new StorageEsp());
        this.put(new ChestStealer());
        this.put(new Search());
        this.put(new ScreenMove());
        this.put(new AutoRespawn());
        this.put(new Esp());
        this.put(new Knockback());
        this.put(new AutoHeal());
        this.put(new Killaura());
        this.put(new ClickTeleport());
        this.put(new VanishDetector());
        this.put(new Fly());
        this.put(new Nametags());
        this.put(new Step());
        this.put(new Sneak());
        this.put(new Antihunger());
        this.put(new NameProtect());
        this.put(new Brightness());
        this.put(new Speed());
        this.put(new Dolphin());
        this.put(new Scaffold());
        this.put(new Retard());
        this.put(new Blink());
        this.put(new Safewalk());
        this.put(new Day());
    }

    // Sets up all of the module modes.
    public void setup(){
        // Registers all the module modes.
        this.modules.values().forEach(Module::setup);
    }

    /**
     * Registers the given module into the module manager.
     *
     * @param module The module to be registered.
     */
    public void put(Module module){
        this.modules.put(module.getLabel().toLowerCase().replaceAll("_", ""), module);
    }

    /**
     * @return a list of all the manager's content.
     */
    public Map<String, Module> getMap(){
        return modules;
    }

    /**
     * Finds the module with the given label.
     *
     * @param label The module label.
     * @return the found module.
     */
    public Module get(String label){
        return this.modules.get(label.toLowerCase().replaceAll("_", ""));
    }

    /**
     * Finds the module with the given class.
     *
     * @param clazz The module class.
     * @return the module found.
     */
    public Optional<Module> get(Class<?> clazz){
        return this.modules.values().stream().filter(m -> m.getClass() == clazz).findFirst();
    }

    /**
     * @return the number of running modules.
     */
    @Deprecated
    public int getActiveCount(){
        return (int) this.modules.values().stream().filter(Module::isRunning).count();
    }

    /**
     * @return the module directory.
     */
    public File getModuleDirectory(){
        return moduleDirectory;
    }

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return KeyPressEvent.class;
    }

    /**
     * Handles module toggling.
     */
    @Override
    public void handle(KeyPressEvent event){
        // Toggles all of the modules.
        getMap().values().stream().filter(module -> module.getKeyBind() == event.getKeyPressed()).forEach(Module::toggle);
    }

}
