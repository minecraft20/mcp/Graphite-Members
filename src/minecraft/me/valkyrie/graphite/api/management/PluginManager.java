package me.valkyrie.graphite.api.management;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import me.valkyrie.graphite.api.plugin.Plugin;
import me.valkyrie.graphite.api.plugin.PluginInfo;
import me.valkyrie.graphite.impl.Graphite;

import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Manager that handles the loading of plugins.
 *
 * @author Zeb.
 * @since 5/2/2017
 */
public class PluginManager {

    /**
     * The directory where plugins are stored.
     */
    private File pluginDirectory;

    /**
     * List of all the plugins.
     */
    private List<Plugin> plugins = new LinkedList<>();

    public PluginManager(File clientDirectory){

        // Defines the plugin directory.
        this.pluginDirectory = new File(clientDirectory + File.separator + "plugins" + File.separator);

        // Creates the plugin folder.
        this.pluginDirectory.mkdirs();

        // Tries to load all of the plugins.
        try{
            // Finds all of the plugins.
            findPlugins();

            // Adds all of the modules and shit.
            plugins.forEach(plugin -> {
                Arrays.stream(plugin.formModules()).forEach(module -> Graphite.INSTANCE.getModuleManager().put(module));
                Arrays.stream(plugin.formCommands()).forEach(command -> Graphite.INSTANCE.getCommandManager().getContent().add(command));
            });

            // Enables all of the plugins.
            enable();
        } catch(Exception ignored){
        }

    }

    /**
     * Finds all of the plugins inside of the plugin directory.
     *
     * @throws IOException
     */
    public void findPlugins() throws IOException{

        // Searches through all of the files.
        for(File file : pluginDirectory.listFiles()){

            // Continues if the file isn't a jar file.
            if(!file.getName().toLowerCase().endsWith(".jar")) continue;

            // Creates the jar file.
            JarFile jarFile = new JarFile(file);

            // Tries to load the jars.
            try{
                loadPlugin(file, getPluginInfo(jarFile));
            } catch(Exception exception){
                exception.printStackTrace();
            }
        }
    }

    /**
     * Gets the plugin data from the jar.
     *
     * @param jarFile The plugin jar.
     * @return the plugin info.
     * @throws IOException If the file wasn't valid.
     */
    public PluginInfo getPluginInfo(JarFile jarFile) throws IOException{

        // Gets the plugin json.
        JarEntry fileEntry = jarFile.getJarEntry("plugin.json");

        // The info of the plugin.
        PluginInfo info = new PluginInfo();

        // Parses the plugin info.
        info.fromJson(new JsonParser().parse(new InputStreamReader(jarFile.getInputStream(fileEntry))).getAsJsonObject());

        // Returns the info.
        return info;
    }

    /**
     * Loads the plugin into the manager/
     *
     * @param file       The plugin file.
     * @param pluginInfo The plugin's info.
     * @return the plugin.
     * @throws Exception Y'ALL FUCKED UP.
     */
    public Plugin loadPlugin(File file, PluginInfo pluginInfo) throws Exception{

        // Uses a classloader to load the jar.
        ClassLoader classLoader = URLClassLoader.newInstance(new URL[]{file.toURL()});

        // Creates the plugin main-class.
        Plugin plugin = (Plugin) classLoader.loadClass(pluginInfo.getMainClass()).newInstance();

        System.out.printf("Loading %s.\n", plugin.getLabel());

        // Adds the plugin.
        this.plugins.add(plugin);

        // Returns the plugin.
        return plugin;
    }

    /**
     * Enables all of the plugins.
     */
    public void enable(){
        plugins.forEach(plugin -> plugin.setRunning(true));
    }

    /**
     * Disables all of the plugins.
     */
    public void disable(){
        plugins.forEach(plugin -> plugin.setRunning(false));
    }

}
