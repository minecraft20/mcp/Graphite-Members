package me.valkyrie.graphite.api.management;

import com.google.gson.*;
import me.valkyrie.graphite.api.util.Friend;
import me.valkyrie.graphite.api.util.chat.ChatBuilder;

import java.io.*;
import java.net.URL;
import java.util.*;

/**
 * Manager used to handle friends.
 *
 * @author Zeb.
 * @since 4/25/2017
 */
public class FriendManager {

    /**
     * An instance of {@link Gson} to help with pretty printing of the json to the module files.
     */
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    /**
     * Url to fetch the uuid of the given name.
     */
    private static final String NAME = "https://api.mcuuid.com/json/uuid/%s";

    /**
     * Url to fetch the name of the given uuid.
     */
    private static final String PROFILE = "https://api.mcuuid.com/json/name/%s";

    /**
     * The file where the friends are saved.
     */
    private static File friendFile;

    /**
     * Map of all the cached uuid's.
     */
    private static final Map<String, UUID> CACHED_NAMES = new HashMap<>();

    /**
     * All of the friends in the client.
     */
    private List<Friend> friendList = new ArrayList<>();

    public FriendManager(File parentDirectory){
        // Sets the client directory.
        friendFile = new File(parentDirectory + File.separator + "friends.json");
    }

    /**
     * Sets up the friend manager.
     */
    public void setup(){
        // Tries to create the module's file.
        try{
            // Creates the module file if it doesn't exist.
            if(!friendFile.exists()){

                // Creates the module's file.
                friendFile.createNewFile();

                // Saves the module file.
                save();

                // Returns because there is no need to load.
                return;
            }

            // Loads the file.
            load();
        } catch(IOException exception){
            System.exit(0);
        }
    }

    /**
     * @return all of the friends in the manager.
     */
    public List<Friend> getFriendList(){
        return friendList;
    }

    /**
     * Adds the friend with the given uuid and name.
     *
     * @param uuid The friend's uuid.
     * @param name The friend's name.
     */
    public void add(UUID uuid, String name){
        friendList.add(new Friend(uuid, name));
        save();
    }

    /**
     * Finds the friend with the given name.
     *
     * @param name The friend's name.
     * @return the found friend.
     */
    public Optional<Friend> get(String name){
        return this.friendList.stream().filter(friend -> friend.getName().equalsIgnoreCase(name)).findFirst();
    }

    /**
     * Finds the friend with the given uuid.
     *
     * @param uuid The friend's uuid.
     * @return the found friend.
     */
    public Optional<Friend> get(UUID uuid){
        return this.friendList.stream().filter(friend -> friend.getUUID().equals(uuid)).findFirst();
    }

    /**
     * Checks if a user is a friend
     *
     * @param uuid The players UUID
     * @return if the player is a friend
     */
    public boolean isFriend(UUID uuid){
        return this.friendList.stream().anyMatch(friend -> friend.getUUID().equals(uuid));
    }

    /**
     * Saves all of the friends to the file.
     */
    public void save(){
        // Tries to write data, might fail so statement is in try statement.
        try{
            // The print writer of the file that is used to write the json data.
            PrintWriter printWriter = new PrintWriter(friendFile);

            // The json object that stores the friends.
            JsonObject jsonObject = new JsonObject();

            // Adds all of the friends to the json object.
            friendList.forEach(friend -> jsonObject.add(friend.getName(), new JsonPrimitive(friend.getUUID().toString())));

            // Prints the json into the file correctly formatted.
            printWriter.print(GSON.toJson(jsonObject));

            // Closes the writer.
            printWriter.close();
        } catch(IOException | NullPointerException ignored){
        }
    }

    /**
     * Loads all the friends in the friend file into the friend manager.
     */
    public void load(){
        // Tries to read data, might fail so statement is in try statement.
        try{
            // Parses the json read from the file.
            JsonObject jsonObject = new JsonParser().parse(new FileReader(friendFile)).getAsJsonObject();

            // A set of all of the element in the json object.
            Set<Map.Entry<String, JsonElement>> elements = jsonObject.entrySet();

            // Loads all of the friends.
            elements.forEach(entry ->
                    friendList.add(new Friend(UUID.fromString(entry.getValue().getAsString()), entry.getKey())));
        } catch(IOException ignored){
        }
    }

}