package me.valkyrie.graphite.api.management;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import me.valkyrie.graphite.api.command.Command;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.helper.IHelper;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import me.valkyrie.graphite.impl.command.*;
import me.valkyrie.graphite.impl.event.player.PlayerChatEvent;

/**
 * Manager that stores and handles commands and their execution.
 *
 * @author Zeb.
 * @since 4/21/2017
 */
public final class CommandManager implements EventHandler<PlayerChatEvent>, IHelper {

    /**
     * An instance of {@link Gson} to help with pretty printing of the json to the module files.
     */
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    /**
     * The prefix for the command manager.
     */
    private char prefix = '$';

    /**
     * All of the commands in the client.
     */
    private List<Command> content = new ArrayList<>();

    /**
     * File where command prefix is saved.
     */
    private File commandFile;

    public CommandManager(File gameDirectory){
        // Registers the command manager as a event handler.
        EventManager.INSTANCE.register(this);

        // Defines the command file.
        this.commandFile = new File(gameDirectory + File.separator + "prefix.json");

        // Registers all the commands.
        this.content.add(new Toggle());
        this.content.add(new Prefix());
        this.content.add(new Friend());
        this.content.add(new Bind());
        this.content.add(new Mods());
        this.content.add(new Reload());
        this.content.add(new Visible());
        this.content.add(new Help());
        this.content.add(new Login());

        // Tries to create the module's file.
        try{
            // Creates the module file if it doesn't exist.
            if(!commandFile.exists()){

                // Creates the module's file.
                commandFile.createNewFile();

                // Saves the module file.
                save();

                // Returns because there is no need to load.
                return;
            }

            // Loads the file.
            load();
        } catch(IOException exception){
            System.exit(0);
        }
    }

    /**
     * Executes any found command if present.
     *
     * @param message The command message.
     */
    private void publish(String message){

        // Removes the prefix from the command.
        message = message.substring(1);

        // Splits the message by white-space.
        String[] arguments = message.split("\\s+");

        // Returns if message is empty.
        if(arguments.length == 0){
            // Prints error message.
            this.clientChatMsg().appendText("Please enter a valid command.").send();
            return;
        }

        // The command found.
        Optional<Command> command = get(arguments[0]);

        // Returns if command is not present.
        if(!command.isPresent()){
            // Prints error message.
            this.clientChatMsg().appendText("Command").appendText(String.format(" '%s' ", arguments[0]), ChatColor.GRAY)
                    .appendText("is invalid.").send();

            return;
        }

        try{
            // Executes the command.
            command.get().execute(Arrays.copyOfRange(arguments, 1, arguments.length), message);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Checks the given message is a valid command.
     *
     * @param message The message to be checked for the prefix.
     * @return If the given message is a valid command statement.
     */
    public boolean valid(String message){
        // Returns if message is empty.
        if(message.isEmpty())
            return false;

        // Returns if first character in message is the prefix.
        return message.toCharArray()[0] == prefix;
    }

    /**
     * Finds the command with the given label or alias.
     *
     * @param label The command label or alias.
     * @return the found command.
     */
    public Optional<Command> get(String label){
        return this.content.stream().filter(command -> command.matches(label)).findFirst();
    }

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return PlayerChatEvent.class;
    }

    /**
     * Publishes the command if the message is a command statement.
     */
    @Override
    public void handle(PlayerChatEvent event){

        // Returns if message isn't valid.
        if(!valid(event.getMessage()))
            return;

        // Publishes the command.
        publish(event.getMessage());

        // Cancels the event.
        event.setCancelled(true);
    }

    /**
     * @return the client's prefix.
     */
    public char getPrefix(){
        return prefix;
    }

    /**
     * Sets the client's prefix.
     *
     * @param prefix The new prefix.
     */
    public void setPrefix(char prefix){
        this.prefix = prefix;
    }

    /**
     * @return the manager's commands.
     */
    public List<Command> getContent(){
        return content;
    }

    /**
     * Loads the module from its file.
     */
    private void load(){
        // Tries to read data, might fail so statement is in try statement.
        try{
            // Parses the json read from the file.
            this.prefix = new JsonParser().parse(new FileReader(commandFile)).getAsJsonObject().get("prefix").getAsCharacter();
        } catch(IOException ignored){
        }
    }

    /**
     * Saves the module.
     */
    public void save(){
        // Tries to read data, might fail so statement is in try statement.
        try{
            // The print writer of the file that is used to write the json data.
            PrintWriter printWriter = new PrintWriter(commandFile);

            // The json object that stores the prefix.
            JsonObject jsonObject = new JsonObject();

            // Stores the prefix.
            jsonObject.add("prefix", new JsonPrimitive(prefix));

            // Prints the json into the file correctly formatted.
            printWriter.print(GSON.toJson(jsonObject));

            // Closes the writer.
            printWriter.close();
        } catch(IOException | NullPointerException ignored){
        }
    }

}
