package me.valkyrie.graphite.api.util.chat;

import net.minecraft.util.EnumChatFormatting;

/**
 * Created by Zeb on 8/27/2016.
 */
public enum ChatColor {

    BLACK(EnumChatFormatting.BLACK),
    DARK_BLUE(EnumChatFormatting.DARK_BLUE),
    DARK_GREEN(EnumChatFormatting.DARK_GREEN),
    DARK_AQUA(EnumChatFormatting.DARK_AQUA),
    DARK_RED(EnumChatFormatting.DARK_RED),
    DARK_PURPLE(EnumChatFormatting.DARK_PURPLE),
    GOLD(EnumChatFormatting.GOLD),
    GRAY(EnumChatFormatting.GRAY),
    DARK_GRAY(EnumChatFormatting.DARK_GRAY),
    BLUE(EnumChatFormatting.BLUE),
    GREEN(EnumChatFormatting.GREEN),
    AQUA(EnumChatFormatting.AQUA),
    RED(EnumChatFormatting.RED),
    LIGHT_PURPLE(EnumChatFormatting.LIGHT_PURPLE),
    YELLOW(EnumChatFormatting.YELLOW),
    WHITE(EnumChatFormatting.WHITE),
    OBFUSCATED(EnumChatFormatting.OBFUSCATED),
    BOLD(EnumChatFormatting.BOLD),
    STRIKETHROUGH(EnumChatFormatting.STRIKETHROUGH),
    UNDERLINE(EnumChatFormatting.UNDERLINE),
    ITALIC(EnumChatFormatting.ITALIC),
    RESET(EnumChatFormatting.RESET);

    private EnumChatFormatting baseColor;

    ChatColor(EnumChatFormatting baseColor){
        this.baseColor = baseColor;
    }

    public EnumChatFormatting getBaseColor(){
        return baseColor;
    }

    public static ChatColor getFromCharacter(char character){
        for (ChatColor chatColor : ChatColor.values()){
            EnumChatFormatting baseColor = chatColor.getBaseColor();

            if (baseColor.getFormattingCode() == character) return chatColor;
        }
        return null;
    }
}
