package me.valkyrie.graphite.api.util.chat;

import net.minecraft.client.Minecraft;
import net.minecraft.event.ClickEvent;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.IChatComponent;

import java.util.regex.Pattern;

/**
 * Created by Zeb on 8/27/2016.
 */
public class ChatBuilder {

    /*
        Base message.
     */
    private ChatComponentText message = new ChatComponentText("");

    /**
     * Adds the client prefix to the message.
     *
     * @return ChatBuilder The current ChatBuilder.
     */
    public ChatBuilder appendPrefix(){
        appendText("Graphite", ChatColor.GRAY).appendText(" > ", ChatColor.DARK_GRAY);
        return this;
    }

    /**
     * @return the unsent message.
     */
    public ChatComponentText getMessage(){
        return message;
    }

    /**
     * @param text Text to be parsed.
     * @return ChatBuilder The ChatBuilder that is currently being used.
     * @author FishyLP
     * <p>
     * Taken from a bukkit thread. https://bukkit.org/threads/jsonbuilder.352325/
     */
    public ChatBuilder fromString(String text){
        String regex = "[&§]([a-fA-Fl-oL-O0-9])";
        text = text.replaceAll(regex, "§$1");
        if(!Pattern.compile(regex).matcher(text).find()){
            appendText(text);
            return this;
        }
        String[] words = text.split(regex);

        int index = words[0].length();
        for(String word : words){
            try{
                if(index != words[0].length()){
                    String color = "§" + text.charAt(index - 1);
                    while(color.length() != 1) color = color.substring(1).trim();
                    appendText(word, ChatColor.getFromCharacter(color.charAt(0)));
                }
            } catch(Exception e){
            }
            index += word.length() + 2;
        }
        return this;
    }

    /**
     * Adds text with color.
     *
     * @param text   Text that should be displayed.
     * @param colors Colors for the text. Example: BLUE, BOLD, UNDERLINED.
     * @return ChatBuilder The ChatBuilder that is currently being used.
     */
    public ChatBuilder appendText(String text, ChatColor... colors){
        ChatComponentText componentText = new ChatComponentText(text);

        ChatStyle chatStyle = new ChatStyle();

        for(ChatColor color : colors){
            switch(color){
                case BOLD:
                    chatStyle.setBold(true);
                    break;
                case UNDERLINE:
                    chatStyle.setUnderlined(true);
                    break;
                case ITALIC:
                    chatStyle.setItalic(true);
                    break;
                case STRIKETHROUGH:
                    chatStyle.setStrikethrough(true);
                    break;
                case OBFUSCATED:
                    chatStyle.setObfuscated(true);
                    break;
                case RESET:
                    chatStyle = new ChatStyle();
                    break;
                default:
                    chatStyle.setColor(color.getBaseColor());
            }
        }

        componentText.setChatStyle(chatStyle);

        message.appendSibling(componentText);
        return this;
    }

    /**
     * Adds text with color.
     *
     * @param text       Text that should be displayed.
     * @param colors     Colors for the text. Example: BLUE, BOLD, UNDERLINED.
     * @param clickEvent What happens what the user clicks the message.
     * @return ChatBuilder The ChatBuilder that is currently being used.
     */
    public ChatBuilder appendText(String text, ChatColor color, ClickEvent clickEvent){
        ChatComponentText componentText = new ChatComponentText(text);

        ChatStyle chatStyle = new ChatStyle();
        switch(color){
            case BOLD:
                chatStyle.setBold(true);
                break;
            case UNDERLINE:
                chatStyle.setUnderlined(true);
                break;
            case ITALIC:
                chatStyle.setItalic(true);
                break;
            case STRIKETHROUGH:
                chatStyle.setStrikethrough(true);
                break;
            case OBFUSCATED:
                chatStyle.setObfuscated(true);
                break;
            case RESET:
                chatStyle = new ChatStyle();
                break;
            default:
                chatStyle.setColor(color.getBaseColor());
        }

        chatStyle.setChatClickEvent(clickEvent);

        componentText.setChatStyle(chatStyle);

        message.appendSibling(componentText);
        return this;
    }

    public ChatBuilder appendTextF(String text, Object... formatted){
        ChatComponentText componentText = new ChatComponentText(String.format(text, formatted));
        message.appendSibling(componentText);
        return this;
    }

    /*
     * Sends the message to the player.
     */
    public void send(){
        Minecraft.getMinecraft().player.addChatMessage(message);
    }

    /**
     * Returns the message in json.
     *
     * @return String Message in json.
     */
    @Override
    public String toString(){
        return IChatComponent.Serializer.componentToJson(message);
    }

}
