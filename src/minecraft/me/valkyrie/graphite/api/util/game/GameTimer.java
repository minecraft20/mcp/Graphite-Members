package me.valkyrie.graphite.api.util.game;

/**
 * Created by Zeb on 9/22/2016.
 */
public class GameTimer {

    /**
     * The time when the timer was created.
     */
    private long now = System.currentTimeMillis();

    /**
     * If the timer has passed the delay.
     *
     * @param delay delay to be checked.
     * @return if the delay has been reached.
     */
    public boolean reached(long delay){
        return System.currentTimeMillis() - now >= delay;
    }

    /**
     * Resets the timer.
     */
    public void reset(){
        now = System.currentTimeMillis();
    }

    /**
     * Returns the time passed since reset or creation of the timer.
     *
     * @return time passed.
     */
    public long getTimePassed(){
        return System.currentTimeMillis() - now;
    }

}
