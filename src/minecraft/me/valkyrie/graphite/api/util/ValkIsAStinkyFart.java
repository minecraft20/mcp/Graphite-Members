package me.valkyrie.graphite.api.util;

import java.util.function.Predicate;

public interface ValkIsAStinkyFart {

	public static <T> Predicate<T> not(Predicate<T> t) {
	    return t.negate();
	}

}