package me.valkyrie.graphite.api.util;

import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.util.*;

/**
 * Created by Zeb on 10/30/2016.
 */
public class MathUtil {

    public static float normalizeAngle(float angle){
        float newAngle = angle;
        while(newAngle <= -180) newAngle += 360;
        while(newAngle > 180) newAngle -= 360;
        return newAngle;
    }

    /**
     * Length (angular) of a shortest way between two angles.
     * It will be in range [0, 180].
     */
    public static float getAngleDifference(float alpha, float beta){
        float phi = Math.abs(beta - alpha) % 360;       // This is either the distance or 360 - distance
        float distance = phi > 180 ? 360 - phi : phi;
        return distance;
    }

    /**
     * Ray traces to see if the position is visible from the origin.
     *
     * @param origin   The origin that the ray will be checked from.
     * @param position The position to have the visibility checked.
     * @return If there is a clear path to the position from the origin.
     */
    public static boolean rayTrace(Vec3 origin, Vec3 position){
        // Gets the difference between the vectors.
        Vec3 difference = position.subtract(origin);

        // The number of steps to check.
        int steps = MathHelper.ceiling_double_int(origin.distanceTo(position) * 100);

        // The x step.
        double x = difference.xCoord / steps;

        // The y step.
        double y = difference.yCoord / steps;

        // The z step.
        double z = difference.zCoord / steps;

        // The point used to check collision.
        Vec3 point = origin;

        // A bounding box with the corners being the origin and the position.
        AxisAlignedBB collisionChunk = new AxisAlignedBB(origin, position);

        // Iterates through all the steps
        for(int i = 0; i < steps; i++){
            // Adds the step value to the point.
            point = point.addVector(x, y, z);

            // The block position at the vector.
            BlockPos blockPosition = new BlockPos(point);

            // The block pos of the position.
            BlockPos destination = new BlockPos(position);

            // Break if its the origin block.
            if(blockPosition.getX() == destination.getX()
                    && blockPosition.getY() == destination.getY()
                    && blockPosition.getZ() == destination.getZ()) break;

            // The block state at the point.
            IBlockState blockState = Minecraft.getMinecraft().world.getBlockState(blockPosition);

            // Continue if the block is liquid or air.
            if(blockState.getBlock() instanceof BlockLiquid || blockState.getBlock() instanceof BlockAir) continue;

            // The block bounding box.
            AxisAlignedBB boundingBox = blockState.getBlock().getCollisionBoundingBox(Minecraft.getMinecraft().world, blockPosition, blockState);

            // Offsets the bounding box by the block position.
            boundingBox = boundingBox.offset(blockPosition);

            // Returns the point if the point is in a block.
            if(boundingBox.contains(point)) return true;
        }

        // Returns no point at the moment for testing purposes.
        return false;
    }

    /**
     * Ray traces to see if the position is visible from the origin.
     *
     * @param origin   The origin that the ray will be checked from.
     * @param position The position to have the visibility checked.
     * @return The position hit.
     */
    public static Vec3 rayTraceVector(Vec3 origin, Vec3 position){
        // Gets the difference between the vectors.
        Vec3 difference = position.subtract(origin);

        // The number of steps to check.
        int steps = MathHelper.ceiling_double_int(origin.distanceTo(position) * 100);

        // The x step.
        double x = difference.xCoord / steps;

        // The y step.
        double y = difference.yCoord / steps;

        // The z step.
        double z = difference.zCoord / steps;

        // The point used to check collision.
        Vec3 point = origin;

        // A bounding box with the corners being the origin and the position.
        AxisAlignedBB collisionChunk = new AxisAlignedBB(origin, position);

        // Iterates through all the steps
        for(int i = 0; i < steps; i++){
            // Adds the step value to the point.
            point = point.addVector(x, y, z);

            Minecraft.getMinecraft().world.spawnParticle(EnumParticleTypes.CRIT, point.xCoord, point.yCoord, point.zCoord, 0, 0, 0);

            // The block position at the vector.
            BlockPos blockPosition = new BlockPos(point);

            // The block pos of the position.
            BlockPos destination = new BlockPos(position);

            // Break if its the origin block.
            if(blockPosition.getX() == destination.getX()
                    && blockPosition.getY() == destination.getY()
                    && blockPosition.getZ() == destination.getZ()) break;

            // The block state at the point.
            IBlockState blockState = Minecraft.getMinecraft().world.getBlockState(blockPosition);

            // Continue if the block is liquid or air.
            if(blockState.getBlock() instanceof BlockLiquid || blockState.getBlock() instanceof BlockAir) continue;

            // The block bounding box.
            AxisAlignedBB boundingBox = blockState.getBlock().getCollisionBoundingBox(Minecraft.getMinecraft().world, blockPosition, blockState);

            // Offsets the bounding box by the block position.
            boundingBox = boundingBox.offset(blockPosition);

            // Returns the point if the point is in a block.
            if(boundingBox.contains(point)) return point;
        }

        // Returns no point at the moment for testing purposes.
        return null;
    }

}
