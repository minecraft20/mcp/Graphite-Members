package me.valkyrie.graphite.api.util;

import me.valkyrie.graphite.api.helper.ProfileHelper;
import me.valkyrie.graphite.impl.Graphite;

import java.util.UUID;

/**
 * @author Zeb.
 * @since 4/26/2017
 */
public class Friend {

    /**
     * The uuid of the friend.
     */
    private UUID uuid;

    /**
     * The name of the friend.
     */
    private String name;

    /**
     * The player's player name.
     */
    private String playerName;

    public Friend(UUID uuid, String name){
        this.uuid = uuid;
        this.name = name;
        this.playerName = name;

        // Fetches the friend's player name.
        new Thread(() -> {
            try{
                // Tries to fetch the friend's player name.
                this.playerName = ProfileHelper.INSTANCE.getName(uuid);
            } catch(Exception ignored){
            }

            // If name isn't found for some reason, it sets it to the nickname.
            if(playerName == null)
                playerName = name;
        }).start();
    }

    /**
     * @return the friend's uuid.
     */
    public UUID getUUID(){
        return uuid;
    }

    /**
     * @return the friend's name.
     */
    public String getName(){
        return name;
    }

    /**
     * @return the friend's player name.
     */
    public String getPlayerName(){
        return playerName;
    }
}
