package me.valkyrie.graphite.api.util;

import com.google.gson.JsonObject;

/**
 * @author Zeb.
 * @since 4/24/2017
 */
public interface Jsonable {

    /**
     * @return the object in json format.
     */
    JsonObject toJson();

    /**
     * Creates the object from json.
     *
     * @param json The json data.
     */
    void fromJson(JsonObject json);

}
