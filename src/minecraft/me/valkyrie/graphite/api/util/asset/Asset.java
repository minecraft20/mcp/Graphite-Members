package me.valkyrie.graphite.api.util.asset;

import java.io.InputStream;

/**
 * Created by Zeb on 12/9/2016.
 */
public class Asset {

    /**
     * The path to the asset.
     */
    private String label;

    public Asset(String label){
        this.label = label;
    }

    /**
     * Gives back the asset in the form of a input stream.
     *
     * @return The asset as an input stream.
     */
    public InputStream asInputStream(){
        if(getClass().getClassLoader().getResourceAsStream(label) != null)
            return getClass().getClassLoader().getResourceAsStream(label);
        return getClass().getResourceAsStream(label);
    }

}
