package me.valkyrie.graphite.api.util.account;

/**
 * @author Zeb.
 * @since 6/26/2017.
 */
public class Account {

    /**
     * The account username.
     */
    private String username;

    /**
     * The account email.
     */
    private String email;

    /**
     * The account password.
     */
    private String password;

    public Account(String username, String email, String password){
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public Account(String email, String password){
        this.email = email;
        this.password = password;
    }

    

}
