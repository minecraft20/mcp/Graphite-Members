package me.valkyrie.graphite.api.util.render.shader;

import me.valkyrie.graphite.api.util.asset.Asset;
import net.minecraft.client.renderer.OpenGlHelper;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

/**
 * Created by Zeb on 12/27/2016.
 */
public class Shader {

    /**
     * The combined shader id.
     */
    private int combined;

    /**
     * The vertex shader id.
     */
    private int vertex;

    /**
     * The fragment shader id.
     */
    private int fragment;

    /**
     * Creates the shader programs from the given shader assets.
     *
     * @param vertex   The vertex shader asset.
     * @param fragment The fragment shader asset.
     */
    public Shader(Asset vertex, Asset fragment){
        // Creates the vertex shader from the asset.
        this.vertex = ShaderFactory.create(vertex, ShaderType.VERTEX);

        // Creates the fragment shader from the asset.
        this.fragment = ShaderFactory.create(fragment, ShaderType.FRAGMENT);

        // Throws an exception if the shader programs didn't compile correctly.
        if(this.vertex == 0 || this.fragment == 0)
            throw new RuntimeException(String.format("Invalid shader compile. Vertex: %s, Fragment: %s, error: (%s)\n (%s).", vertex, fragment, getLogInfo(this.vertex), getLogInfo(this.fragment)));

        // Creates the combined shader.
        this.combined = ARBShaderObjects.glCreateProgramObjectARB();

        // Attaches the vertex shader to the combined shader.
        ARBShaderObjects.glAttachObjectARB(this.combined, this.vertex);

        // Attaches the fragment shader to the combined shader.
        ARBShaderObjects.glAttachObjectARB(this.combined, this.fragment);

        // Links the combined shader.
        ARBShaderObjects.glLinkProgramARB(this.combined);

        // Validates the combined shader program to check for errors.
        ARBShaderObjects.glValidateProgramARB(this.combined);

        // Unbinds the shader.
        unbind();
    }

    /**
     * Gets the error from the shader log.
     *
     * @param obj The shader to get the log from.
     * @return The error.
     */
    public static String getLogInfo(int obj){
        return ARBShaderObjects.glGetInfoLogARB(obj, ARBShaderObjects.glGetObjectParameteriARB(obj, ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB));
    }

    /**
     * Binds the shader.
     */
    public void bind(){
        // Binds the combined shader.
        ARBShaderObjects.glUseProgramObjectARB(combined);
    }

    /**
     * Unbinds the shader.
     */
    public void unbind(){
        // Unbinds the current shader.
        ARBShaderObjects.glUseProgramObjectARB(0);
    }

    /**
     * Gets the location of a uniform inside the shader.
     *
     * @param label The label of the uniform.
     * @return The location of the uniform.
     */
    public int getUniform(String label){
        // Returns the uniform location.
        return OpenGlHelper.glGetUniformLocation(combined, label);
    }

    /**
     * Sets a uniform with the label to the given value.
     *
     * @param label The label of the uniform.
     * @param value The new value.
     */
    public void setVector(String label, Vector2f value){
        GL20.glUniform2f(getUniform(label), value.x, value.y);
    }

    /**
     * Sets a uniform with the label to the given value.
     *
     * @param label The label of the uniform.
     * @param value The new value.
     */
    public void setVector(String label, Vector3f value){
        GL20.glUniform3f(getUniform(label), value.x, value.y, value.z);
    }

    /**
     * Sets a uniform with the label to the given value.
     *
     * @param label The label of the uniform.
     * @param value The new value.
     */
    public void setVector(String label, Vector4f value){
        GL20.glUniform4f(getUniform(label), value.x, value.y, value.z, value.w);
    }

    /**
     * Sets a uniform with the label to the given value.
     *
     * @param label The label of the uniform.
     * @param value The sampler texture id..
     */
    public void setSampler2d(String label, int value){
        int loc = getUniform(label);
        GL20.glUniform1i(loc, 0);
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, value);
    }

    /**
     * Sets a uniform with the label to the given value.
     *
     * @param label         The label of the uniform.
     * @param activeTexture The active texture id to use.
     * @param value         The sampler texture id..
     */
    public void setSampler2d(String label, int value, int activeTexture){
        int loc = getUniform(label);
        GL20.glUniform1i(loc, 0);
        GL13.glActiveTexture(GL13.GL_TEXTURE0 + activeTexture);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, value);
    }

    /**
     * Sets a uniform with the label to the given value.
     *
     * @param label The label of the uniform.
     * @param value The new value.
     */
    public void setInteger(String label, int value){
        // Sets the value.
        GL20.glUniform1i(getUniform(label), value);
    }

    /**
     * Sets a uniform with the label to the given value.
     *
     * @param label The label of the uniform.
     * @param value The new value.
     */
    public void setFloat(String label, float value){
        // Sets the value.
        GL20.glUniform1f(getUniform(label), value);
    }

    /**
     * Sets a uniform with the label to the given value.
     *
     * @param label The label of the uniform.
     * @param value The new value.
     */
    public void setBoolean(String label, boolean value){
        // Sets the value to 1 or 0 depending on the state of the value.
        setInteger(label, value ? 1 : 0);
    }

}
