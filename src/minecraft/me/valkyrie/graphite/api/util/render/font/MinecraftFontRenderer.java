package me.valkyrie.graphite.api.util.render.font;

import me.valkyrie.graphite.api.helper.IHelper;
import net.minecraft.client.gui.FontRenderer;

/**
 * @author Zeb.
 * @since 5/15/2017
 */
public class MinecraftFontRenderer implements IFontRenderingStrategy, IHelper {

    /**
     * Draws a string at the given position with the color specified.
     *
     * @param text  The text to be drawn.
     * @param x     The x position of the text.
     * @param y     The y position of the text.
     * @param color The text color.
     */
    @Override
    public void drawString(String text, float x, float y, int color){
        minecraft.fontRendererObj.drawString(text, x, y, color);
    }

    /**
     * Draws a string with a shadow at the given position with the color specified.
     *
     * @param text  The text to be drawn.
     * @param x     The x position of the text.
     * @param y     The y position of the text.
     * @param color The text color.
     */
    @Override
    public void drawStringWithShadow(String text, float x, float y, int color){
        minecraft.fontRendererObj.drawStringWithShadow(text, x, y, color);
    }

    /**
     * Gets the width of the given text for the font-renderer.
     *
     * @param text The text to get the width of.
     * @return the width of the text.
     */
    @Override
    public float getWidth(String text){
        return minecraft.fontRendererObj.getStringWidth(text);
    }

    /**
     * Gets the height of the given text for the font-renderer.
     *
     * @param text The text to get the height of.
     * @return the height of the text.
     */
    @Override
    public float getHeight(String text){
        return minecraft.fontRendererObj.FONT_HEIGHT;
    }

}
