package me.valkyrie.graphite.api.util.render.font;

/**
 * @author Zeb.
 * @since 5/15/2017
 */
public interface IFontRenderingStrategy {

    /**
     * Draws a string at the given position with the color specified.
     *
     * @param text  The text to be drawn.
     * @param x     The x position of the text.
     * @param y     The y position of the text.
     * @param color The text color.
     */
    void drawString(String text, float x, float y, int color);

    /**
     * Draws a string with a shadow at the given position with the color specified.
     *
     * @param text  The text to be drawn.
     * @param x     The x position of the text.
     * @param y     The y position of the text.
     * @param color The text color.
     */
    void drawStringWithShadow(String text, float x, float y, int color);

    /**
     * Gets the width of the given text for the font-renderer.
     *
     * @param text The text to get the width of.
     * @return the width of the text.
     */
    float getWidth(String text);

    /**
     * Gets the height of the given text for the font-renderer.
     *
     * @param text The text to get the height of.
     * @return the height of the text.
     */
    float getHeight(String text);

}
