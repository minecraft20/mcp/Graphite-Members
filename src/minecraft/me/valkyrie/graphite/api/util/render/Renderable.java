package me.valkyrie.graphite.api.util.render;

import me.valkyrie.graphite.api.helper.IHelper;
import me.valkyrie.graphite.api.helper.RenderHelper;
import me.valkyrie.graphite.api.helper.RotationHelper;
import me.valkyrie.graphite.impl.Graphite;
import me.valkyrie.graphite.impl.module.visual.storageesp.StorageEsp;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.resources.model.IBakedModel;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.*;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLSync;

import java.util.Arrays;

/**
 * @author Zeb.
 * @since 6/17/2017.
 */
public interface Renderable{

    /**
     * Draws the renderable.
     */
    void draw();

}
