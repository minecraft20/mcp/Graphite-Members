package me.valkyrie.graphite.api.util.render.shader;

import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.ARBVertexShader;

/**
 * Created by Zeb on 12/27/2016.
 */
public enum ShaderType {

    // The vertex shader id.
    VERTEX(ARBVertexShader.GL_VERTEX_SHADER_ARB),
    // The fragment shader id.
    FRAGMENT(ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);

    /**
     * The type id of the shader type.
     */
    private int typeId;

    ShaderType(int typeId){
        this.typeId = typeId;
    }

    /**
     * @return The shader type id.
     */
    public int getTypeId(){
        return typeId;
    }

}
