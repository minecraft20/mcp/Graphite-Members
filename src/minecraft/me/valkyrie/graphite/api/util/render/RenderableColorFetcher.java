package me.valkyrie.graphite.api.util.render;

import java.awt.*;

/**
 * @author Zeb.
 * @since 6/17/2017.
 */
public interface RenderableColorFetcher {

    /**
     * Gets the color of the outline for the renderable.
     *
     * @param renderable The renderable color.
     * @return the outline color.
     */
    public Color fetch(Renderable renderable);

}
