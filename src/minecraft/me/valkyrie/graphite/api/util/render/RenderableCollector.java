package me.valkyrie.graphite.api.util.render;

import java.util.List;
import java.util.Set;

/**
 * @author Zeb.
 * @since 6/17/2017.
 */
public interface RenderableCollector {

    Set<Renderable> collect();

}
