package me.valkyrie.graphite.api.util.render;

import me.valkyrie.graphite.api.helper.RenderHelper;
import me.valkyrie.graphite.api.util.Stencil;
import me.valkyrie.graphite.api.util.asset.Asset;
import me.valkyrie.graphite.api.util.render.shader.Shader;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

import static me.valkyrie.graphite.api.helper.IHelper.minecraft;

/**
 * @author Zeb.
 * @since 6/17/2017.
 */
public class OutlineShaderRenderPipeline {

    /**
     * The outline shader used to render the outline effect.
     */
    private Shader outlineShader;

    /**
     * The buffer that the entities should be rendered to
     */
    private Framebuffer objectBuffer;

    /**
     * The resolution of the outline.
     */
    private int resolution = 1;

    /**
     * The radius of the outline.
     */
    private int radius = 2;

    /**
     * If the outline glows.
     */
    private boolean glow = false;

    /**
     * A consumer that will render all objects into the object framebuffer.
     */
    private RenderableCollector objectRenderer;

    /**
     * The color fetcher.
     */
    private RenderableColorFetcher colorFetcher;

    public OutlineShaderRenderPipeline(int resolution, int radius, boolean glow, RenderableCollector objectRenderer, RenderableColorFetcher colorFetcher){
        this.resolution = resolution;
        this.radius = radius;
        this.glow = glow;
        this.objectRenderer = objectRenderer;
        this.colorFetcher = colorFetcher;
    }

    /**
     * Initiates the buffers.
     */
    private void setup(){
        // Creates the buffer if wrong size or null.
        if(objectBuffer == null
                || objectBuffer.framebufferWidth != minecraft.getFramebuffer().framebufferWidth
                || objectBuffer.framebufferHeight != minecraft.getFramebuffer().framebufferHeight)
            objectBuffer = new Framebuffer(minecraft.getFramebuffer().framebufferWidth, minecraft.getFramebuffer().framebufferHeight, true);

        // Creates the shader if null.
        if(outlineShader == null){
            outlineShader = new Shader(new Asset("/shader/outline/outline.vert"), new Asset("/shader/outline/outline.frag"));
        }
    }

    /**
     * Draws all of the outlines.
     */
    public void draw(){
        // Sets up the frame buffers and the
        setup();

        // Returns if no subjects are found.
        if(minecraft.player == null) return;

        // Clears the framebuffer.
        objectBuffer.framebufferClear();

        // The render partial ticks.
        float partialTicks = minecraft.timer.renderPartialTicks;

        // Binds the minecraft framebuffer.
        objectBuffer.bindFramebuffer(false);

        minecraft.entityRenderer.setupCameraTransform(minecraft.timer.renderPartialTicks, 0);

        // Draws all the subjects.
        drawSubjects();

        objectBuffer.unbindFramebuffer();

        minecraft.getFramebuffer().bindFramebuffer(false);

        // Switch to 2d camera.
        minecraft.entityRenderer.setupOverlayRendering();

        // Binds the minecraft framebuffer.
        minecraft.getFramebuffer().bindFramebuffer(false);

        GlStateManager.enableAlpha();

        // Binds the outline shader.
        outlineShader.bind();

        // Sets the uniforms.
        setUniforms();

        GlStateManager.enableBlend();

        // Renders the entity buffer with the outline.
        objectBuffer.framebufferRender();

        // Unbinds the outline shader.
        outlineShader.unbind();

        // Switches to 3d view.
        minecraft.entityRenderer.setupCameraTransform(partialTicks, 0);

        // Enables depth so player in inventory renders correctly.
        GlStateManager.enableAlpha();
        GlStateManager.enableBlend();
        GlStateManager.enableDepth();
    }

    /**
     * Draws the given renderable into the color and entity buffer.
     *
     * @param renderable The subject to be drawn.
     */
    private void renderSubject(Renderable renderable){
        // The render partial ticks.
        float partialTicks = minecraft.timer.renderPartialTicks;

        Color color = colorFetcher.fetch(renderable);

        GlStateManager.pushMatrix();

        // Enables outlining stuff.
        GlStateManager.enableColorMaterial();
        GlStateManager.enableOutlineMode(color.getRGB());

        GlStateManager.color(color.getRed() / 255f, color.getGreen() / 255f, color.getBlue() / 255f, color.getAlpha() / 255f);

        // Draws the subject.
        renderable.draw();

        GlStateManager.popMatrix();

        // Disables outlining stuff.
        GlStateManager.disableOutlineMode();
        GlStateManager.disableColorMaterial();
        GlStateManager.color(1,1,1,1);
    }

    /**
     * Sets all the uniforms for the shader.
     */
    private void setUniforms(){
        // Uploads the sampler as the entity buffer texture.
        outlineShader.setSampler2d("DiffuseSamper", objectBuffer.framebufferTexture);

        // Uploads the texel size (The dimensions of one pixel relative to the framebuffer size).
        outlineShader.setVector("TexelSize", new Vector2f(((float) resolution) / objectBuffer.framebufferWidth,
                ((float) resolution) / objectBuffer.framebufferHeight));

        // Sets the width.
        outlineShader.setInteger("width", radius);

        // Sets the width.
        outlineShader.setBoolean("glow", glow);
    }

    /**
     * Draws all the subjects of the outline manager.
     */
    private void drawSubjects(){
        this.objectRenderer.collect().forEach(this::renderSubject);
    }

    public int getResolution(){
        return resolution;
    }

    public void setResolution(int resolution){
        this.resolution = resolution;
    }

    public int getRadius(){
        return radius;
    }

    public void setRadius(int radius){
        this.radius = radius;
    }

    public boolean isGlow(){
        return glow;
    }

    public void setGlow(boolean glow){
        this.glow = glow;
    }

    public RenderableCollector getObjectRenderer(){
        return objectRenderer;
    }

    /**
     * A builder class for {@link OutlineShaderRenderPipeline}.
     */
    public static class Builder {

        /**
         * The resolution of the outline.
         */
        private int resolution = 1;

        /**
         * The radius of the outline.
         */
        private int radius = 2;

        /**
         * If the outline glows.
         */
        private boolean glow = false;

        /**
         * A consumer that will render all objects into the object framebuffer.
         */
        private RenderableCollector objectRenderer;

        /**
         * The color fetcher.
         */
        private RenderableColorFetcher colorFetcher;

        public Builder withResolution(int resolution){
            this.resolution = resolution;
            return this;
        }

        public Builder withRadius(int radius){
            this.radius = radius;
            return this;
        }

        public Builder withGlow(boolean glow){
            this.glow = glow;
            return this;
        }

        public Builder withObjectRenderer(RenderableCollector objectRenderer){
            this.objectRenderer = objectRenderer;
            return this;
        }

        public Builder withColorFetcher(RenderableColorFetcher colorFetcher){
            this.colorFetcher = colorFetcher;
            return this;
        }

        public OutlineShaderRenderPipeline create(){
            return new OutlineShaderRenderPipeline(resolution, radius, glow, objectRenderer, colorFetcher);
        }
    }

}
