package me.valkyrie.graphite.api.util.property.types;

import me.valkyrie.graphite.api.util.property.PropertyValue;

import java.lang.reflect.Field;

/**
 * Created by Zeb on 9/26/2016.
 */
public class BooleanPropertyValue extends PropertyValue<Boolean> {

    public BooleanPropertyValue(String label, Field field, Object object, String description, String[] aliases){
        super(label, field, object, description, aliases);
    }

    @Override
    public void fromString(String value) throws IllegalArgumentException{
        if(value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
            setValue(value.equalsIgnoreCase("true"));
        else
            throw new IllegalArgumentException();
    }
}
