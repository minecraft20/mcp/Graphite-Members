package me.valkyrie.graphite.api.util.property.types;

import me.valkyrie.graphite.api.util.property.PropertyValue;

import java.lang.reflect.Field;

/**
 * Created by Zeb on 9/26/2016.
 */
public class StringPropertyValue extends PropertyValue<String> {

    public StringPropertyValue(String label, Field field, Object object, String description, String[] aliases){
        super(label, field, object, description, aliases);
    }

    @Override
    public void fromString(String value) throws IllegalArgumentException{
        setValue(value);
    }
}
