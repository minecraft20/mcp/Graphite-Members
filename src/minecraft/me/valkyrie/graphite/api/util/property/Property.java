package me.valkyrie.graphite.api.util.property;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Zeb.
 * @since 9/17/2016
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Property {

    /**
     * Label of the property.
     *
     * @return the name of the property.
     */
    String label();

    /**
     * Aliases of the property.
     *
     * @return the aliases of the property.
     */
    String[] aliases() default {};

    /**
     * Description of the property.
     *
     * @return the description of the property.
     */
    String description() default "No description found for the property.";

}
