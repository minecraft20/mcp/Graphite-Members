package me.valkyrie.graphite.api.util.property.types;

import me.valkyrie.graphite.api.util.property.PropertyValue;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @author Zeb.
 * @since 9/26/2016
 */
public class EnumPropertyValue extends PropertyValue<Enum> {

    public EnumPropertyValue(String label, Field field, Object object, String description, String[] aliases){
        super(label, field, object, description, aliases);
    }

    @Override
    public void fromString(String value) throws IllegalArgumentException{
        if(!field.getType().isEnum()) return;

        Enum<?> enumValue = Enum.valueOf((Class<Enum>) field.getType(), value.toUpperCase());

        setValue(enumValue);
    }

    public Enum[] values(){
        Class<? extends Enum> clazz = getValue().getClass();
        return clazz.getEnumConstants();
    }

}
