package me.valkyrie.graphite.api.helper;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import org.lwjgl.opengl.GL11;

import java.util.LinkedList;
import java.util.Stack;
import java.util.Vector;

/**
 * Loads of credit to DoubleParallax for giving me the idea and some tips on how to implement
 * depth masking.
 * <p>
 * Created by Zeb on 3/8/2017.
 */
public enum DepthHelper {

    /**
     * The instance of the depth helper.
     */
    INSTANCE;

    /**
     * Stack that stores all the depth functions in order.
     */
    private Stack<Integer> functionStack = new Stack();

    /**
     * Begins the depth masking.
     */
    public void begin(){
        // Returns if the function stack is empty.
        if(!functionStack.empty()) return;

        // Clears the depth buffer.
        GlStateManager.clearDepth(1);
        GlStateManager.clear(GL11.GL_DEPTH_BUFFER_BIT);
    }

    /**
     * Ends the depth masking process.
     */
    public void end(){
        // Returns if the function stack is empty.
        if(functionStack.isEmpty()) return;

        // Changes the depth function to the less function.
        GlStateManager.depthFunc(functionStack.pop());

        // Clears the depth buffer.
        GlStateManager.clear(GL11.GL_DEPTH_BUFFER_BIT);
    }

    /**
     * Begins the mask render process.
     */
    public void beginMaskRender(){
        // Pushes the current depth function to the function stack.
        functionStack.push(GL11.glGetInteger(GL11.GL_DEPTH_FUNC));

        // Disables writing to the color buffer.
        GlStateManager.colorMask(false, false, false, false);

        // Enables depth.
        GlStateManager.enableDepth();

        // Enables depth masking.
        GlStateManager.depthMask(true);

        // Changes the depth function to the less function.
        GlStateManager.depthFunc(GL11.GL_LESS);
    }

    /**
     * Begins drawing normally.
     */
    public void beginNormalRender(){
        // Changes the depth function to the equal function,
        GlStateManager.depthFunc(GL11.GL_EQUAL);

        // Enables writing to the color buffer.
        GlStateManager.colorMask(true, true, true, true);
    }

}
