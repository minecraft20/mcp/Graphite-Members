package me.valkyrie.graphite.api.helper;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import me.valkyrie.graphite.impl.event.game.Direction;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import me.valkyrie.graphite.impl.event.game.PlayerSpawnEvent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.server.S0CPacketSpawnPlayer;
import net.minecraft.network.play.server.S13PacketDestroyEntities;
import org.lwjgl.input.Keyboard;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Helper class used to determine if an entity is a bot or not.
 *
 * @author Zeb.
 * @since 5/6/2017
 */
public class AntibotHelper implements IHelper {

    private static Map<Integer, Double> distanceMap = new HashMap<>();

    /**
     * Event handler used to handle entity removing.
     */
    private static EventHandler<PacketEvent> packetEventHandler = new EventHandler<PacketEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PacketEvent.class;
        }

        @Override
        public void handle(PacketEvent event){

            // Returns if the event is a pre event or an incoming packet.
            if(event.getDirection() == Direction.OUTGOING)
                return;

            // If the packet was a player spawn packet.
            if(event.getPacket() instanceof S0CPacketSpawnPlayer){

                // The packet received.
                S0CPacketSpawnPlayer packet = (S0CPacketSpawnPlayer) event.getPacket();

                double x = (double)packet.getX() / 32.0D;
                double y = (double)packet.getY() / 32.0D;
                double z = (double)packet.getZ() / 32.0D;

                double d = minecraft.player.getDistance(x,y,z);

                distanceMap.put(packet.func_148943_d(), d);
            }
        }

    };

    /**
     * Initializes the helper.
     */
    public static void init(){
        EventManager.INSTANCE.register(packetEventHandler);
    }

    /**
     * If the given entity is a bot.
     *
     * @param entity The entity to check if it's a bot.
     * @return if the entity is a bot.
     */
    public static boolean isEntityBot(Entity entity){
        if(!distanceMap.containsKey(entity.getEntityId()))
            return false;

        double distance = distanceMap.get(entity.getEntityId());

        // Returns if the entity is a bot.
        return (distance > 14.5 && distance < 16.5) || entity.getName().contains("\247");
    }

}
