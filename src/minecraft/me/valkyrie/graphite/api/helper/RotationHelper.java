package me.valkyrie.graphite.api.helper;

import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;

/**
 * Created by Zeb on 11/26/2016.
 */
public enum RotationHelper {

    /**
     * The instance of the rotation helper.
     */
    INSTANCE;

    /**
     * The instance of minecraft.
     */
    private Minecraft minecraft = Minecraft.getMinecraft();

    /**
     * Returns the rotations to face the given point.
     *
     * @param position The position to face.
     * @return The rotations to face the given point.
     */
    public float[] getRotations(Vec3 origin, Vec3 position){
        // Gets the difference as a vector.
        Vec3 difference = position.subtract(origin);

        // Gets the distance of the vector's x y and z components.
        double distance = difference.flat().lengthVector();

        // Finds the yaw using math and subtracts 90 degrees because minecraft does that as well.
        float yaw = (float) Math.toDegrees(Math.atan2(difference.zCoord, difference.xCoord)) - 90.0F;

        // Finds the pitch using inverse arc tangent.
        float pitch = (float) -Math.toDegrees(Math.atan2(difference.yCoord, distance));

        return new float[]{yaw, pitch};
    }

    /**
     * Returns the rotations to face the given point.
     *
     * @param position The position to face.
     * @return The rotations to face the given point.
     */
    public float[] getRotations(Vec3 position){
        return getRotations(minecraft.player.getPositionVector().addVector(0, minecraft.player.getEyeHeight(), 0),
                position);
    }

    /**
     * Returns the rotations to face the given entity.
     *
     * @param entity The entity to face.
     * @return The rotations to face the given entity.
     */
    public float[] getRotations(Entity entity){
        return getRotations(minecraft.player.getPositionVector().addVector(0, minecraft.player.getEyeHeight(), 0),
                entity.getPositionVector().addVector(0, entity.getEyeHeight(), 0).subtract(0, entity.getEyeHeight() / 2, 0));
    }

    /**
     * Returns the rotations to face the given block pos.
     *
     * @param pos The entity to face.
     * @return The rotations to face the given block pos.
     */
    public float[] getRotations(BlockPos pos){
        return getRotations(minecraft.player.getPositionVector().addVector(0, minecraft.player.getEyeHeight(), 0),
                new Vec3(pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5));
    }

    private float normalizeAngle(float angle){
        return MathHelper.wrapAngleTo180_float(angle);
    }

    /**
     * Returns the distance between two angles.
     *
     * @return The distance between two angles.
     */
    public float getDistance(float[] original, float[] rotations){
        return (float) Math.hypot(normalizeAngle(rotations[0]) - normalizeAngle(original[0]), normalizeAngle(rotations[1]) - normalizeAngle(original[1]));
    }

    /**
     * Returns the distance between two angles.
     *
     * @param vector The vector to get the distance of.
     * @return The distance between two angles.
     */
    public float getDistance(Vec3 vector){
        // The rotations to the entity.
        float[] rotations = getRotations(minecraft.player.getPositionVector().addVector(0, minecraft.player.getEyeHeight(), 0),
                vector);

        // The view angles of the player.
        float[] view = new float[]{minecraft.player.rotationYaw % 360, minecraft.player.rotationPitch};

        // Returns the distance.
        return getDistance(view, rotations);
    }

    /**
     * Returns the distance between two angles.
     *
     * @param entity The entity to get the distance of.
     * @return The distance between two angles.
     */
    public float getDistance(float[] viewRotations, Entity entity){
        // The rotations to the entity.
        float[] rotations = getRotations(entity);

        // Returns the distance.
        return getDistance(viewRotations, rotations);
    }

    /**
     * Returns the distance between two angles.
     *
     * @param entity The entity to get the distance of.
     * @return The distance between two angles.
     */
    public float getDistance(Entity entity){
        // The rotations to the entity.
        float[] rotations = getRotations(entity);

        // The view angles of the player.
        float[] view = new float[]{minecraft.player.rotationYaw, minecraft.player.rotationPitch};

        // Returns the distance.
        return getDistance(view, rotations);
    }

}
