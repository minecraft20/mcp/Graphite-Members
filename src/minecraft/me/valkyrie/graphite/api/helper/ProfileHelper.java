package me.valkyrie.graphite.api.helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Helper used to handle uuid and name fecthing from web apis.
 *
 * @author Zeb.
 * @since 4/28/2017
 */
public enum ProfileHelper {

    /**
     * The instance of the profile helper.
     */
    INSTANCE;

    /**
     * Map of all the cached uuid's.
     */
    private static final Map<String, UUID> CACHED_NAMES = new HashMap<>();

    /**
     * Url to fetch the uuid of the given name.
     */
    private static final String NAME = "https://api.mojang.com/users/profiles/minecraft/%s";

    /**
     * Url to fetch the name of the given uuid.
     */
    private static final String PROFILE = "https://sessionserver.mojang.com/session/minecraft/profile/%s";

    /**
     * Gets the uuid of the given name.
     *
     * @param name The name of the uuid's profile.
     * @return the uuid of the given name.
     */
    public UUID getUUID(String name){
        // Returns a cached uuid if any present.
        if(CACHED_NAMES.containsKey(name))
            return CACHED_NAMES.get(name);

        // Tries to fetch the uuid from the web api.
        try{
            //Gets name from web api.
            Reader uuidReader = new InputStreamReader(new URL(String.format(NAME, name)).openStream());

            // Turns text from reader into a {@link JsonObject}.
            JsonObject jsonObject = new JsonParser().parse(uuidReader).getAsJsonObject();

            String unfomatted = jsonObject.get("id").getAsString();

            String formatted = "";

            for(int length : new int[]{8, 4, 4, 4, 12}){
                formatted += "-";

                for(int i = 0; i < length; i++){
                    formatted += unfomatted.charAt(0);

                    unfomatted = unfomatted.substring(1);
                }
            }

            formatted = formatted.substring(1);

            // Gets the name from json.
            UUID uuid = UUID.fromString(formatted);

            // Puts the uuid in the cache.
            CACHED_NAMES.put(name, uuid);

            // Returns the uuid.
            return uuid;
        } catch(Exception exception){
            exception.printStackTrace();

            return null;
        }
    }

    /**
     * Gets the name of the given uuid.
     *
     * @param uuid The uuid of the player.
     * @return the name of the player.
     */
    public String getName(UUID uuid){
        try{
            // Returns any cached uuids.
            if(CACHED_NAMES.containsValue(uuid))
                return CACHED_NAMES.entrySet().stream().filter(entry -> uuid == entry.getValue()).findFirst().get().getKey();
        } catch(Exception ignored){
        }

        // Tries to fetch the uuid from the web api.
        try{
            //Gets name from web api.
            Reader uuidReader = new InputStreamReader(new URL(String.format(PROFILE, uuid.toString().replaceAll("-", ""))).openStream());

            // Turns text from reader into a {@link JsonObject}.
            JsonObject jsonObject = new JsonParser().parse(uuidReader).getAsJsonObject();

            // The unformatted uuid.
            String name = jsonObject.get("name").getAsString();

            // Puts the uuid in the cache.
            CACHED_NAMES.put(name, uuid);

            // Returns the uuid.
            return name;
        } catch(Exception exception){
            exception.printStackTrace();
            return "";
        }
    }

}
