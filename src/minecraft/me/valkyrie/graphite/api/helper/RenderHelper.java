package me.valkyrie.graphite.api.helper;

import me.valkyrie.graphite.api.helper.IHelper;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLSync;
import org.lwjgl.util.glu.GLU;

import javax.vecmath.Vector3f;
import java.awt.*;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * Helps with rendering of basic shapes and other things opengl related.
 *
 * @author Zeb.
 * @since 4/30/2017
 */
public class RenderHelper implements IHelper {

    public static void drawCube(AxisAlignedBB boundingBox){
        drawCube(boundingBox.minX, boundingBox.minY, boundingBox.minZ, boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
    }

    public static void drawCube(double x, double y, double z, double x1,
                                double y1, double z1){

        GlStateManager.pushMatrix();
        GlStateManager.enableBlend();
        GlStateManager.disableLighting();
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glVertex3d(x, y, z);
        GL11.glVertex3d(x, y1, z);
        GL11.glVertex3d(x1, y, z);
        GL11.glVertex3d(x1, y1, z);

        GL11.glVertex3d(x1, y, z1);
        GL11.glVertex3d(x1, y1, z1);
        GL11.glVertex3d(x, y, z1);
        GL11.glVertex3d(x, y1, z1);

        GL11.glEnd();

        GL11.glBegin(GL11.GL_QUADS);

        GL11.glVertex3d(x1, y1, z);
        GL11.glVertex3d(x1, y, z);
        GL11.glVertex3d(x, y1, z);
        GL11.glVertex3d(x, y, z);

        GL11.glVertex3d(x, y1, z1);
        GL11.glVertex3d(x, y, z1);
        GL11.glVertex3d(x1, y1, z1);
        GL11.glVertex3d(x1, y, z1);

        GL11.glEnd();

        GL11.glBegin(GL11.GL_QUADS);

        GL11.glVertex3d(x, y1, z);
        GL11.glVertex3d(x1, y1, z);
        GL11.glVertex3d(x1, y1, z1);
        GL11.glVertex3d(x, y1, z1);

        GL11.glVertex3d(x, y1, z);
        GL11.glVertex3d(x, y1, z1);
        GL11.glVertex3d(x1, y1, z1);
        GL11.glVertex3d(x1, y1, z);

        GL11.glEnd();

        GL11.glBegin(GL11.GL_QUADS);

        GL11.glVertex3d(x, y, z);
        GL11.glVertex3d(x1, y, z);
        GL11.glVertex3d(x1, y, z1);
        GL11.glVertex3d(x, y, z1);

        GL11.glVertex3d(x, y, z);
        GL11.glVertex3d(x, y, z1);
        GL11.glVertex3d(x1, y, z1);
        GL11.glVertex3d(x1, y, z);

        GL11.glEnd();

        GL11.glBegin(GL11.GL_QUADS);

        GL11.glVertex3d(x, y, z);
        GL11.glVertex3d(x, y1, z);
        GL11.glVertex3d(x, y, z1);
        GL11.glVertex3d(x, y1, z1);

        GL11.glVertex3d(x1, y, z1);
        GL11.glVertex3d(x1, y1, z1);
        GL11.glVertex3d(x1, y, z);
        GL11.glVertex3d(x1, y1, z);

        GL11.glEnd();

        GL11.glBegin(GL11.GL_QUADS);

        GL11.glVertex3d(x, y1, z1);
        GL11.glVertex3d(x, y, z1);
        GL11.glVertex3d(x, y1, z);
        GL11.glVertex3d(x, y, z);

        GL11.glVertex3d(x1, y1, z);
        GL11.glVertex3d(x1, y, z);
        GL11.glVertex3d(x1, y1, z1);
        GL11.glVertex3d(x1, y, z1);

        GL11.glEnd();
        GlStateManager.enableLighting();
        GlStateManager.popMatrix();
    }

    /**
     * Draws a rectangle with given positions.
     *
     * @param left   Leftmost position.
     * @param top    Topmost position.
     * @param right  Rightmost position.
     * @param bottom Bottommost potion.
     * @param color  Color of the rectangle.
     */
    public static void drawRect(double left, double top, double right, double bottom, int color){
        GlStateManager.enableAlpha();
        GlStateManager.enableBlend();
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GlStateManager.tryBlendFuncSeparate(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE, GL11.GL_ZERO);

        float a = (float) (color >> 24 & 255) / 255.0F;
        float r = (float) (color >> 16 & 255) / 255.0F;
        float g = (float) (color >> 8 & 255) / 255.0F;
        float b = (float) (color & 255) / 255.0F;

        GlStateManager.color(r, g, b, a);

        // Begins drawing the quad.
        GL11.glBegin(GL11.GL_QUADS);
        {
            GL11.glVertex2d(left, top);
            GL11.glVertex2d(left, bottom);
            GL11.glVertex2d(right, bottom);
            GL11.glVertex2d(right, top);
        }
        // Ends the quad.
        GL11.glEnd();
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GlStateManager.disableBlend();
        GlStateManager.disableAlpha();
        GlStateManager.color(1, 1, 1, 1);
    }

    /**
     * Draws a circle at the given position with the given radius.
     *
     * @param x      The position of the circle on the x axis.
     * @param y      The position of the circle on the y axis.
     * @param radius The radius of the circle.
     * @param color  The color of the circle.
     */
    public static void drawCircle(float x, float y, float radius, Color color){
        GlStateManager.enableAlpha();
        GlStateManager.alphaFunc(516, 0.001F);
        GlStateManager.enableBlend();
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GlStateManager.tryBlendFuncSeparate(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE, GL11.GL_ZERO);
        GlStateManager.color(color.getRed() / 255f, color.getGreen() / 255f, color.getBlue() / 255f, color.getAlpha() / 255f);

        // Begins drawing the circle.
        GL11.glBegin(GL11.GL_POLYGON);
        {
            for(double i = 0; i < 36; i++){
                double cs = i * 10 * Math.PI / 180;
                double ps = (i * 10 - 1) * Math.PI / 180;
                double[] outer = new double[]{
                        Math.cos(cs) * radius, -Math.sin(cs) * radius,
                        Math.cos(ps) * radius, -Math.sin(ps) * radius
                };

                GL11.glVertex2d(x + outer[0], y + outer[1]);
            }
        }
        // Ends the circle.
        GL11.glEnd();

        GL11.glEnable(GL11.GL_LINE_SMOOTH);

        // Begins drawing the circle.
        GL11.glBegin(GL11.GL_LINE_STRIP);
        {
            for(double i = 0; i < 37; i++){
                double cs = i * 10 * Math.PI / 180;
                double ps = (i * 10 - 1) * Math.PI / 180;
                double[] outer = new double[]{
                        Math.cos(cs) * radius, -Math.sin(cs) * radius,
                        Math.cos(ps) * radius, -Math.sin(ps) * radius
                };

                GL11.glVertex2d(x + outer[0], y + outer[1]);
            }
        }
        // Ends the circle.
        GL11.glEnd();

        GL11.glDisable(GL11.GL_LINE_SMOOTH);

        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GlStateManager.disableBlend();
        GlStateManager.disableAlpha();
        GlStateManager.color(1, 1, 1, 1);
        GlStateManager.alphaFunc(516, 0.1F);
    }

    /**
     * I stole this from programcreek.
     *
     * @param x X coordinate in the Minecraft world
     * @param y Y coordinate in the Minecraft world
     * @param z Z coordinate in the Minecraft world
     * @return Returns a {@link Vector3f} representing a 2D location on the screen,
     * or null if the vector fails to be converted.
     * @author Joshua Powers <jsh.powers@yahoo.com>
     * {@see http://www.programcreek.com/java-api-examples/index.php?source_dir=ShoulderSurfing-master/src/r150_r151__mc1_6_2/src/minecraft/com/teamderpy/shouldersurfing/math/VectorConverter.java}
     * <p>
     * Converts a Minecraft world coordinate to a screen coordinate
     * <p>
     * The world coordinate is the absolute location of a 3d vector
     * in the Minecraft world relative to the world origin.
     * <p>
     * Note that the return value will be scaled to match the current
     * GUI resolution of Minecraft.
     */
    public static Vector3f project2D(double x, double y, double z){
        /**
         * Buffer that will hold the screen coordinates
         */
        FloatBuffer screen_coords = GLAllocation.createDirectFloatBuffer(3);

        /**
         * Buffer that holds the transformation matrix of the view port
         */
        IntBuffer viewport = GLAllocation.createDirectIntBuffer(16);

        /**
         * Buffer that holds the transformation matrix of the model view
         */
        FloatBuffer modelview = GLAllocation.createDirectFloatBuffer(16);

        /**
         * Buffer that holds the transformation matrix of the projection
         */
        FloatBuffer projection = GLAllocation.createDirectFloatBuffer(16);

        /**
         * the return value of the gluProject call
         */
        boolean ret;


        screen_coords.clear();
        modelview.clear();
        projection.clear();

        viewport.clear();

        GL11.glGetFloat(GL11.GL_MODELVIEW_MATRIX, modelview);
        GL11.glGetFloat(GL11.GL_PROJECTION_MATRIX, projection);
        GL11.glGetInteger(GL11.GL_VIEWPORT, viewport);

        ret = GLU.gluProject((float) x, (float) y, (float) z, modelview, projection, viewport, screen_coords);

        if(ret){
            return new Vector3f(screen_coords.get(0) / 2, screen_coords.get(1) / 2, screen_coords.get(2));
        }

        return null;
    }

}
