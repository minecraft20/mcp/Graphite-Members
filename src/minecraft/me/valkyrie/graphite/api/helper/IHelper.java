package me.valkyrie.graphite.api.helper;

import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import net.minecraft.client.Minecraft;

/**
 * Provides a default factory method for creating a chat builder object and an
 * instance of minecraft
 *
 * @author Zeb
 * @author catgorl
 */
public interface IHelper {

    /**
     * An instance of minecraft.
     */
    Minecraft minecraft = Minecraft.getMinecraft();

    static ChatBuilder chatBuilderFactory(){
        return new ChatBuilder();
    }

    static ChatBuilder clientChatMessageFactory(){
        return new ChatBuilder().appendPrefix();
    }

    default ChatBuilder cb(){
        return chatBuilderFactory();
    }

    default ChatBuilder clientChatMsg(){
        return clientChatMessageFactory();
    }
}