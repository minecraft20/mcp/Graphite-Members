package me.valkyrie.graphite.api.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.MouseHelper;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import java.io.IOException;

/**
 * Used to display game screen ingame.
 * <p>
 * Created by Zeb on 12/9/2016.
 */
public class WrappedScreen extends GuiScreen {

    /**
     * The screen being displayed.
     */
    private GameScreen gameScreen;

    public WrappedScreen(GameScreen gameScreen){
        this.gameScreen = gameScreen;
    }

    @Override
    public void initGui(){
        gameScreen.open();
    }

    @Override
    public void onGuiClosed(){
        gameScreen.exit();
    }

    @Override
    public void handleInput() throws IOException{
        this.inputUpdate();
        super.handleInput();
    }

    public void inputUpdate(){
        Minecraft mc = Minecraft.getMinecraft();

        if(Keyboard.isCreated()){
            Keyboard.enableRepeatEvents(true);
            while(Keyboard.next()){
                if(Keyboard.getEventKeyState()){
                    if(Keyboard.getEventKey() == 1){
                        mc.displayGuiScreen(null);
                    }
                    gameScreen.keyPress(Keyboard.getEventKey());
                }else{
                    gameScreen.keyRelease(Keyboard.getEventKey());
                }
            }
            Keyboard.enableRepeatEvents(false);
        }

        if(Mouse.isCreated()){
            while(Mouse.next()){

                ScaledResolution scaledResolution = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);

                int var1 = Mouse.getEventX() * scaledResolution.getScaledWidth()
                        / mc.displayWidth;
                int var2 = scaledResolution.getScaledHeight() - Mouse.getEventY()
                        * scaledResolution.getScaledHeight() / mc.displayHeight - 1;

                MouseHelper.deltaX = var1;
                MouseHelper.deltaY = var2;

                if(Mouse.getEventButton() == -1){

                    if(Mouse.getEventDWheel() != 0){
                        int x = var1;
                        int y = var2;

                        gameScreen.mouseScroll((Mouse.getEventDWheel() / 100) * 3);
                    }

                    gameScreen.mouseMove(var1, var2);
                }else if(Mouse.getEventButtonState()){
                    gameScreen.mouseClick(var1, var2,
                            Mouse.getEventButton());
                }else{
                    gameScreen.mouseRelease(var1, var2);
                }
            }
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks){
        gameScreen.organize();
        gameScreen.draw();
    }
}
