package me.valkyrie.graphite.api.gui;

/**
 * Base class for a button.
 *
 * @author Zeb.
 * @since 5/3/2017
 */
public abstract class Button extends Element {

    /**
     * The text on the button.
     */
    private String text;

    public Button(String text){
        this.text = text;
    }

    /**
     * @return the button's text.
     */
    public String getText(){
        return text;
    }

    /**
     * Sets the button's display text.
     *
     * @param text The new button text.
     */
    public void setText(String text){
        this.text = text;
    }

}
