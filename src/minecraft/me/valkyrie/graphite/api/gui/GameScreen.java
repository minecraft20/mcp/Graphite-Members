package me.valkyrie.graphite.api.gui;

import me.valkyrie.graphite.api.module.Module;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Zeb.
 * @since 5/10/2017
 */
public abstract class GameScreen extends Container {

    /**
     * Adds all of the given {@link Element}s to the element collection.
     *
     * @param elements The {@link Element}s to be added.
     */
    public void add(Element... elements){
        getElements().addAll(Arrays.asList(elements));
    }

    /**
     * Removes all of the given {@link Element}s to the element collection.
     *
     * @param elements The {@link Element}s to be removed.
     */
    public void remove(Element... elements){
        getElements().removeAll(Arrays.asList(elements));
    }

    public void open(){}

    public void exit(){}
}
