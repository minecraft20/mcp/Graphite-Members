package me.valkyrie.graphite.api.gui;

import net.minecraft.util.MouseHelper;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Base class for a type of element that contains multiple sub-elements.
 *
 * @author Zeb.
 * @since 5/3/2017
 */
public abstract class Container extends Element {

    /**
     * List containing all of the elements inside of the container.
     */
    private List<Element> elements = new ArrayList<>();

    /**
     * Called when the elements need to be organized.
     */
    public abstract void organize();

    /**
     * @return all of the children inside of the container.
     */
    public List<Element> getElements(){
        return elements;
    }

    /**
     * Adds all of the given elements to the container.
     *
     * @param elements The elements to be added.
     */
    protected void add(Element... elements){
        // Adds all of the elements.
        this.elements.addAll(Arrays.asList(elements));

        // Assigns all of the elements their parents.
        Arrays.stream(elements).forEach(element -> element.assignParent(this));
    }

    /**
     * Removes all of the given elements from the container.
     *
     * @param elements The elements that should be removed.
     */
    protected void remove(Element... elements){
        // Removes all of the elements.
        this.elements.removeAll(Arrays.asList(elements));

        // Abandons all of the elements, removing their parent.
        Arrays.stream(elements).forEach(Element::abandon);
    }

    protected void organizeChildren(){
        getElements().stream().filter(Container.class::isInstance).map(Container.class::cast).forEach(Container::organize);
    }

    /**
     * Draws all of the child elements.
     */
    protected void drawChildren(){
        elements.forEach(Element::draw);
    }

    protected void drawChildrenShadowed(){
        getElements().forEach(Element::drawShadowed);
    }

    @Override
    public void mouseClick(float x, float y, int button){
        getElements().stream().filter(element -> element.isMouseOver(x,y)).forEach(element -> element.mouseClick(x,y, button));
    }

    /**
     * Called when the mouse is moved.
     *
     * @param x The x position of the mouse.
     * @param y The y position of the mouse.
     */
    @Override
    public void mouseMove(float x, float y){
        getElements().forEach(element -> element.mouseMove(x,y));
    }

    /**
     * Called when the mouse is released. Doesn't need
     * to be over the element.
     *
     * @param x The x position of the mouse.
     * @param y The y position of the mouse.
     */
    @Override
    public void mouseRelease(float x, float y){
        getElements().forEach(element -> element.mouseRelease(x,y));
    }

    /**
     * Called when the mouse is scrolled over an element.
     *
     * @param amount The scroll amount.
     */
    @Override
    public void mouseScroll(float amount){
        getElements().stream()
                .filter(element -> element.isMouseOver(MouseHelper.deltaX, MouseHelper.deltaY))
                .forEach(element -> element.mouseScroll(amount));
    }
}
