package me.valkyrie.graphite.api.gui;

import java.util.Optional;

/**
 * Base class for rendering UI elements in graphite.
 *
 * @author Zeb.
 * @since 5/3/2017
 */
public abstract class Element {

    /**
     * The parent of the gui.
     */
    private Optional<Container> parent = Optional.empty();

    /**
     * The element's x position.
     */
    public float x;

    /**
     * The element's y position.
     */
    public float y;

    /**
     * The element's width.
     */
    public float width;

    /**
     * The elements height.
     */
    public float height;

    /**
     * Creates a new element will empty bounds.
     */
    public Element(){
        this(0, 0, 0, 0);
    }

    /**
     * Creates an element at the given position without any size.
     *
     * @param x The element's x position.
     * @param y The element's y position.
     */
    public Element(float x, float y){
        this.x = x;
        this.y = y;
    }

    /**
     * Creates an element at the given position with the given size.
     *
     * @param x      The element's x position.
     * @param y      The element's y position.
     * @param width  The element's width.
     * @param height The element's height.
     */
    public Element(float x, float y, float width, float height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * Returns the x position of the element.
     *
     * @return the x position of the element.
     */
    public float getX(){
        return x;
    }

    /**
     * Sets the x position of the element.
     *
     * @param x The new x position of the element.
     */
    public void setX(float x){
        this.x = x;
    }

    /**
     * Returns the x position of the element.
     *
     * @return the x position of the element.
     */
    public float getY(){
        return y;
    }

    /**
     * Sets the y position of the element.
     *
     * @param y The new y position of the element.
     */
    public void setY(float y){
        this.y = y;
    }

    /**
     * Returns the width position of the element.
     *
     * @return the width position of the element.
     */
    public float getWidth(){
        return width;
    }

    /**
     * Sets the width of the element.
     *
     * @param width The new width of the element.
     */
    public void setWidth(float width){
        this.width = width;
    }

    /**
     * Returns the height position of the element.
     *
     * @return the height position of the element.
     */
    public float getHeight(){
        return height;
    }

    /**
     * Sets the height of the element.
     *
     * @param height The new height of the element.
     */
    public void setHeight(float height){
        this.height = height;
    }

    /**
     * Resizes the element.
     *
     * @param width  The new width.
     * @param height The new height.
     */
    public void resize(float width, float height){
        setWidth(width);
        setHeight(height);
    }

    /**
     * Draws the element.
     */
    public abstract void draw();

    /**
     * Draws in dark to be shadowed.
     */
    public void drawShadowed(){}

    /**
     * @return if the element has a parent.
     */
    public boolean hasParent(){
        return parent.isPresent();
    }

    /**
     * @return the element's parent element.
     */
    public Optional<Container> getParent(){
        return parent;
    }

    /**
     * @return the parent in the highest position of the container tree.
     */
    public Optional<Container> getHighestParent(){

        // Returns no parent if element is parent-less. This should rarely, if not never, happen.
        if(!hasParent())
            return Optional.empty();

        // The current parent.
        Optional<Container> containerOptional = getParent();

        while(containerOptional.isPresent()){

            // The container of the optional.
            Container parent = containerOptional.get();

            // Returns the container optional if there is no higher parent.
            if(!parent.hasParent())
                return containerOptional;

            // Sets the container to the higher parent.
            containerOptional = parent.getParent();
        }

        return containerOptional;
    }

    /**
     * Assigns the given container as the element's parent.
     *
     * @param container The element's new parent.
     */
    void assignParent(Container container){
        this.parent = Optional.of(container);
    }

    /**
     * Removes the element's parent.
     */
    void abandon(){
        this.parent = Optional.empty();
    }

    /**
     * Checks if the given position is over the element.
     *
     * @param x The mouse's x position.
     * @param y The mouse's y position.
     * @return if the mouse is over the button.
     */
    public boolean isMouseOver(float x, float y){
        return x >= this.x && y >= this.y && x <= this.x + width && y <= this.y + height;
    }

    /**
     * Called when the mouse is clicked on the element.
     *
     * @param x      The x position of the mouse.
     * @param y      The y position of the mouse.
     * @param button The button clicked.
     */
    public void mouseClick(float x, float y, int button){}

    /**
     * Called when the mouse is moved.
     *
     * @param x The x position of the mouse.
     * @param y The y position of the mouse.
     */
    public void mouseMove(float x, float y){}

    /**
     * Called when the mouse is released. Doesn't need
     * to be over the element.
     *
     * @param x The x position of the mouse.
     * @param y The y position of the mouse.
     */
    public void mouseRelease(float x, float y){}

    /**
     * Called when the mouse is scrolled over an element.
     *
     * @param amount The scroll amount.
     */
    public void mouseScroll(float amount){}

    /**
     * Called when a key is pressed.
     *
     * @param eventKey The key pressed.
     */
    public void keyPress(int eventKey){}

    /**
     * Called when a key is released.
     *
     * @param eventKey The key released.
     */
    public void keyRelease(int eventKey){}
}
