package me.valkyrie.graphite.api.event;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Zeb on 2/23/2017.
 */
public enum EventManager {

    /**
     * The instance of the event manager.
     */
    INSTANCE;

    /**
     * A list of all the handlers in the system.
     */
    private List<EventHandler> handlers = new CopyOnWriteArrayList<>();

    /**
     * Registers the given event handler in the event system.
     *
     * @param handler the handler to be registered.
     */
    public void register(EventHandler handler){
        handlers.add(handler);
    }

    /**
     * Unregisters the given event handler in the event system.
     *
     * @param handler the handler to be unregistered.
     */
    public void unregister(EventHandler handler){
        handlers.remove(handler);
    }

    /**
     * Calls the given event.
     *
     * @param event the event to be called.
     * @return the event called.
     */
    public <T extends Event> T call(T event){

        // Iterates though all the handlers.
        for(EventHandler eventHandler : handlers){

            // Continues if handler type doesn't match the event.
            if(eventHandler.type() != event.getClass()) continue;

            // Calls the event.
            eventHandler.handle(event);

            // Breaks the loop if the event was cancelled.
            if(event.isCancelled() || event.isBroken()) break;
        }

        // Returns the event.
        return event;
    }

}
