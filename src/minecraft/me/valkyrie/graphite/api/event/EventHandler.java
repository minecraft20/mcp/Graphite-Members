package me.valkyrie.graphite.api.event;

/**
 * Created by Zeb on 2/23/2017.
 */
public interface EventHandler<T extends Event> {

    /**
     * @return the class of the event that should be handled.
     */
    Class type();

    /**
     * Handles the event.
     *
     * @param event The event called.
     */
    void handle(T event);

}
