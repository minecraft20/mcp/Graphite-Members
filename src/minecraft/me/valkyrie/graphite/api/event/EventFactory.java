package me.valkyrie.graphite.api.event;

import java.util.function.Consumer;

public final class EventFactory { // wip idea

    public static <E> EventHandler form(Class<? extends Event> clazz, Consumer<E> handlerConsumer){
        return new LambdaBasedEventHandler(clazz, handlerConsumer);
    }

}
