package me.valkyrie.graphite.api.event;

/**
 * When the event was fired relative to it's function.
 * <p>
 * Created by Zeb on 4/21/2017.
 */
public enum Type {

    PRE,
    POST

}
