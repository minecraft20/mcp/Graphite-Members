package me.valkyrie.graphite.api.event;

/**
 * @author Zeb.
 * @since 2/23/2017
 */
public abstract class Event {

    private boolean cancelled = false;

    private boolean broken = false;

    public boolean isCancelled(){
        return cancelled;
    }

    public void setCancelled(boolean cancelled){
        this.cancelled = cancelled;
    }

    public void cancel(){
        setCancelled(true);
    }

    public void breakEvent(){
        broken = true;
    }

    public boolean isBroken(){
        return broken;
    }
}
