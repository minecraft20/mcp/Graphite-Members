package me.valkyrie.graphite.api.event;

import java.util.function.Consumer;

/**
 * @author Zeb.
 * @since 5/25/2017.
 */
public class LambdaBasedEventHandler<T extends Event> implements EventHandler<T> {

    /**
     * The type of the handler.
     */
    private Class type;

    /**
     * The consumer to handle the event.
     */
    private Consumer<Event> handlerConsumer;

    /**
     * The priority of the handler.
     */
    private int priority = 0;

    public LambdaBasedEventHandler(Class type, Consumer<Event> handlerConsumer){
        this.type = type;
        this.handlerConsumer = handlerConsumer;
    }

    public LambdaBasedEventHandler(Class type, Consumer<Event> handlerConsumer, int priority){
        this.type = type;
        this.handlerConsumer = handlerConsumer;
        this.priority = priority;
    }

    /**
     * @return The type of the handler.
     */
    @Override
    public Class type(){
        return type;
    }

    /**
     * Handles the given {@link Event} and fires the listener.
     *
     * @param event The {@link Event} to be handled.
     */
    @Override
    public void handle(T event){
        handlerConsumer.accept(event);
    }

}
