package me.valkyrie.graphite.api.module;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Zeb on 4/22/2017.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ModeManifest {

    /**
     * @return The label of the mode.
     */
    String label();

    /**
     * @return the class of the parent module.
     */
    Class<? extends Module> parentClass();

}
