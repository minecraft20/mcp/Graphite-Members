package me.valkyrie.graphite.api.module;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to register module's and fetch their properties instead of having
 * information inside the constructor.
 * <p>
 * Created by Zeb on 4/21/2017.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ModuleManifest {

    /**
     * @return the name of the module.
     */
    String label();

    /**
     * @return the category of which the module belongs to.
     */
    Category type();

    /**
     * @return the module's default display color.
     */
    int color() default 0xffd2d2d2;

    /**
     * @return if the module is visible on the array-list by default.
     */
    boolean visible() default true;

    /**
     * @return all the mode classes of the module.
     */
    Class<? extends Mode>[] modes() default {};

}
