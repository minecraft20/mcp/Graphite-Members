package me.valkyrie.graphite.api.module;

import com.google.gson.*;
import me.valkyrie.graphite.api.helper.IHelper;
import me.valkyrie.graphite.api.util.property.PropertySetFactory;
import me.valkyrie.graphite.api.util.Jsonable;
import me.valkyrie.graphite.api.util.property.PropertyValue;
import me.valkyrie.graphite.impl.Graphite;
import net.minecraft.client.entity.EntityPlayerSP;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Handles client functions.
 *
 * @author Zeb.
 * @since 4/21/2017
 */
public abstract class Module implements IHelper, Jsonable {

    /**
     * An instance of {@link Gson} to help with pretty printing of the json to the module files.
     */
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    /**
     * The player.
     */
    public EntityPlayerSP player;

    /**
     * The module's manifest containing it's information.
     */
    private ModuleManifest moduleManifest;

    /**
     * The module's key bind.
     */
    private int bind = 0;

    /**
     * The file where the module is saved to.
     */
    private File moduleFile;

    /**
     * If the module is currently running.
     */
    private boolean running;

    /**
     * If the module is visible in the arraylist.
     */
    private boolean visible = false;

    /**
     * The module's current mode.
     */
    private Mode mode;

    /**
     * The modes of the module.
     */
    private Mode[] modes;

    /**
     * The module's suffix.
     */
    protected String suffix = "";

    /**
     * All of the properties sof the module.
     */
    private Set<PropertyValue> properties = new HashSet<>();

    public Module(){

        // Checks for the module manifest.
        if(!getClass().isAnnotationPresent(ModuleManifest.class)){
            // Prints an error to let developer the module manifest is missing.
            System.err.printf("Module class \"%s\" does not have a ModuleManifest annotated.", getClass().getSimpleName());

            // Returns so no fuckery is present.
            return;
        }

        // Get's the modules manifest.
        moduleManifest = getClass().getAnnotation(ModuleManifest.class);

        // Sets the visible state.
        this.visible = moduleManifest.visible();

        // Gets the properties of the module.
        properties = PropertySetFactory.create(this);

        // Registers the command.
        Graphite.INSTANCE.getCommandManager().getContent().add(new CommandModule(this));
    }

    /**
     * Sets up the module modes and files.
     */
    public void setup(){
        // Register's the modules mode.
        registerModes();

        // Initiates the module's file.
        moduleFile = new File(Graphite.INSTANCE.getModuleManager().getModuleDirectory()
                + File.separator
                + moduleManifest.label()
                + ".json");

        // Tries to create the module's file.
        try{
            // Creates the module file if it doesn't exist.
            if(!moduleFile.exists()){

                // Creates the module's file.
                moduleFile.createNewFile();

                // Saves the module file.
                save();

                // Returns because there is no need to load.
                return;
            }

            try{
                // Loads the file.
                load();
            } catch(Exception ignored){
            }
        } catch(IOException exception){
            System.exit(0);
        }
    }

    /**
     * Adds all the given modes to the mode array.
     */
    private void registerModes(){
        // Sets the mode array to a new array of modes the length of the class mode array.
        this.modes = new Mode[moduleManifest.modes().length];

        // Iterates through all of the module modes.
        for(int i = 0; i < moduleManifest.modes().length; i++){
            // The class of the mode.
            Class<? extends Mode> modeClass = moduleManifest.modes()[i];

            // The mode.
            Mode mode = null;

            // Creates the mode.
            try{
                // Sets the mode.
                mode = modeClass.newInstance();
            } catch(Exception ignored){
            }

            // Sets the module's mode to the mode created if the module has no current mode.
            if(this.mode == null)
                setMode(mode);

            this.modes[i] = mode;
        }
    }

    /**
     * Finds the property value with the given input.
     *
     * @param input The text input.
     * @return the property value with the given input.
     */
    public Optional<PropertyValue> getProperty(String input){
        return getAllProperties().stream().filter(propertyValue -> propertyValue.matches(input)).findFirst();
    }

    /**
     * @return all of the module's properties not including children.
     */
    public Set<PropertyValue> getProperties(){
        return properties;
    }

    /**
     * @return all of the module's properties including children.
     */
    public List<PropertyValue> getAllProperties(){
        // List of all property values.
        List<PropertyValue> propertyValues = new LinkedList<>();

        // Adds all the pre-defined properties.
        propertyValues.addAll(properties);

        // Adds all of the original properties' children.
        properties.forEach(propertyValue -> addChildProperties(propertyValues, propertyValue));

        // Returns all of the property values.
        return propertyValues;
    }

    /**
     * Adds all the child properties of the given property.
     *
     * @param propertyList  The list for the children to be added to.
     * @param propertyValue The property value to add the children of.
     */
    public void addChildProperties(List<PropertyValue> propertyList, PropertyValue propertyValue){
        // Iterates through all of the properties children.
        propertyValue.getChildren().forEach(child -> {
            // Adds the child to the list.
            propertyList.add((PropertyValue) child);

            // Adds the child's children to the list.
            addChildProperties(propertyList, (PropertyValue) child);
        });
    }

    /**
     * Sets if the module is visible in the arraylist.
     *
     * @param visible If the module is visible in the arraylist.
     */
    public void setVisible(boolean visible){
        this.visible = visible;

        // Saves the module.
        save();
    }

    /**
     * @return if the module is visible.
     */
    public boolean isVisible(){
        return visible;
    }

    /**
     * @return the label of the module.
     */
    public String getLabel(){
        return moduleManifest.label();
    }

    /**
     * @return the display label of the module.
     */
    public String getDisplayLabel(){
        return String.format("%s \2477%s", moduleManifest.label().replaceAll("_", " "), suffix);
    }

    /**
     * @return the module's suffix.
     */
    public String getSuffix(){
        return suffix;
    }

    /**
     * @return the module's display color.
     */
    public int getColor(){
        return moduleManifest.color();
    }

    /**
     * @return the module's key bind to toggle it.
     */
    public int getKeyBind(){
        return bind;
    }

    /**
     * Set the module keybind to the specified KeyIndex
     *
     * @param bind The KeyIndex of the bind
     */
    public void setBind(int bind){
        this.bind = bind;
    }

    /**
     * @return all of the module's {@link Mode}
     */
    public Mode[] getModes(){
        return modes;
    }

    /**
     * @return the module's mode.
     */
    public Optional<Mode> getMode(String label){
        return Arrays.stream(modes).filter(mode -> mode.getLabel().equalsIgnoreCase(label)).findFirst();
    }

    /**
     * @return the module's current mode.
     */
    public Mode getMode(){
        return mode;
    }

    /**
     * Sets the module's current working mode.
     *
     * @param mode The new module mode.
     */
    public void setMode(Mode mode){

        // Disables the mode if there is a current mode and the module is on.
        if(getMode() != null && isRunning())
            getMode().disable();

        // Sets the current mode.
        this.mode = mode;

        // Enables the mode if the module is running.
        if(isRunning())
            mode.enable();

        // Saves the module.
        save();
    }

    /**
     * Toggles the module's state.
     */
    public void toggle(){
        this.setRunning(!running);
    }

    /**
     * Sets the module's running state.
     *
     * @param running If the module should be running.
     */
    public void setRunning(boolean running){
        if(running){
            enable();

            if(mode != null)
                mode.enable();
        } else{
            disable();

            if(mode != null)
                mode.disable();
        }

        this.running = running;

        // Saves the module.
        save();
    }

    /**
     * @return if the module is running.
     */
    public boolean isRunning(){
        return running;
    }

    /**
     * @return the {@link Category} the module belongs to,
     */
    public Category getType(){
        return this.moduleManifest.type();
    }

    /**
     * Checks if the given input matches the module's label.
     *
     * @param input The input to check.
     * @return if the given input matches the module's label.
     */
    public boolean matches(String input){
        return input.equalsIgnoreCase(getLabel()) || input.equalsIgnoreCase(getLabel().replaceAll("_", ""));
    }

    /**
     * Called when module is enabled.
     */
    public void enable(){
    }

    /**
     * Called when module is disabled.
     */
    public void disable(){
    }

    /**
     * Serializes the module's label, display label, and bind
     * into a {@link JsonObject} to be written to a file.
     *
     * @return a {@link JsonObject} containing the module properties.
     */
    public JsonObject toJson(){
        // The json object to be returned.
        JsonObject jsonObject = new JsonObject();

        // Puts the properties into the json object.
        jsonObject.add("running", new JsonPrimitive(this.running));
        jsonObject.add("keybind", new JsonPrimitive(this.bind));
        jsonObject.add("visible", new JsonPrimitive(this.visible));

        // Adds mode property if mode exists.
        if(mode != null)
            jsonObject.add("mode", new JsonPrimitive(this.mode.getLabel()));

        // Adds all of the properties to the json object.
        getAllProperties().forEach(propertyValue ->
                jsonObject.add(propertyValue.getDisplayLabel(), new JsonPrimitive(propertyValue.getValue().toString())));

        JsonArray modeArray = new JsonArray();

        Arrays.stream(modes).filter(mode -> !mode.getAllProperties().isEmpty()).forEach(mode -> modeArray.add(mode.toJson()));

        jsonObject.add("modes", modeArray);

        // Returns the json object all the properties.
        return jsonObject;
    }

    /**
     * Parses data from the given json object to
     * change properties in the module.
     *
     * @param jsonObject The serialized json object.
     */
    public void fromJson(JsonObject jsonObject){
        // Gets the bind from the json.
        this.bind = jsonObject.get("keybind").getAsInt();

        // Reads the visible state.
        if(jsonObject.has("visible"))
            this.visible = jsonObject.get("visible").getAsBoolean();

        // Sets the mode.
        if(jsonObject.has("mode"))
            Arrays.stream(modes).filter(mode -> mode.getLabel().equalsIgnoreCase(jsonObject.get("mode").getAsString())).forEach(this::setMode);

        // Loads all property values in the file.
        getAllProperties().stream()
                .filter(propertyValue -> jsonObject.has(propertyValue.getDisplayLabel()))
                .forEach(propertyValue -> propertyValue.fromString(jsonObject.get(propertyValue.getDisplayLabel()).getAsString()));

        // Tries in-case module tries to access player or world.
        try{
            // Sets the running state of the module to the state in the json object.
            this.setRunning(jsonObject.get("running").getAsBoolean());
        } catch(NullPointerException exception){
            setRunning(false);
        }

        JsonArray modeDataJsonArray = jsonObject.get("mode").getAsJsonArray();

        for(JsonElement aModeDataJsonArray : modeDataJsonArray){
            JsonObject json = aModeDataJsonArray.getAsJsonObject();

            String modeName = json.get("label").getAsString();

            getMode(modeName).ifPresent(mode -> mode.fromJson(jsonObject));
        }
    }

    /**
     * Loads the module from its file.
     */
    public void load(){
        // Tries to read data, might fail so statement is in try statement.
        try{
            // Parses the json read from the file.
            fromJson(new JsonParser().parse(new FileReader(moduleFile)).getAsJsonObject());
        } catch(IOException ignored){
        }
    }

    /**
     * Saves the module.
     */
    public void save(){
        // Tries to read data, might fail so statement is in try statement.
        try{
            // The print writer of the file that is used to write the json data.
            PrintWriter printWriter = new PrintWriter(moduleFile);

            // Prints the json into the file correctly formatted.
            printWriter.print(GSON.toJson(toJson()));

            // Closes the writer.
            printWriter.close();
        } catch(IOException | NullPointerException ignored){
        }
    }

}
