package me.valkyrie.graphite.api.module;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import me.valkyrie.graphite.api.helper.IHelper;
import me.valkyrie.graphite.api.util.property.PropertySetFactory;
import me.valkyrie.graphite.api.util.Jsonable;
import me.valkyrie.graphite.api.util.property.PropertyValue;
import me.valkyrie.graphite.impl.Graphite;
import net.minecraft.client.entity.EntityPlayerSP;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Class to used to handle module modes. Essentially sub modules inside of modules.
 * <p>
 * Created by Zeb on 4/21/2017.
 */
public abstract class Mode<T extends Module> implements IHelper, Jsonable {

    /**
     * The parent of the module mode.
     */
    protected T parent;

    /**
     * The player.
     */
    public EntityPlayerSP player;

    /**
     * The mode's manifest that stores its basic information.
     */
    private ModeManifest modeManifest;

    /**
     * All of the properties in the mode.
     */
    private Set<PropertyValue> properties;

    public Mode(){
        // Checks for the mode manifest.
        if(!getClass().isAnnotationPresent(ModeManifest.class)){
            // Prints an error to let developer the mode manifest is missing.
            System.err.printf("Mode class \"%s\" does not have a ModeManifest annotated.", getClass().getSimpleName());

            // Returns so no fuckery is present.
            return;
        }

        // Get's the mode's manifest.
        modeManifest = getClass().getAnnotation(ModeManifest.class);

        // Tries to get the parent.
        Optional<Module> parentOptional = Graphite.INSTANCE.getModuleManager().get(modeManifest.parentClass());

        // Returns if no parent is found.
        if(!parentOptional.isPresent()){
            // Prints an error to let developer the mode has no valid parent.
            System.err.printf("Mode class \"%s\" does not have a valid parent.", getClass().getSimpleName());

            // Returns so no fuckery is present.
            return;
        }

        // Sets the parent to the found parent,
        this.parent = (T) parentOptional.get();

        // Gets all of the mode's properties.
        this.properties = PropertySetFactory.create(this);
    }

    /**
     * Finds the property value with the given input.
     *
     * @param input The text input.
     * @return the property value with the given input.
     */
    public Optional<PropertyValue> getProperty(String input){
        return getAllProperties().stream().filter(propertyValue -> propertyValue.matches(input)).findFirst();
    }

    /**
     * @return all of the mode's properties not including children.
     */
    public Set<PropertyValue> getProperties(){
        return properties;
    }

    /**
     * @return all of the mode's properties including children.
     */
    public List<PropertyValue> getAllProperties(){
        // List of all property values.
        List<PropertyValue> propertyValues = new LinkedList<>();

        // Adds all the pre-defined properties.
        propertyValues.addAll(properties);

        // Adds all of the original properties' children.
        properties.forEach(propertyValue -> addChildProperties(propertyValues, propertyValue));

        // Returns all of the property values.
        return propertyValues;
    }

    /**
     * Adds all the child properties of the given property.
     *
     * @param propertyList  The list for the children to be added to.
     * @param propertyValue The property value to add the children of.
     */
    public void addChildProperties(List<PropertyValue> propertyList, PropertyValue propertyValue){
        // Iterates through all of the properties children.
        propertyValue.getChildren().forEach(child -> {
            // Adds the child to the list.
            propertyList.add((PropertyValue) child);

            // Adds the child's children to the list.
            addChildProperties(propertyList, (PropertyValue) child);
        });
    }

    /**
     * @return the mode label.
     */
    public String getLabel(){
        return modeManifest.label();
    }

    /**
     * @return the object in json format.
     */
    @Override
    public JsonObject toJson(){
        // The json object to be returned.
        JsonObject jsonObject = new JsonObject();

        jsonObject.add("label", new JsonPrimitive(getLabel()));

        // Adds all of the properties to the json object.
        getAllProperties().forEach(propertyValue ->
                jsonObject.add(propertyValue.getDisplayLabel(), new JsonPrimitive(propertyValue.getValue().toString())));

        // Returns the json object all the properties.
        return jsonObject;
    }

    /**
     * Creates the object from json.
     *
     * @param json The json data.
     */
    @Override
    public void fromJson(JsonObject json){
        // Loads all property values in the file.
        getAllProperties().stream()
                .filter(propertyValue -> json.has(propertyValue.getDisplayLabel()))
                .forEach(propertyValue -> propertyValue.fromString(json.get(propertyValue.getDisplayLabel()).getAsString()));
    }

    /**
     * Called when mode is enabled.
     */
    public void enable(){
    }

    /**
     * Called when mode is disabled.
     */
    public void disable(){
    }
}
