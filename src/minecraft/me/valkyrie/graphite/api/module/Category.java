package me.valkyrie.graphite.api.module;

public enum Category {

    FIGHT,
    VISUAL,
    MOVE,
    SELF,
    WORLD;

    /**
     * @author Zeb.
     * @since 4/21/2017
     */
    /**
     * @return the name of the category formatted properly
     */
    @Override
    public String toString(){
        return super.toString().substring(0, 1) + super.toString().toLowerCase().substring(1);
    }
}
