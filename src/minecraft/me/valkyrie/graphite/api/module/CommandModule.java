package me.valkyrie.graphite.api.module;

import me.valkyrie.graphite.api.command.Command;
import me.valkyrie.graphite.api.command.CommandManifest;
import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import me.valkyrie.graphite.api.util.property.PropertyValue;

import java.util.Arrays;
import java.util.Optional;

/**
 * Commands that helps handle properties for modules.
 *
 * @author Zeb.
 * @since 5/9/2017
 */
@CommandManifest(label = "", aliases = {})
public class CommandModule extends Command {

    /**
     * The module the command belongs to.
     */
    private Module module;

    public CommandModule(Module module){
        this.module = module;
    }

    /**
     * Executes the command.
     *
     * @param arguments The command arguments.
     * @param raw       The raw command message.
     */
    @Override
    public void execute(String[] arguments, String raw){

        // Returns if argument lengths are invalid.
        if(arguments.length != 1 && arguments.length != 2){

            // Prints error message.
            clientChatMsg().appendText("Invalid arguments.").send();

            return;
        }

        if(arguments[0].equalsIgnoreCase("mode")){

            // Returns if argument lengths are invalid.
            if(arguments.length != 2){

                // Prints error message.
                clientChatMsg().appendText("Invalid arguments, please enter mode label.").send();

                return;
            }

            // Optional with the mode.
            Optional<Mode> modeOptional = module.getMode(arguments[1]);

            // Return if mode isn't found.
            if(!modeOptional.isPresent()){

                // Prints error message.
                clientChatMsg().appendText("Mode not found.").send();

                return;
            }

            // Sets the mode.
            module.setMode(modeOptional.get());

            // Prints error message.
            clientChatMsg().appendText("Set mode to ")
                    .appendText("" + modeOptional.get().getLabel().replaceAll("_", " "), ChatColor.GRAY).appendText(".").send();

            return;

        } else if(arguments[0].equalsIgnoreCase("modes")){

            // Returns if argument lengths are invalid.
            if(arguments.length != 1){

                // Prints error message.
                clientChatMsg().appendText("Invalid arguments.").send();

                return;
            }

            // Sends start message.
            clientChatMsg().appendText("Modes in ").appendText(module.getLabel().replaceAll("_", " "), ChatColor.GRAY).appendText(" include;").send();

            // The property list message.a
            ChatBuilder chatBuilder = new ChatBuilder().appendText(" ");

            // Lists all properties.
            Arrays.stream(module.getModes())
                    .forEach(mode ->
                            chatBuilder.appendText(mode.getLabel().replaceAll("_", " "), ChatColor.GRAY).appendText(", "));

            // Sends the message.
            chatBuilder.send();

            return;
        } else if(arguments[0].equalsIgnoreCase("properties")){

            // Sends start message.
            clientChatMsg().appendText("Properties in ").appendText(module.getLabel().replaceAll("_", " "), ChatColor.GRAY).appendText(" include;").send();

            // The property list message.a
            ChatBuilder chatBuilder = new ChatBuilder();

            // Lists all properties.
            module.getAllProperties().forEach(propertyValue ->
                    chatBuilder.appendText(propertyValue.getDisplayLabel().replaceAll("_", " "), ChatColor.GRAY)
                            .appendText(", "));

            // Sends the message.
            chatBuilder.send();

            return;
        }

        // Attempts to fetch the property.
        Optional<PropertyValue> propertyValueOptional = module.getProperty(arguments[0]);

        if(!propertyValueOptional.isPresent()){

            // Prints error message.
            clientChatMsg().appendText("Property ").appendText(String.format("\"%s\"", arguments[0]), ChatColor.GRAY).appendText(" does not exist.").send();

            return;
        }

        // Gets the property from the optional.
        PropertyValue propertyValue = propertyValueOptional.get();

        // Handles property.
        if(arguments.length == 1)
            // Prints error message.
            clientChatMsg().appendText(propertyValue.getLabel().replaceAll("_", " ") + "'s", ChatColor.GRAY)
                    .appendText(" current value is ").appendText("" + propertyValue.getValue(), ChatColor.GRAY).appendText(".").send();
        else{

            // Tries to parse value.
            try{
                propertyValue.fromString(arguments[1]);

                // Prints error message.
                clientChatMsg().appendText("Set ")
                        .appendText(propertyValue.getLabel().replaceAll("_", " ") + "'s", ChatColor.GRAY)
                        .appendText(" value to ").appendText("" + propertyValue.getValue(), ChatColor.GRAY).appendText(".").send();

                // Saves the module.
                module.save();
            } catch(IllegalArgumentException exception){
                // Prints error message.
                clientChatMsg().appendText("Invalid input for property.").send();
            }
        }
    }

    @Override
    public boolean matches(String input){
        return module.matches(input);
    }
}
