package me.valkyrie.graphite.impl.module.move;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Increment;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.Vec3;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Halts outbound packets while Blink is enabled.
 * Sends them all out on disable.
 * <p>
 * TODO: Add burst mode (Send x packets every y seconds)
 * <p>
 * Fortune Cookie of the day ^_^
 * <p>
 * Back away from individuals who are impulsive.
 * Learn Chinese: Expensive = gui
 * Lucky numbers (Lotto): 14-45-28-19-46-34
 * Daily numbers (Pick3): 476
 *
 * @author deputy
 * @since 5/9/2017
 */
@ModuleManifest(label = "Blink", color = 0xFFb597c4, type = Category.MOVE)
public class Blink extends Module implements EventHandler<PacketEvent> {

    /**
     * The vertical friction of minecraft.
     */
    private static final double AIR_FRICTION = 0.9800000190734863D;

    /**
     * Queue of all the packets.
     */
    private Queue<C03PacketPlayer> packetQueue = new LinkedList<>();

    /**
     * The starting position.
     */
    private Vec3 position;

    /**
     * The blink player.
     */
    private EntityOtherPlayerMP entity;

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return PacketEvent.class;
    }

    /**
     * Adds outbound C03PacketPlayer to list of choked packets.
     */
    @Override
    public void handle(PacketEvent event){
        // Sets the info suffix.
        suffix = packetQueue.size() + "";

        // The player.
        EntityPlayerSP player = minecraft.player;

        // Returns if its not a pre event.
        if(event.getEventType() != Type.PRE) return;

        // Returns if the packet isn't a position update.
        if(!(event.getPacket() instanceof C03PacketPlayer)) return;

        // Cancels the event.
        event.setCancelled(true);

        // Adds the packet to the queue if the player moved.
        if(player.posX != player.prevPosX || player.posZ != player.prevPosZ || player.posY != player.prevPosY || !player.onGround)
            packetQueue.add((C03PacketPlayer) event.getPacket());
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        Vec3 startLocation = player.getPositionVector();

        entity = new EntityOtherPlayerMP(minecraft.world, player.getGameProfile());
        entity.setPosition(startLocation.xCoord, startLocation.yCoord, startLocation.zCoord);
        entity.rotationPitch = player.rotationPitch;
        entity.rotationYaw = player.rotationYaw;
        entity.rotationYawHead = player.rotationYawHead;
        minecraft.world.spawnEntityInWorld(entity);

        EventManager.INSTANCE.register(this);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(this);

        // Returns if the player isn't in the game.
        if(minecraft.world == null) return;

        // Sets the suffix to 0. Showing that there are no packets.
        suffix = "0";

        // Flushes the packet queue.
        while(packetQueue.size() > 0)
            minecraft.getNetHandler().getNetworkManager().sendPacket(packetQueue.poll());

        // Removes the player.
        minecraft.world.removeEntity(entity);
    }

}