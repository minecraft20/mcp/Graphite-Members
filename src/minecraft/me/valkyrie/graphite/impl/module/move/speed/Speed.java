package me.valkyrie.graphite.impl.module.move.speed;

import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.module.move.speed.modes.Hop;
import me.valkyrie.graphite.impl.module.move.speed.modes.Latest;
import net.minecraft.potion.Potion;

/**
 * Makes the user run faster than normal.
 *
 * @author Zeb.
 * @since 5/8/2017
 */
@ModuleManifest(label = "Speed",
        color = 0xFFd8c5a0,
        type = Category.MOVE,
        modes = {Latest.class, Hop.class})
public class Speed extends Module {

    /**
     * The default player walking speed.
     */
    private static final double WALK_SPEED = 0.21585;

    /**
     * The default player running speed.
     */
    private static final double RUN_SPEED = 0.2806;

    /**
     * @return the base movement speed of the player without potions.
     */
    private double getBaseMovementSpeed(){
        return player.isSprinting() ? RUN_SPEED : WALK_SPEED;
    }

    /**
     * @return the player's movement speed with it's potion effects.
     */
    public double getMovementSpeed(){

        // Returns the base speed if the player doesn't have a speed potion on.
        if(!player.isPotionActive(Potion.moveSpeed))
            return getBaseMovementSpeed();

        // Returns base speed with speed potion modifier.
        return getBaseMovementSpeed()
                * 1 + (0.06 * (player.getActivePotionEffect(Potion.moveSpeed).getAmplifier() + 1));
    }

    /**
     * If the player will collide with a bounding box.
     *
     * @param x The x offset.
     * @param z The z offset.
     * @return If th player will collide with a block.
     */
    public boolean willCollide(double x, double z){
        // Iterates through some values to check future collisions.
        for(int i = 1; i < 4; i++){
            // Returns true if a collision will occur.
            if(!minecraft.world.getCollisionBoxes(minecraft.player, player.getEntityBoundingBox().offset(x * i, 0, z * i)).isEmpty())
                return true;
        }

        // Returns false because noo collisions will occur.
        return false;
    }

    /**
     * Gets the highest possible offset.
     *
     * @param max The highest offset that can be used.
     * @return The highest possible offset.
     */
    public double getHighestOffset(double max){
        // Searches for obstructions.
        for(double i = 0; i < max; i += 0.01){
            for(int offset : new int[]{-2, -1, 0, 1, 2}){
                if(minecraft.world.getCollisionBoxes(minecraft.player,
                        player.getEntityBoundingBox().offset(player.motionX * offset, i, player.motionZ * offset)).size() > 0)
                    return i - 0.01;
            }
        }

        // Returns max if no obstruction was found.
        return max;
    }

}
