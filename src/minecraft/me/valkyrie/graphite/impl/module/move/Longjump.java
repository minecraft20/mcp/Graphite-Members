package me.valkyrie.graphite.impl.module.move;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Increment;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMoveEvent;

import java.awt.*;

/**
 * Makes the player jump far.
 *
 * @author Zeb.
 * @since 10/2/2016
 */
@ModuleManifest(label = "Longjump", color = 0xFFE45E4A, type = Category.MOVE)
public class Longjump extends Module {

    /**
     * The amount of boost that should be used when landing.
     * <p>
     * NoCheatPlus allows anything under 4.5 without damage, 16 with.
     */
    @Property(label = "Boost", aliases = {"Velocity"}, description = "The amount of boost that should be used when landing.")
    @Clamp(min = "1.35", max = "16")
    @Increment("0.25")
    private double boost = 4.5;

    /**
     * The factor of acceleration when you leave the ground.
     * <p>
     * NoCheatPlus allows anything under 2.15 so we subtract 1/10^5.
     */
    private final double GROUND_ACCELERATION = 2.15;

    /**
     * How much initial friction needs to be applied to bypass nocheatplus.
     */
    private final double INITIAL_FRICTION = 0.66;

    /**
     * How much air friction needs to be applied to bypass nocheatplus.
     */
    private final int AIR_FRICTION = 160;

    /**
     * How fast the player can run according to ncp.
     * Todo: Make optional?
     */
    private static final double WALK_SPEED = 0.29 - (1 / 100d);

    /**
     * How fast the player is moving.
     */
    private double speed = WALK_SPEED;

    /**
     * If the player was on the ground last tick.
     */
    private boolean onGroundLastTick = false;

    /**
     * How far the player travelled in the last tick.
     */
    private double distance = 0;

    /**
     * Makes the player run faster.
     */
    private final EventHandler<PlayerMoveEvent> playerMoveEventHandler = new EventHandler<PlayerMoveEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerMoveEvent.class;
        }

        /**
         * Makes the player run faster.
         */
        @Override
        public void handle(PlayerMoveEvent event){
            // Not bunny hop if not moving, in or on water, or the world is null.
            if(player.moveForward == 0 && player.moveStrafing == 0 || minecraft.world == null || player.onLiquid() || player.inLiquid()){
                speed = WALK_SPEED;
                return;
            }

            if(player.onGround){
                if(onGroundLastTick || player.motionY < -0.3){
                /*
                 * Multiplies the current speed by the ground acceleration factor.
                 *
                 * Math:
                 *  https://raw.githubusercontent.com/NoCheatPlus/NoCheatPlus/master/NCPCore/src/main/java/fr/neatmonster/nocheatplus/checks/moving/player/SurvivalFly.java
                 * Line: 1522
                 */
                    speed *= GROUND_ACCELERATION - (1 / Math.pow(10, 5));

                    // Hops up into air.
                    event.y = player.motionY = 0.4;
                    player.onGround = true;
                } else{
                    // Sets the speed.
                    speed = boost * WALK_SPEED;
                }
            } else{
                // Checks if the player was on the ground last tick to apply the right friction.
                if(onGroundLastTick){

                /*
                 * Applies initial friction of the hop.
                 *
                 * Math:
                 *  https://raw.githubusercontent.com/NoCheatPlus/NoCheatPlus/master/NCPCore/src/main/java/fr/neatmonster/nocheatplus/checks/moving/player/SurvivalFly.java
                 * Line: 1454
                 */
                    if(distance < 2.147) distance = 2.147;
                    speed = distance - (INITIAL_FRICTION * (distance - WALK_SPEED));
                } else{

                /*
                 * Applies air friction of the hop.
                 *
                 * Math:
                 *  https://raw.githubusercontent.com/NoCheatPlus/NoCheatPlus/master/NCPCore/src/main/java/fr/neatmonster/nocheatplus/checks/moving/player/SurvivalFly.java
                 * Line: 1461
                 */
                    speed = distance - (distance / (AIR_FRICTION - 1));
                }
            }

            // Sets the new on ground state used for the next tick.
            onGroundLastTick = player.onGround;

            // Makes the speed at least the normal walk speed.
            speed = Math.max(speed, WALK_SPEED);

            // Sets the horizontal movement.
            event.x = -(Math.sin(player.getDirection()) * speed);
            event.z = (Math.cos(player.getDirection()) * speed);
        }

    };

    /**
     * Handles player offsetting.
     */
    private EventHandler<PlayerMotionUpdateEvent> playerMotionUpdateEventHandler = new EventHandler<PlayerMotionUpdateEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerMotionUpdateEvent.class;
        }

        /**
         * Makes the player offset above the ground.
         */
        @Override
        public void handle(PlayerMotionUpdateEvent event){
            // Returns if the event is a post type.
            if(event.getEventType() != Type.PRE) return;

            // Sets the previously travelled distance.
            distance = Math.hypot(player.posX - player.prevPosX, player.posZ - player.prevPosZ);
        }
    };

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(playerMoveEventHandler);
        EventManager.INSTANCE.register(playerMotionUpdateEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(playerMoveEventHandler);
        EventManager.INSTANCE.unregister(playerMotionUpdateEventHandler);
    }

}

