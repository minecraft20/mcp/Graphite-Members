package me.valkyrie.graphite.impl.module.move;

import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.helper.RotationHelper;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMoveEvent;
import me.valkyrie.graphite.impl.event.player.PlayerReachEvent;
import net.minecraft.block.BlockAir;
import net.minecraft.block.state.IBlockState;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.*;

import java.util.*;

/**
 * @author Zeb.
 * @since 6/15/2017.
 */
@ModuleManifest(label = "Parkour", color = 0xFFb7e2da, type = Category.MOVE)
public class Parkour extends Module {

    /**
     * Where the player should walk to.
     */
    private Vec3 start = null;

    /**
     * Where the player should jump to.
     */
    private Vec3 end = null;

    /**
     * The starting block.
     */
    private BlockPos startBlock;

    /**
     * The end block.
     */
    private BlockPos endBlock;

    /**
     * The movement state.
     */
    private State state = State.WALKING;

    /**
     * If the player was on the ground last tick.
     */
    private boolean onGroundLastTick = false;

    /**
     * How fast the player is moving.
     */
    private double speed;

    /**
     * If the player has moved.
     */
    private boolean moved = false;

    /**
     * Handler to handle movement.
     */
    private EventHandler<PlayerMoveEvent> playerMotionEventHandler = EventFactory.form(PlayerMoveEvent.class, (PlayerMoveEvent event) -> {

        // The movement speed of the player.
        double baseSpeed = player.isSneaking() ? 0.3 : 1 * (player.getFoodStats().getFoodLevel() > 6 ? 0.283 : 0.23);

        // Botched as shit to stop the player when they land.
        if (player.onGround && !onGroundLastTick)
            state = State.RESTING;

        // Handles parallel jumping.
        if(state == State.WALKING && !moved){
            if(startBlock.getX() == endBlock.getX()){
                this.start = start.setX(player.posX);
                this.end = end.setX(player.posX);
            }else if(startBlock.getZ() == endBlock.getZ()){
                this.start = start.setZ(player.posZ);
                this.end = end.setZ(player.posZ);
            }

            if(!canJump(start, true)){
                state = State.RESTING;
                clientChatMsg().appendText("Jump is not possible.").send();
                return;
            }

            if(canJump())
                state = State.JUMPING;
        }

        // Returns if the state is resting.
        if (state == State.RESTING){
            start = null;
            end = null;

            // Sets the new on ground state used for the next tick.
            onGroundLastTick = player.onGround;
            return;
        }

        // Finds the distance to the start.
        double startDistance = player.getPositionVector().flat().distanceTo(start.flat());

        // Finds the distance to the ebd.
        double endDistance = player.getPositionVector().flat().distanceTo(end.flat());

        // Handles walking.
        if (state == State.WALKING){

            // Sets the movement to 0 if there is no destination.
            if (start == null){
                event.x = 0;
                event.z = 0;

                // Sets the new on ground state used for the next tick.
                onGroundLastTick = player.onGround;
                return;
            }

            // Will set the speed to the destination difference so it doesn't over shoot.
            baseSpeed = Math.min(baseSpeed, startDistance);

            // Calculates the rotations to the destination.
            float[] rotations = RotationHelper.INSTANCE.getRotations(start);

            // Sets the horizontal movement.
            event.x = -(Math.sin(Math.toRadians(rotations[0])) * baseSpeed);
            event.z = (Math.cos(Math.toRadians(rotations[0])) * baseSpeed);

            // Sets the move state to true to prevent pre-jumping.
            moved = true;

            // Sets the state to jumping if near the destination.
            if (startDistance <= 0.1 || startDistance == 0.30000001192092896 /* Colliding with a wall */)
                state = State.JUMPING;
        } else{

            // Calculates the rotations to the destination.
            float[] rotations = RotationHelper.INSTANCE.getRotations(end);

            // Preforms the jump.
            if (player.onGround){

                // Found this variable by jumping ALOT.
                speed = 0.4764960967713995;

                // Hops up into air.
                event.y = player.motionY = 0.42;
            } else{
                // Checks if the player was on the ground last tick to apply the right friction.
                if (onGroundLastTick){
                    speed = speed - (0.66 * (speed - 0.28));
                } else{
                    speed = speed - (speed / (160 - 1));
                }
            }

            // Makes the speed at least the normal walk speed.
            speed = Math.max(speed, baseSpeed);

            // Gets the speed the player should move to not overshoot the block.
            double moveSpeed = Math.min(speed, endDistance);

            // Stops jumping if the player is over the block.
            if (endDistance == 0){
                // Resets the state.
                state = State.RESTING;

                // Sets the new on ground state used for the next tick.
                onGroundLastTick = player.onGround;
                return;
            }

            // Sets the horizontal movement.
            event.x = -(Math.sin(Math.toRadians(rotations[0])) * moveSpeed);
            event.z = (Math.cos(Math.toRadians(rotations[0])) * moveSpeed);

            // Sets the move state to true to prevent pre-jumping.
            moved = true;
        }

        // Sets the new on ground state used for the next tick.
        onGroundLastTick = player.onGround;
    });

    /**
     * Handles selecting of a target block.
     */
    private EventHandler<PacketEvent> packetEventHandler = EventFactory.form(PacketEvent.class, (PacketEvent event) -> {
        // Returns if the packet isn't a placement packet and the event type isn't a pre event.
        if (!(event.getPacket() instanceof C08PacketPlayerBlockPlacement && event.getEventType() == Type.PRE))
            return;

        // The packet being sent in the form of a C08PacketPlayerBlockPlacement.
        C08PacketPlayerBlockPlacement packet = (C08PacketPlayerBlockPlacement) event.getPacket();

        // Jumps to the block.
        proceed(packet.func_179724_a());

        // Cancels the event if the reach is too far.
        if (packet.func_179724_a().toVector().distanceTo(player.getPositionEyes(1)) > 4.5)
            event.cancel();
    });

    /**
     * Handles setting the reach to allow the player to jump to far blocks.
     */
    private EventHandler<PlayerReachEvent> playerReachEventHandler =
            EventFactory.form(PlayerReachEvent.class, (PlayerReachEvent event) -> event.setReach(1000));

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        state = State.RESTING;

        EventManager.INSTANCE.register(packetEventHandler);
        EventManager.INSTANCE.register(playerReachEventHandler);
        EventManager.INSTANCE.register(playerMotionEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(playerReachEventHandler);
        EventManager.INSTANCE.unregister(packetEventHandler);
        EventManager.INSTANCE.unregister(playerMotionEventHandler);
    }

    private boolean canJump(){
        return canJump(player.getPositionVector(), false);
    }

    /**
     * @return if the player can make the jump.
     */
    private boolean canJump(Vec3 startPos, boolean full){
        double yDifference = end.yCoord - start.yCoord;

        double y = 0;

        double speed = 0;

        Vec3 pos = startPos;

        boolean grounded = player.onGround;

        float yaw = RotationHelper.INSTANCE.getRotations(startPos, end)[0];

        if(yDifference > 1.3)
            return false;

        AxisAlignedBB boundingBox = player.getEntityBoundingBox();

        for(int i = 0; i < 200; i++){
            if (i == 0){
                speed = full ? 0.4764960967713995 : 0.4;
                y = 0.42;
            } else{
                if (i == 1)
                    speed = speed - (0.66 * (speed - 0.28));
                else
                    speed = speed - (speed / (160 - 1));
            }

            if(!minecraft.world.getCollisionBoxes(player, boundingBox.offset(0, y, 0)).isEmpty()){
                y = 0;
            }

            speed = Math.max(speed, 0.28);

            double moveSpeed = Math.min(speed, pos.flat().distanceTo(end.flat()));

            if(pos.yCoord < end.yCoord && y < 0)
                return false;

            if (moveSpeed < 0.28)
                return true;

            // Sets the horizontal movement.
            pos = pos.addVector(-(Math.sin(Math.toRadians(yaw)) * moveSpeed), y ,Math.cos(Math.toRadians(yaw)) * moveSpeed);
            boundingBox = boundingBox.offset(-(Math.sin(Math.toRadians(yaw)) * moveSpeed), y ,Math.cos(Math.toRadians(yaw)) * moveSpeed);

            minecraft.world.spawnParticle(EnumParticleTypes.CRIT, pos.xCoord, pos.yCoord, pos.zCoord, 0, 0, 0, 10, 10);

            if(pos.yCoord < end.yCoord && y < 0)
                return false;

            y *= 0.9800000190734863;
            if(minecraft.world.getCollisionBoxes(player, boundingBox.offset(0, -0.1, 0)).isEmpty())
                y -= 0.08;
        }

        return false;
    }

    /**
     * Gets the module ready to preform the jump to the given {@link BlockPos}.
     *
     * @param pos The {@link BlockPos} that the player should proceed to.
     */
    private void proceed(BlockPos pos){

        // Returns if the current state is resting.
        if(state != State.RESTING)
            return;

        // Returns if the position is invalid.
        if(pos.getY() == -1)
            return;

        // Sets the ending block.
        endBlock = pos;

        // Tries to get the starting points directly under the player.
        Set<Vec3> startPoints = getCorners(startBlock = new BlockPos(player.posX, Math.floor(player.posY), player.posZ));

        // If no points were found, search the blocks under it. This can be caused by fences and other 1.5m height objects.
        if (startPoints.isEmpty())
            startPoints = getCorners(startBlock = new BlockPos(player.posX, Math.floor(player.posY) - 1, player.posZ));

        if(startPoints.isEmpty() && player.getLand() != null)
            startPoints = getCorners(startBlock = player.getLand());

        // Gets all of the end points from the given position.
        Set<Vec3> endPoints = getCorners(pos);

        // The minimum amount of distance found between two points.
        double minDistance = Double.POSITIVE_INFINITY;

        // Does a horrible o(n^2) search for the best pair to jump with.
        for (Vec3 start : startPoints){
            for(Vec3 end : endPoints){
                // Checks if distance is within minimum distance.
                if(start.distanceTo(end) > minDistance)
                    continue;

                // Sets the current start-end pair to the best pair.
                minDistance = start.distanceTo(end);
                this.end = end;
                this.start = start;
            }
        }

        // Returns if no good points were found.
        if (this.start == null)
            return;

        // Sets the state to walking to proceed with the jump.
        state = State.WALKING;

        // Resets the speed.
        this.speed = 0;

        this.moved = false;
    }

    /**
     * Gets all the corners for the block at the given {@link BlockPos}.
     *
     * @param pos The {@link BlockPos} that the method should get the corners of.
     * @return all of the corners at the block.
     */
    private Set<Vec3> getCorners(BlockPos pos){

        // Gets the block state.
        IBlockState blockState = minecraft.world.getBlockState(pos);

        // Returns an empty set if there is no valid block at the position.
        if (blockState == null || blockState.getBlock() == null || blockState.getBlock() instanceof BlockAir)
            return new HashSet<>();

        // A list to store all of the possible bounding boxes for the block.
        List<AxisAlignedBB> boundingBoxes = new ArrayList<>();

        // Adds all of the block bounding boxes to the bounding boxes list.
        blockState.getBlock().addCollisionBoxesToList(minecraft.world,
                pos, blockState, AxisAlignedBB.INFINITY, boundingBoxes, player);

        // Creates a set of positions to store all of the corners in.
        Set<Vec3> positions = new HashSet<>();

        // Adds all of the positions from the bounding boxes.
        boundingBoxes.forEach(boundingBox -> positions.addAll(Arrays.asList(getCorners(boundingBox))));

        return positions;
    }

    /**
     * Gets all of the corners in the bounding box.
     *
     * @param boundingBox The bounding box to get the corners of.
     * @return the corners found.
     */
    private Vec3[] getCorners(AxisAlignedBB boundingBox){

        // Returns an empty array of vectors if the bounding box can't be collided with.
        if (boundingBox == null)
            return new Vec3[0];

        // Returns all of the corners of the bounding box.
        return new Vec3[]{
                new Vec3(boundingBox.minX, boundingBox.maxY, boundingBox.minZ),
                new Vec3(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ),
                new Vec3(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ),
                new Vec3(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ)
        };
    }

    /**
     * The states that the parkour can be in.
     */
    enum State {

        RESTING,
        WALKING,
        JUMPING

    }

}
