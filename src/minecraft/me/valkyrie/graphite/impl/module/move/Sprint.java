package me.valkyrie.graphite.impl.module.move;

import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.player.PlayerUpdateEvent;
import me.valkyrie.graphite.impl.event.render.RenderScreenEvent;


/**
 * This module forces the player to sprint.
 * <p>
 * Created by Zeb on 4/21/2017.
 */
@ModuleManifest(label = "Sprint", color = 0xFFc761dd, type = Category.MOVE, visible = false)
public class Sprint extends Module {

    /**
     * Automatically sprints, uses render screen event for minimal input lag.
     */
    private EventHandler<RenderScreenEvent> sprintHandler = new EventHandler<RenderScreenEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return RenderScreenEvent.class;
        }

        /**
         * Forces the player to sprint if they can.
         */
        @Override
        public void handle(RenderScreenEvent event){
            // Returns if the player is not on the ground or the event type is a post update event.
            if(!player.onGround || player.isSprinting() == canSprint()) return;

            // Sets the player's sprinting state to if they can sprint.
            player.setSprinting(canSprint());
        }

    };

    @Override
    public void enable(){
        EventManager.INSTANCE.register(sprintHandler);
    }

    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(sprintHandler);
    }

    /**
     * @return if the player is able to sprint.
     */
    private boolean canSprint(){
        return player.getFoodStats().getFoodLevel() > 6 && player.onGround && (player.moveForward > 0);
    }

}
