package me.valkyrie.graphite.impl.module.move;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.entity.EntityCollisionEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;

/**
 * Makes the player jump high out of the water.
 *
 * @author Zeb.
 * @since 5/6/2017
 */
@ModuleManifest(label = "Dolphin", color = 0xFF5b9fc6, type = Category.MOVE)
public class Dolphin extends Module {

    /**
     * Ticks used to wait until jumping again.
     */
    private int ticks = 0;

    /**
     * If the player was in water.
     */
    private boolean wasWater = false;

    /**
     * Makes the liquid bounding boxes solid.
     */
    private EventHandler<EntityCollisionEvent> entityCollisionEventHandler = new EventHandler<EntityCollisionEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return EntityCollisionEvent.class;
        }

        /**
         * Makes the liquid bounding boxes solid.
         */
        @Override
        public void handle(EntityCollisionEvent event){

            if(minecraft.world == null)
                return;

            // The block type the entity is colliding with.
            Block block = minecraft.world.getBlockState(event.getPosition()).getBlock();

            // Returns if the block isn't a liquid or if the entity isn't the player.
            if(!(block instanceof BlockLiquid)
                    || !(event.getEntity() instanceof EntityPlayerSP)
                    || player.inLiquid()
                    || player.isSneaking())
                return;

            // Sets the event's bounding box to a full bounding box.
            event.setBoundingBox(AxisAlignedBB.FULL.contract(0, 0.000001, 0).offset(event.getPosition()));
        }

    };

    /**
     * Handles player floating.
     */
    private EventHandler<PlayerMotionUpdateEvent> playerMotionUpdateEventHandler = new EventHandler<PlayerMotionUpdateEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerMotionUpdateEvent.class;
        }

        /**
         * Makes the player float to the surface of the water.
         */
        @Override
        public void handle(PlayerMotionUpdateEvent event){
            // Return if the event is a post motion update or the player is sneaking.
            if(event.getEventType() == Type.POST || player.isSneaking())
                return;

            // Sets wasWater to false if on land or on a ladder.
            if(player.onGround || player.isOnLadder()) wasWater = false;

            // Makes you go much higher than you normally should.
            if(wasWater && player.motionY > 0){

                double f = 1.2;

                // FUCKING HELL MAN.
                if(player.motionY < 0.03){
                    player.motionY *= f;
                    player.motionY += 0.067;
                } else if(player.motionY < 0.05){
                    player.motionY *= f;
                    player.motionY += 0.06;
                } else if(player.motionY < 0.07){
                    player.motionY *= f;
                    player.motionY += 0.057;
                } else if(player.motionY < 0.11){
                    player.motionY *= f;
                    player.motionY += 0.05;
                } else{
                    player.motionY += 0.0517;
                }
            }

            if(wasWater && player.motionY < 0 && player.motionY > -0.3)
                player.motionY += 0.04;

            // Sets fall distance cuz fall distance is dumb.
            player.fallDistance = 0;

            // Sets onground to true to remove fall damage.
            event.setOnGround(true);

            // Returns if the player isn't in liquid.
            if(!player.onLiquid()) return;

            // Does a large jump.
            player.motionY = 0.5;

            // Sets the previous in water state to true so extra height can be added..
            wasWater = true;
        }
    };

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(entityCollisionEventHandler);
        EventManager.INSTANCE.register(playerMotionUpdateEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(entityCollisionEventHandler);
        EventManager.INSTANCE.unregister(playerMotionUpdateEventHandler);
    }

}