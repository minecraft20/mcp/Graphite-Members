package me.valkyrie.graphite.impl.module.move;

import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.helper.RotationHelper;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import me.valkyrie.graphite.impl.event.player.PlayerReachEvent;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.BlockPos;
import net.minecraft.util.Vec3;

/**
 * @author Zeb.
 * @since 6/20/2017.
 */
@ModuleManifest(label = "Click_teleport", color = 0xFFe58d34, type = Category.MOVE)
public class ClickTeleport extends Module {

    /**
     * The maximum amount of packets that can be sent.
     */
    private final int PACKETS = 120;

    /**
     * Handles selecting of a target block.
     */
    private EventHandler<PacketEvent> packetEventHandler = EventFactory.form(PacketEvent.class, (PacketEvent event) -> {
        // Returns if the packet isn't a placement packet and the event type isn't a pre event.
        if (!(event.getPacket() instanceof C08PacketPlayerBlockPlacement && event.getEventType() == Type.PRE))
            return;

        // The packet being sent in the form of a C08PacketPlayerBlockPlacement.
        C08PacketPlayerBlockPlacement packet = (C08PacketPlayerBlockPlacement) event.getPacket();

        // The position to teleport to.
        Vec3 position = packet.func_179724_a().toVector().addVector(0, 0.5, 0);

        // The starting position.
        Vec3 startingPosition = player.getPositionVector();

        // Returns if initial distance is too large.
        if(startingPosition.flat().distanceTo(position.flat()) > 30){
            clientChatMsg().appendText("Distance is too far.").send();
            return;
        }

        // Jumps to the block.
        teleport(packet.func_179724_a());

        // Cancels the event if the reach is too far.
        if (packet.func_179724_a().toVector().distanceTo(player.getPositionEyes(1)) > 4.5)
            event.cancel();
    });

    /**
     * Handles setting the reach to allow the player to jump to far blocks.
     */
    private EventHandler<PlayerReachEvent> playerReachEventHandler =
            EventFactory.form(PlayerReachEvent.class, (PlayerReachEvent event) -> event.setReach(31));

    /**
     * Teleports to the given block position.
     *
     * @param pos The block to teleport to.
     */
    public void teleport(BlockPos pos){

        // The position to teleport to.
        Vec3 position = pos.toVector().addVector(0, 0.5, 0);

        // The starting position.
        Vec3 startingPosition = player.getPositionVector();

        // The current position of the teleport.
        Vec3 currentPosition = startingPosition;

        // The rotations to the block.
        float[] rotations = RotationHelper.INSTANCE.getRotations(startingPosition, position);

        // The x multipliers to teleport.
        double x = -Math.sin(Math.toRadians(rotations[0]));
        double z = Math.cos(Math.toRadians(rotations[0]));

        // The distance from the start.
        double distance = 0;

        for(int i = 0; i < PACKETS; i++){

            // The distance to the end.
            double distanceToEnd = currentPosition.flat().distanceTo(position.flat());

            if(distanceToEnd == 0)
                break;

            if(!minecraft.world.getCollisionBoxes(player, player.getEntityBoundingBox().offset(x * distance, 0, z * distance)).isEmpty())
                break;

            // Adds to the total distance.
            distance += Math.min(0.3, distanceToEnd);

            // Sets the new current position
            currentPosition = startingPosition.addVector(x * distance, 0, z * distance);

            // Sends the packet to get to the position.
            minecraft.getNetHandler().getNetworkManager().sendPacket(new C03PacketPlayer.C04PacketPlayerPosition(
                    currentPosition.xCoord,
                    currentPosition.yCoord,
                    currentPosition.zCoord,
                    false));
        }
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(packetEventHandler);
        EventManager.INSTANCE.register(playerReachEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(playerReachEventHandler);
        EventManager.INSTANCE.unregister(packetEventHandler);
    }

}
