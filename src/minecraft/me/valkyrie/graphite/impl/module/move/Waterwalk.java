package me.valkyrie.graphite.impl.module.move;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.entity.EntityCollisionEvent;
import me.valkyrie.graphite.impl.event.game.Direction;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import me.valkyrie.graphite.impl.event.player.PlayerUpdateEvent;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.init.Items;
import net.minecraft.network.play.client.*;
import net.minecraft.network.play.server.S2FPacketSetSlot;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.MathHelper;

import java.util.Random;

/**
 * Allows the player to walk on water.
 *
 * @author Zeb.
 * @since 4/24/2017
 */
@ModuleManifest(label = "Waterwalk", color = 0xFF507cc4, type = Category.MOVE)
public class Waterwalk extends Module {

    /**
     * Makes the liquid bounding boxes solid.
     */
    private EventHandler<EntityCollisionEvent> entityCollisionEventHandler = new EventHandler<EntityCollisionEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return EntityCollisionEvent.class;
        }

        /**
         * Makes the liquid bounding boxes solid.
         */
        @Override
        public void handle(EntityCollisionEvent event){

            if(minecraft.world == null)
                return;

            // The block type the entity is colliding with.
            Block block = minecraft.world.getBlockState(event.getPosition()).getBlock();

            // Returns if the block isn't a liquid or if the entity isn't the player.
            if(!(block instanceof BlockLiquid)
                    || !(event.getEntity() instanceof EntityPlayerSP)
                    || player.inLiquid()
                    || player.isSneaking()
                    || player.fallDistance > 3)
                return;

            // Sets the event's bounding box to a full bounding box.
            event.setBoundingBox(AxisAlignedBB.FULL.contract(0, 0.000000000001, 0).offset(event.getPosition()));
        }

    };

    /**
     * Handles offsetting to allow the water-walk to bypass nocheatplus.
     */
    private EventHandler<PacketEvent> packetEventHandler = new EventHandler<PacketEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PacketEvent.class;
        }

        /**
         * Handles the event.
         *
         * @param event The event called.
         */
        @Override
        public void handle(PacketEvent event){

            // Returns if the packet isn't a player packet or if the event type is post or if the player is in a liquid, sneaking, or falling.
            if(!(event.getPacket() instanceof C03PacketPlayer)
                    || player.inLiquid()
                    || !player.onLiquid()
                    || player.isSneaking()
                    || player.fallDistance > 3)
                return;

            // The packet being sent.
            C03PacketPlayer packet = (C03PacketPlayer) event.getPacket();

            // Cancels the move event.
            if(player.moveStrafing == 0 && player.moveForward == 0)
                event.setCancelled(true);

//            player.sendQueue.addToSendQueue(new C0BPacketEntityAction(minecraft.player, event.getEventType() == Type.PRE ?
//                    C0BPacketEntityAction.Action.STOP_SPRINTING : C0BPacketEntityAction.Action.START_SPRINTING));

            // Sets the packet's offset.
            packet.setY(packet.getPositionY() + (player.ticksExisted % 2 == 0 ? 0.000000000002 : 0));

            // Spoofs onground state to not burn.
            packet.setGrounded(player.ticksExisted % 2 != 0);
        }

    };

    /**
     * Handles player floating.
     */
    private EventHandler<PlayerUpdateEvent> playerUpdateEventHandler = new EventHandler<PlayerUpdateEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerUpdateEvent.class;
        }

        /**
         * Makes the player float to the surface of the water.
         */
        @Override
        public void handle(PlayerUpdateEvent event){
            // Returns if the event is of the post type.
            if(event.getEventType() == Type.POST) return;

            // Sets the player's upwards motion.
            if(player.inLiquid() && !player.isSneaking() && !minecraft.gameSettings.keyBindJump.isPressed() && player.fallDistance < 3)
                player.motionY = 0.13;
        }
    };

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(packetEventHandler);
        EventManager.INSTANCE.register(entityCollisionEventHandler);
        EventManager.INSTANCE.register(playerUpdateEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(packetEventHandler);
        EventManager.INSTANCE.unregister(entityCollisionEventHandler);
        EventManager.INSTANCE.unregister(playerUpdateEventHandler);
    }

}