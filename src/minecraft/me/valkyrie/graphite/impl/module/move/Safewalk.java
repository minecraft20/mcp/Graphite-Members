package me.valkyrie.graphite.impl.module.move;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.ModeManifest;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.entity.EntitySafewalkEvent;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.network.play.client.C0BPacketEntityAction;

/**
 * Makes the player not fall.
 *
 * @author Zeb.
 * @since 5/9/2017
 */
@ModuleManifest(label = "Safewalk", color = 0xFFaddbbe, type = Category.MOVE)
public class Safewalk extends Module implements EventHandler<EntitySafewalkEvent> {

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return EntitySafewalkEvent.class;
    }

    /**
     * Handles the event.
     *
     * @param event The event called.
     */
    @Override
    public void handle(EntitySafewalkEvent event){
        event.setCancelled(true);
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(this);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(this);
    }

}
