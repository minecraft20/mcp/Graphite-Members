package me.valkyrie.graphite.impl.module.move.speed.modes;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Mode;
import me.valkyrie.graphite.api.module.ModeManifest;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Increment;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMoveEvent;
import me.valkyrie.graphite.impl.module.move.speed.Speed;

/**
 * A speed that hops to go faster.
 *
 * @author Zeb.
 * @since 5/8/2017
 */
@ModeManifest(label = "Hop", parentClass = Speed.class)
public class Hop extends Mode<Speed> {

    /**
     * The factor of acceleration when you leave the ground.
     * <p>
     * NoCheatPlus allows anything under 2.15 so we subtract 1/10^5.
     */
    @Property(label = "Ground_acceleration", aliases = {"Boost_acceleration"}, description = "The factor of acceleration when you leave the ground.")
    @Clamp(min = "0", max = "2.15")
    @Increment("0.05")
    private double GROUND_ACCELERATION = 2.15;

    /**
     * How much initial friction needs to be applied to bypass nocheatplus.
     */
    @Property(label = "Initial_friction", aliases = {"Boost_friction", "Launch_friction"}, description = "How much initial friction needs to be applied to bypass nocheatplus.")
    @Clamp(min = "0.66", max = "1.0")
    @Increment("0.01")
    private double INITIAL_FRICTION = 0.66;

    /**
     * How much air friction needs to be applied to bypass nocheatplus.
     */
    @Property(label = "Air_friction", aliases = {"Friction", "Glide_friction"}, description = "How much air friction needs to be applied to bypass nocheatplus.")
    @Clamp(min = "0", max = "160")
    private int AIR_FRICTION = 160;

    /**
     * How fast the player is moving.
     */
    private double speed;

    /**
     * If the player was on the ground last tick.
     */
    private boolean onGroundLastTick = false;

    /**
     * How far the player travelled in the last tick.
     */
    private double distance = 0;

    /**
     * Makes the player run faster.
     */
    private final EventHandler<PlayerMoveEvent> playerMoveEventHandler = new EventHandler<PlayerMoveEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerMoveEvent.class;
        }

        /**
         * Makes the player run faster.
         */
        @Override
        public void handle(PlayerMoveEvent event){
            // Not bunny hop if not moving, in or on water, or the world is null.
            if(player.moveForward == 0 && player.moveStrafing == 0 || minecraft.world == null || player.onLiquid() || player.inLiquid()){
                speed = parent.getMovementSpeed();
                return;
            }

            if(player.onGround){
                /*
                 * Multiplies the current speed by the ground acceleration factor.
                 *
                 * Math:
                 *  https://raw.githubusercontent.com/NoCheatPlus/NoCheatPlus/master/NCPCore/src/main/java/fr/neatmonster/nocheatplus/checks/moving/player/SurvivalFly.java
                 * Line: 1522
                 */
                speed *= GROUND_ACCELERATION - (1 / Math.pow(10, 5));

                // Hops up into air.
                event.y = player.motionY = 0.42;
            } else{
                // Checks if the player was on the ground last tick to apply the right friction.
                if(onGroundLastTick){

                    /*
                     * Applies initial friction of the hop.
                     *
                     * Math:
                     *  https://raw.githubusercontent.com/NoCheatPlus/NoCheatPlus/master/NCPCore/src/main/java/fr/neatmonster/nocheatplus/checks/moving/player/SurvivalFly.java
                     * Line: 1454
                     */
                    speed = distance - (INITIAL_FRICTION * (distance - parent.getMovementSpeed()));
                } else{

                    /*
                     * Applies air friction of the hop.
                     *
                     * Math:
                     *  https://raw.githubusercontent.com/NoCheatPlus/NoCheatPlus/master/NCPCore/src/main/java/fr/neatmonster/nocheatplus/checks/moving/player/SurvivalFly.java
                     * Line: 1461
                     */
                    speed = distance - (distance / (AIR_FRICTION - 1));
                }
            }

            // Sets the new on ground state used for the next tick.
            onGroundLastTick = player.onGround;

            // Makes the speed at least the normal walk speed.
            speed = Math.max(speed, parent.getMovementSpeed());

            // Sets the horizontal movement.
            event.x = -(Math.sin(player.getDirection()) * speed);
            event.z = (Math.cos(player.getDirection()) * speed);
        }

    };

    /**
     * Handles player distance calculations.
     */
    private EventHandler<PlayerMotionUpdateEvent> playerMotionUpdateEventHandler = new EventHandler<PlayerMotionUpdateEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerMotionUpdateEvent.class;
        }

        /**
         * Makes the player offset above the ground.
         */
        @Override
        public void handle(PlayerMotionUpdateEvent event){
            // Returns if the event is a post type.
            if(event.getEventType() != Type.PRE) return;

            // Sets the previously travelled distance.
            distance = Math.hypot(player.posX - player.prevPosX, player.posZ - player.prevPosZ);
        }
    };

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(playerMoveEventHandler);
        EventManager.INSTANCE.register(playerMotionUpdateEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(playerMoveEventHandler);
        EventManager.INSTANCE.unregister(playerMotionUpdateEventHandler);
    }

}
