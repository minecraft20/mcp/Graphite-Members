package me.valkyrie.graphite.impl.module.move;

import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMoveEvent;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C13PacketPlayerAbilities;
import net.minecraft.network.play.server.S08PacketPlayerPosLook;
import net.minecraft.util.Vec3;

/**
 * @author Zeb.
 * @since 5/17/2017
 */
@ModuleManifest(label = "Fly", color = 0xFFc4e082, type = Category.MOVE)
public class Fly extends Module {


	/**
	 * How fast you fly.
	 */
	@Property(label = "Speed", aliases = { "Multiplier" }, description = "How fast you fly.")
	@Clamp(min = "1", max = "5")
	private double speed = 1;

	private EventHandler<PlayerMoveEvent> playerMoveEventHandler = EventFactory.form(PlayerMoveEvent.class,
			(rawEvent) -> {
				PlayerMoveEvent event = (PlayerMoveEvent) rawEvent;

				// Handles vertical movement.
				double y = (minecraft.gameSettings.keyBindJump.getIsKeyPressed() ? .5 : 0)
						+ (minecraft.gameSettings.keyBindSneak.getIsKeyPressed() ? -.5 : 0);

				// Sets the speed to the base walking speed.
				double speed = this.speed * 0.8; // but why ?__?

				// Stops the player from moving if the player isn't moving.
				if (player.moveForward == 0 && player.moveStrafing == 0)
					speed = 0;

				// The horizontal velocities for the player.
				double x = -(Math.sin(player.getDirection()) * speed);
				double z = (Math.cos(player.getDirection()) * speed);

				// Sets the horizontal movement.
				event.x = x;
				event.y = player.motionY = y * this.speed;
				event.z = z;
			});

    /**
     * Sets the player to a flying state to bypass in creative mode.
     */
	private EventHandler<PacketEvent> packetEventHandler = EventFactory.form(PacketEvent.class, (PacketEvent event) -> {

		if (!(event.getPacket() instanceof C13PacketPlayerAbilities))
		    return;

        C13PacketPlayerAbilities packet = (C13PacketPlayerAbilities) event.getPacket();

        packet.setFlying(true);
	});

	/**
	 * Registers the listeners to the event system.
	 */
	@Override
	public void enable() {
		EventManager.INSTANCE.register(packetEventHandler);
		EventManager.INSTANCE.register(playerMoveEventHandler);

        // BIG HACK.
        minecraft.player.sendPlayerAbilities();
	}

	/**
	 * Unregisters the listeners from the event system.
	 */
	@Override
	public void disable() {
		EventManager.INSTANCE.unregister(packetEventHandler);
		EventManager.INSTANCE.unregister(playerMoveEventHandler);

        // BIG HACK.
        minecraft.player.sendPlayerAbilities();
	}

}
