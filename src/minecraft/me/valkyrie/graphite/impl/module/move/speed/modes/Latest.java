package me.valkyrie.graphite.impl.module.move.speed.modes;

import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Mode;
import me.valkyrie.graphite.api.module.ModeManifest;
import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMoveEvent;
import me.valkyrie.graphite.impl.module.move.speed.Speed;
import net.minecraft.util.MathHelper;

/**
 * The lastest speed mode that bypasses nocheatplus.
 *
 * @author Zeb.
 * @since 5/8/2017
 */
@ModeManifest(label = "Latest", parentClass = Speed.class)
public class Latest extends Mode<Speed> {

    /**
     * Makes the player run faster.
     */
    private final EventHandler<PlayerMoveEvent> playerMoveEventHandler = EventFactory.form(PlayerMoveEvent.class, (PlayerMoveEvent event) -> {

        // Don't speed if not moving, in or on water, or the world is null.
        if(player.moveForward == 0 && player.moveStrafing == 0
                || minecraft.world == null || player.onLiquid()
                || player.inLiquid() || !player.onGround) return;

        // Sets the speed to the base walking speed.
        double speed = parent.getMovementSpeed() * (player.ticksExisted % 2 != 0 ? 1 : 1.69);

        // The horizontal velocities for the player.
        double x = -(Math.sin(player.getDirection()) * speed);
        double z = (Math.cos(player.getDirection()) * speed);

        // Sets the horizontal movement.
        event.x = x;
        event.z = z;

    });

    /**
     * Handles player offsetting.
     */
    private EventHandler<PlayerMotionUpdateEvent> playerMotionUpdateEventHandler = EventFactory.form(PlayerMotionUpdateEvent.class,
            (PlayerMotionUpdateEvent event) -> {
        // Returns if the event isn't a pre motion update.
        if(event.getEventType() != Type.PRE) return;

        // Returns if the player isn't moving, on ground, or in a liquid.
        if(player.moveForward == 0 && player.moveStrafing == 0
                || minecraft.world == null || player.onLiquid()
                || player.inLiquid() || !player.onGround)
            return;

        // Offsets to bypass.
        event.setY(event.getY() + (player.ticksExisted % 2 == 0 ? parent.getHighestOffset(0.4) : 0));
    });

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(playerMoveEventHandler);
        EventManager.INSTANCE.register(playerMotionUpdateEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(playerMoveEventHandler);
        EventManager.INSTANCE.unregister(playerMotionUpdateEventHandler);
    }

}
