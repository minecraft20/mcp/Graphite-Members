package me.valkyrie.graphite.impl.module.move;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C0BPacketEntityAction;

/**
 * Makes the player sneak at full speed.
 *
 * @author Zeb.
 * @since 5/3/2017
 */
@ModuleManifest(label = "Sneak", color = 0xFFb597c4, type = Category.MOVE)
public class Sneak extends Module implements EventHandler<PacketEvent> {

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return PacketEvent.class;
    }

    /**
     * Handles sneaking.
     */
    @Override
    public void handle(PacketEvent event){

        // Returns if packet is invalid.
        if(!(event.getPacket() instanceof C03PacketPlayer))
            return;

        // Sends sneak packets to move.
        player.sendQueue.addToSendQueue(new C0BPacketEntityAction(minecraft.player, event.getEventType() == Type.PRE ?
                C0BPacketEntityAction.Action.STOP_SNEAKING : C0BPacketEntityAction.Action.START_SNEAKING));
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(this);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(this);

        // Returns if the player isn't in-game yet.
        if(minecraft.player == null)
            return;

        // Sends un-sneak packet if the player isn't sneaking.
        if(!player.isSneaking())
            player.sendQueue.addToSendQueue(new C0BPacketEntityAction(minecraft.player, C0BPacketEntityAction.Action.STOP_SNEAKING));
    }

}
