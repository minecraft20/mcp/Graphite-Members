package me.valkyrie.graphite.impl.module.move;

import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import me.valkyrie.graphite.api.util.property.Child;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Increment;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.entity.EntityStepEvent;
import me.valkyrie.graphite.impl.event.game.GameTickEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMoveEvent;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.Vec3;
import tv.twitch.chat.Chat;

import java.util.Arrays;

/**
 * @author Zeb.
 * @since 6/1/2017.
 */
@ModuleManifest(label = "Step", color = 0xFFd893b7, type = Category.MOVE)
public class Step extends Module {

    /**
     * All of the offsets to use to step up the respective height.
     */
    private static final double[][] RAW_OFFSETS = new double[][]{
            {
                    0.42,
                    0.753
            },
            {
                    0.42,
                    0.75,
                    1,
                    1.16,
                    1.23,
                    1.2
            },
            {
                    0.42,
                    0.78,
                    0.63,
                    0.51,
                    0.9,
                    1.21,
                    1.45,
                    1.43
            }
    };

    /**
     * How high the player steps.
     */
    @Property(label = "Height", aliases = {"Amount", "StepHeight"}, description = "How high the player steps.")
    @Clamp(min = "1", max = "2")
    @Increment("0.5")
    private float height = 1;

    /**
     * If the instant should have a cooldown timer.
     */
    @Property(label = "Timer")
    private boolean timer = true;

    /**
     * If the player should step down blocks instantly.
     */
    @Property(label = "Reverse")
    private boolean reverse = false;

    /**
     * If the step is currently stepping.
     */
    private boolean stepping;

    /**
     * The stepping offsets.
     */
    private double[] offsets;

    /**
     * The timer ticks.
     */
    private int ticks = 0;

    /**
     * Handles offsets.
     */
    private EventHandler<EntityStepEvent> entityStepEventHandler = EventFactory.form(EntityStepEvent.class, (EntityStepEvent event) -> {

        if(player == null || minecraft.world == null)
            return;

        // Doesn't step if the player is in or on liquid or the stepping entity isn't the player.
        if(player.inLiquid() || player.onLiquid() || event.getEntity() != player || !player.onGround) return;


        if(event.getType() == Type.PRE){

            // Sets the current height.
            float stepHeight = this.height;

            if(minecraft.world.getCollisionBoxes(player,
                    player.getEntityBoundingBox().offset(player.motionX, 1.6, player.motionZ)).isEmpty())
                stepHeight = 1.5f;

            // Lowers the height if it can't do a 2 block step.
            if(minecraft.world.getCollisionBoxes(player,
                    player.getEntityBoundingBox().offset(player.motionX, 1.1, player.motionZ)).isEmpty())
                stepHeight = 1;

            // Check to see if where the player is stepping is valid.
            if((stepHeight == 1 && minecraft.world.getCollisionBoxes(player,
                    player.getEntityBoundingBox().offset(player.motionX, 0.6, player.motionZ)).isEmpty())
                    || !minecraft.world.getCollisionBoxes(player, player.getEntityBoundingBox().offset(
                    player.motionX, stepHeight + 0.01, player.motionZ)).isEmpty())
                return;

            if(stepHeight > this.height)
                return;

            // Sets the step height.
            event.setHeight(stepHeight);

            // Finds what offset group to use.
            offsets = RAW_OFFSETS[Math.round(stepHeight * 2) - 2];

            // Sets the stepping state.
            stepping = true;

            // Slows the player down using the timer.
            if(timer){
                // Sets the game timer to slow down the player.
                minecraft.timer.timerSpeed = new float[]{0.7f, 0.35f, 0.25f}[Math.round(stepHeight * 2) - 2];

                ticks = 2;
            }
        } else if(stepping){
            Arrays.stream(offsets)
                    .forEach(offset ->
                            player.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(
                                    player.posX, player.posY + offset, player.posZ, false)));

            stepping = false;
        }
    });

    /**
     * Handles timer cooldown and step reverse.
     */
    private EventHandler<GameTickEvent> gameTickEventHandler = EventFactory.form(GameTickEvent.class, event -> {

        // Returns if there is no world.
        if(minecraft.world == null)
            return;

        // Handles timer.
        if(ticks == 0)
            minecraft.timer.timerSpeed = 1;
        else
            ticks--;

        // Does a reverse step if the player isn't in any special block and on ground.
        if(player != null && player.onGround && !player.inLiquid() && !player.isInWeb && !player.isOnLadder() && player.onGround && reverse && !player.onLiquid()){

            // Checks how low ground is.
            for(double y = 0; y < height + 0.5; y += 0.01){
                if(!minecraft.world.getCollisionBoxes(player, player.getEntityBoundingBox().offset(0, -y, 0)).isEmpty()){

                    // Goes down if ground was found at a safe distance.
                    player.motionY = -9;
                    break;
                }
            }
        }

    });

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(entityStepEventHandler);
        EventManager.INSTANCE.register(gameTickEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(entityStepEventHandler);
        EventManager.INSTANCE.unregister(gameTickEventHandler);
    }

}
