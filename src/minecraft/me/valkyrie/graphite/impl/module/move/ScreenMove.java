package me.valkyrie.graphite.impl.module.move;

import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.game.GameTimer;
import me.valkyrie.graphite.impl.event.entity.EntitySafewalkEvent;
import me.valkyrie.graphite.impl.event.game.GameTickEvent;
import me.valkyrie.graphite.impl.event.render.RenderScreenEvent;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.MathHelper;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import sun.security.krb5.internal.Ticket;

import java.util.Arrays;

/**
 * @author Zeb.
 * @since 5/18/2017
 */
@ModuleManifest(label = "ScreenMove", color = 0xFF896ab5, type = Category.MOVE, visible = false)
public class ScreenMove extends Module implements EventHandler<RenderScreenEvent> {

    /**
     * The {@link KeyBinding}(s) used to move the player.
     */
    private static final KeyBinding[] INPUTS = new KeyBinding[]{
            minecraft.gameSettings.keyBindForward,
            minecraft.gameSettings.keyBindLeft,
            minecraft.gameSettings.keyBindBack,
            minecraft.gameSettings.keyBindRight,
            minecraft.gameSettings.keyBindJump,
            minecraft.gameSettings.keyBindSprint
    };

    /**
     * Game timer to handle rotations.
     */
    private GameTimer gameTimer = new GameTimer();

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return RenderScreenEvent.class;
    }

    /**
     * Handles input, using render screen event for minimal input lag.
     */
    @Override
    public void handle(RenderScreenEvent event){
        if(minecraft.currentScreen instanceof GuiContainer){
            Arrays.stream(INPUTS).forEach(input -> KeyBinding.setKeyBindState(input.getKeyCode(), isActive(input)));

            // Testing value.
            float sensitivity = 0.3f * gameTimer.getTimePassed();

            player.rotationYaw +=
                    Keyboard.isKeyDown(Keyboard.KEY_RIGHT) ? sensitivity : Keyboard.isKeyDown(Keyboard.KEY_LEFT) ? -sensitivity : 0;

            player.rotationPitch +=
                    Keyboard.isKeyDown(Keyboard.KEY_DOWN) ? sensitivity : Keyboard.isKeyDown(Keyboard.KEY_UP) ? -sensitivity : 0;

            player.rotationPitch = MathHelper.clamp_float(player.rotationPitch, -90, 90);

        }

        gameTimer.reset();
    }

    /**
     * @param keyBinding The bind to check the state of.
     * @return if the given bind is active.
     */
    private boolean isActive(KeyBinding keyBinding){
        // Handles mouse interaction.
        if(keyBinding.getKeyCode() < -1)
            return Mouse.isButtonDown(keyBinding.getKeyCode() + 100);

        return Keyboard.isKeyDown(keyBinding.getKeyCode());
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(this);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(this);
    }

}
