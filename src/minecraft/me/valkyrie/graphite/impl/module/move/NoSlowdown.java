package me.valkyrie.graphite.impl.module.move;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.helper.RotationHelper;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import me.valkyrie.graphite.impl.event.player.PlayerSlowdownEvent;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Vec3;

/**
 * Created by Zeb on 4/21/2017.
 */
@ModuleManifest(label = "No_Slowdown", color = 0xFFe2d663, type = Category.MOVE, visible = false)
public class NoSlowdown extends Module {

    /**
     * The packet listener.
     */
    private EventHandler<PacketEvent> packetEventHandler = new EventHandler<PacketEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PacketEvent.class;
        }

        /**
         * Unblocks and blocks to achieve the no-slowdown effect.
         */
        @Override
        public void handle(PacketEvent event){
            // Returns if the packet isn't a player packet.
            if(!(event.getPacket() instanceof C03PacketPlayer)) return;

            // Returns if the player isn't blocking.
            if(!player.isBlocking()) return;

            // Sends the needed packets.
            switch(event.getEventType()){
                case PRE:
                    // Unblocks.
                    player.sendQueue.getNetworkManager().sendPacket(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, BlockPos.ORIGIN, EnumFacing.DOWN));
                    break;
                case POST:
                    // Blocks.
                    player.sendQueue.getNetworkManager().sendPacket(new C08PacketPlayerBlockPlacement(player.getHeldItem()));
                    break;
            }
        }

    };

    /**
     * The slowdown listener.
     */
    private EventHandler<PlayerSlowdownEvent> playerSlowdownEventHandler = new EventHandler<PlayerSlowdownEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerSlowdownEvent.class;
        }

        /**
         * Removes the slowdown.
         */
        @Override
        public void handle(PlayerSlowdownEvent event){
            event.setCancelled(true);
        }

    };

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(packetEventHandler);
        EventManager.INSTANCE.register(playerSlowdownEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(packetEventHandler);
        EventManager.INSTANCE.unregister(playerSlowdownEventHandler);
    }

}
