package me.valkyrie.graphite.impl.module.visual.storageesp;

import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.property.Child;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Increment;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.api.util.render.Renderable;
import me.valkyrie.graphite.api.util.render.RenderableCollector;
import me.valkyrie.graphite.api.util.render.RenderableColorFetcher;
import me.valkyrie.graphite.impl.module.visual.storageesp.modes.Outline;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.*;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Zeb.
 * @since 6/17/2017.
 */
@ModuleManifest(label = "Storage_esp", color = 0xFF7fced8, type = Category.VISUAL,
        modes = Outline.class)
public class StorageEsp extends Module {

    /**
     * If chests should be drawn.
     */
    @Property(label = "Chests", aliases = "Chest", description = "If chests should be drawn.")
    public boolean chests = true;

    /**
     * The red value of chests.
     */
    @Child("Chests")
    @Property(label = "Red", description = "The red value of chests.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int chestRed = 155;

    /**
     * The green value of chests.
     */
    @Child("Chests")
    @Property(label = "Green", description = "The green value of chests.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int chestGreen = 217;

    /**
     * The blue value of chests.
     */
    @Child("Chests")
    @Property(label = "Blue", description = "The blue value of chests.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int chestBlue = 205;

    /**
     * If chests should be drawn.
     */
    @Property(label = "Trapped_chests", aliases = "Trapped_chest", description = "If trapped chests should be drawn.")
    public boolean trappedChests = true;

    /**
     * The red value of chests.
     */
    @Child("Trapped_chests")
    @Property(label = "Red", description = "The red value of trapped chests.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int trappedChestRed = 242;

    /**
     * The green value of chests.
     */
    @Child("Trapped_chests")
    @Property(label = "Green", description = "The green value of trapped chests.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int trappedChestGreen = 131;

    /**
     * The blue value of chests.
     */
    @Child("Trapped_chests")
    @Property(label = "Blue", description = "The blue value of trapped chests.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int trappedChestBlue = 104;

    /**
     * If ender chests should be drawn.
     */
    @Property(label = "Ender_chests", aliases = "Chest", description = "If ender chests should be drawn.")
    public boolean enderChests = true;

    /**
     * The red value of ender chests.
     */
    @Child("Ender_chests")
    @Property(label = "Red", description = "The red value of ender chests.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int enderRed = 198;

    /**
     * The green value of ender chests.
     */
    @Child("Ender_chests")
    @Property(label = "Green", description = "The green value of ender chests.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int enderGreen = 61;

    /**
     * The blue value of ender chests.
     */
    @Child("Ender_chests")
    @Property(label = "Blue", description = "The blue value of ender chests.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int enderBlue = 182;

    /**
     * If chests should be drawn.
     */
    @Property(label = "Furnaces", aliases = "Furnace", description = "If furnaces should be drawn.")
    public boolean furnace = true;

    /**
     * The red value of chests.
     */
    @Child("Furnaces")
    @Property(label = "Red", description = "The red value of Furnaces.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int furnacesRed = 148;

    /**
     * The green value of chests.
     */
    @Child("Furnaces")
    @Property(label = "Green", description = "The green value of Furnaces.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int furnacesGreen = 148;

    /**
     * The blue value of chests.
     */
    @Child("Furnaces")
    @Property(label = "Blue", description = "The blue value of Furnaces.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int furnacesBlue = 148;

    /**
     * If chests should be drawn.
     */
    @Property(label = "Hoppers", aliases = "Hopper", description = "If hoppers should be drawn.")
    public boolean hoppers = true;

    /**
     * The red value of chests.
     */
    @Child("Hoppers")
    @Property(label = "Red", description = "The red value of hoppers.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int hopperRed = 66;

    /**
     * The blue value of chests.
     */
    @Child("Hoppers")
    @Property(label = "Blue", description = "The blue value of hoppers.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int hopperGreen = 143;

    /**
     * The green value of chests.
     */
    @Child("Hoppers")
    @Property(label = "Green", description = "The green value of hoppers.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int hopperBlue = 244;

    /**
     * If chests should be drawn.
     */
    @Property(label = "Enchanting_tables", aliases = "Enchanting_table", description = "If enchant tables should be drawn.")
    public boolean enchantTables = true;

    /**
     * The red value of chests.
     */
    @Child("Enchanting_tables")
    @Property(label = "Red", description = "The red value of enchant tables.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int enchantTableRed = 216;

    /**
     * The blue value of chests.
     */
    @Child("Enchanting_tables")
    @Property(label = "Blue", description = "The blue value of enchant tables.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int enchantTableGreen = 153;

    /**
     * The green value of chests.
     */
    @Child("Enchanting_tables")
    @Property(label = "Green", description = "The green value of enchant tables.")
    @Clamp(min = "0", max = "255")
    @Increment("5")
    public int enchantTableBlue = 229;

    /**
     * The renderable collector for the renderers.
     */
    private RenderableCollector renderableCollector;

    /**
     * The color fetcher for the renderers.
     */
    private RenderableColorFetcher colorFetcher;

    public StorageEsp(){
        this.renderableCollector = new RenderableCollector() {
            @Override
            public Set<Renderable> collect(){

                Set<Renderable> renderables = new HashSet<>();

                for(TileEntity tileEntity : minecraft.world.loadedTileEntityList){
                    if(tileEntity instanceof TileEntityFurnace){
                        if(furnace)
                            renderables.add(tileEntity);
                    }else if(tileEntity instanceof TileEntityHopper){
                        if(hoppers)
                            renderables.add(tileEntity);
                    }else if(tileEntity instanceof TileEntityEnchantmentTable)
                        if(enchantTables)
                            renderables.add(tileEntity);
                }

                for(TileEntity tileEntity : minecraft.world.loadedTileEntityList){
                    if(tileEntity instanceof TileEntityChest){
                        TileEntityChest chest = (TileEntityChest) tileEntity;
                        if(chest.getBlockType() == Blocks.trapped_chest && trappedChests)
                            renderables.add(chest);
                        else if(chests)
                            renderables.add(chest);
                    } else if(tileEntity instanceof TileEntityEnderChest){
                        if(enderChests)
                            renderables.add(tileEntity);
                    }
                }

                return renderables;
            }
        };

        this.colorFetcher = renderable -> {

            if(renderable instanceof TileEntityChest){
                TileEntityChest chest = (TileEntityChest) renderable;
                return chest.getBlockType() == Blocks.trapped_chest ?
                        new Color(trappedChestRed, trappedChestGreen, trappedChestBlue)
                        : new Color(chestRed, chestGreen, chestBlue);
            }else if(renderable instanceof TileEntityEnderChest)
                return new Color(enderRed, enderGreen, enderBlue);
            else if(renderable instanceof TileEntityFurnace)
                return new Color(furnacesRed, furnacesGreen, furnacesBlue);
            else if(renderable instanceof TileEntityHopper)
                return new Color(hopperRed, hopperGreen, hopperBlue);
            else if(renderable instanceof TileEntityEnchantmentTable)
                return new Color(enchantTableRed, enchantTableGreen, enchantTableBlue);

            return Color.WHITE;
        };
    }

    public RenderableCollector getRenderableCollector(){
        return renderableCollector;
    }

    public RenderableColorFetcher getColorFetcher(){
        return colorFetcher;
    }

}
