package me.valkyrie.graphite.impl.module.visual.esp.modes;

import me.valkyrie.graphite.api.module.Mode;
import me.valkyrie.graphite.api.module.ModeManifest;
import me.valkyrie.graphite.impl.module.visual.esp.Esp;

/**
 * @author Zeb.
 * @since 6/23/2017.
 */
@ModeManifest(label = "Panel", parentClass = Esp.class)
public class Panel extends Mode<Esp> {



}
