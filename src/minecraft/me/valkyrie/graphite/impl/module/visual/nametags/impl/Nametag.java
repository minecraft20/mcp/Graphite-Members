package me.valkyrie.graphite.impl.module.visual.nametags.impl;

import com.sun.javafx.geom.Vec2f;
import me.valkyrie.graphite.api.helper.IHelper;
import me.valkyrie.graphite.api.helper.RenderHelper;
import me.valkyrie.graphite.api.util.Friend;
import me.valkyrie.graphite.impl.Graphite;
import me.valkyrie.graphite.impl.module.visual.nametags.Nametags;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import javax.naming.Name;
import javax.vecmath.Vector3f;
import java.util.*;

/**
 * The object representation of a nametag.
 *
 * @author Zeb.
 * @since 4/30/2017
 */
public class Nametag implements IHelper {

    /**
     * All the names of the enchantments.
     */
    private static final Map<Enchantment, String> ENCHANT_NAMES = new HashMap<>();

    /**
     * The font-renderer to use to render the nametag.
     */
    private static FontRenderer fontRenderer = minecraft.fontRendererObj;

    /**
     * The nametags module.
     */
    private Nametags nametags;

    /**
     * Registers all the names.
     */
    static {
        ENCHANT_NAMES.put(Enchantment.protection, "P");
        ENCHANT_NAMES.put(Enchantment.thorns, "Th");
        ENCHANT_NAMES.put(Enchantment.sharpness, "Sh");
        ENCHANT_NAMES.put(Enchantment.flame, "Fir");
        ENCHANT_NAMES.put(Enchantment.fireAspect, "Fir");
        ENCHANT_NAMES.put(Enchantment.punch, "Kb");
        ENCHANT_NAMES.put(Enchantment.knockback, "Kb");
        ENCHANT_NAMES.put(Enchantment.efficiency, "Eff");
        ENCHANT_NAMES.put(Enchantment.unbreaking, "Un");
        ENCHANT_NAMES.put(Enchantment.power, "Pow");
    }

    /**
     * The player that the nametag belongs to.
     */
    private EntityPlayer player;

    /**
     * The x position of the nametag.
     */
    private float x;

    /**
     * The y position of the nametag.
     */
    private float y;

    /**
     * The nametag's width.
     */
    private float width;

    /**
     * The nametag's height.
     */
    private float height;

    /**
     * factor to scale by
     */
    private float scale = 0.5f;

    /**
     * Optional for the player's friend state
     */
    private Optional<Friend> opt;

    public Nametag(EntityPlayer player){
        this.player = player;
        opt = Graphite.INSTANCE.getFriendManager().get(player.getUniqueID());

        this.nametags = (Nametags) Graphite.INSTANCE.getModuleManager().get(Nametags.class).get();
    }

    /**
     * Moves the rectangle to help with clicking shit.
     */
    public void update(){

        // Ticks used for interpolation.
        float partialTicks = minecraft.timer.renderPartialTicks;

        // The position of the nametag in the world.
        Vector3f worldPosition = getWorldPosition();

        // The world positions screen position.
        Vector3f position = RenderHelper.project2D(worldPosition.x, worldPosition.y, worldPosition.z);

        // Checks if position was found and if it has a valid depth. If not, return.
        if(position == null || (position.getZ() < 0 || position.getZ() > 1))
            return;

        // Sets the width of the nametag.
        this.width = fontRenderer.getStringWidth(getText()) + 4;

        // Sets the height of the nametag;
        this.height = fontRenderer.FONT_HEIGHT + 4;

        // Sets the x position.
        this.x = position.x;

        // Sets the y position.
        this.y = Display.getHeight() / 2 - position.y;
    }

    /**
     * Draws the nametag.
     */
    public void draw(){

        this.scale = nametags.scale;

        // MATH.
        this.scale += Math.max(1 - (minecraft.player.getDistanceToEntity(player) / 4), 0);

        if(Keyboard.isKeyDown(minecraft.gameSettings.ofKeyBindZoom.getKeyCode()) && minecraft.inGameHasFocus)
            scale *= 3;

        // create a scale factor to scale back to
        float sclfact = 1 / scale;

        // Starts new opengl state matrix.
        GlStateManager.pushMatrix();

        // Return if position is invalid.
        if(x == y && x == 0){
            // Switches to normal rendering.
            minecraft.entityRenderer.setupOverlayRendering();

            // Pops the gl matrix to restore gl states.
            GlStateManager.popMatrix();

            // Returns.
            return;
        }

        // Scales down.
        GlStateManager.scale(scale, scale, 1);

        // Translates to center the nametag.
        GlStateManager.translate(x * sclfact - (width/2), y * sclfact - (height/2), 0);

        GlStateManager.enableAlpha();
        GL11.glEnable(GL11.GL_BLEND);
        // Sets the blending function.
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        // Draws the transparent back-plate.
        RenderHelper.drawRect(0, 0, width, height, 0x64000000);

        // The width of the nametag text.
        float stringWidth = fontRenderer.getStringWidth(getText());

        // The height of the nametag text.
        float stringHeight = fontRenderer.FONT_HEIGHT;

        GlStateManager.translate(width/2, 0, 0);
        drawItems(player);
        GlStateManager.translate(-width/2, 0, 0);

        // Translates the text over to prefix graphical bug.
        GlStateManager.translate(width / 2 - stringWidth / 2, height / 2 - stringHeight / 2 + 1, 0);

        // Draws the nametag.
        fontRenderer.drawString(getText(), 0, 0, this.getColour());

        // Translates the text over to prefix graphical bug.
        GlStateManager.translate(-(width / 2 - stringWidth / 2), -(height / 2 - stringHeight / 2 + 1), 0);

        // Translates back to the normal position.
        GlStateManager.translate(-x * sclfact, -y * sclfact, 0);

        // Scales back up to normal scale.
        GlStateManager.scale(sclfact, sclfact, 1);

        // Enables blend and alpha to fix hotbar render issues.
        GlStateManager.enableBlend();
        GlStateManager.enableAlpha();

        // Pops the gl matrix to restore gl states.
        GlStateManager.popMatrix();
    }

    /**
     * Draws all the items the given player has on them.
     */
    private void drawItems(EntityPlayer entityPlayer){

        // The scale of the items.
        double scale = 0.6;

        if(Keyboard.isKeyDown(minecraft.gameSettings.ofKeyBindZoom.getKeyCode()) && minecraft.inGameHasFocus)
            scale *= 2;

        GlStateManager.scale(scale, scale, 1);

        // All the items that will be rendered.
        List<ItemStack> items = new ArrayList<>();

        // Adds all the armor the entity has on.
        for(ItemStack armorStack : entityPlayer.inventory.armorInventory)
            if(armorStack != null) items.add(armorStack);

        if(player.getHeldItem() != null)
            items.add(entityPlayer.getHeldItem());

        // Reverse the list.
        Collections.reverse(items);

        // How much space the items will take up.
        float width = 20 * items.size();

        // Iterates through all the items and draws them.
        for(int i = 0; i < items.size(); i++){
            // The current itemstack being rendered.
            ItemStack itemStack = items.get(i);

            // The translations for the rendering of the item.
            Vec2f translations = new Vec2f(-width / 2f + (20 * i), -(2 + 20));

            // Translates so the item is rendered in the correct place.
            GL11.glTranslatef(translations.x, translations.y, 0);
            {
                draw(itemStack);
            }
            GL11.glTranslatef(-translations.x, -translations.y, 0);
        }

        GlStateManager.scale(1 / scale, 1 / scale, 1);
    }

    /**
     * Draws the given itemstack.
     *
     * @param itemStack The itemstack to draw.
     */
    public void draw(ItemStack itemStack){
        // Draws the background rect.
        RenderHelper.drawRect(0, 0, 18, 18, 0x64000000);

        if(itemStack == null || itemStack.getItem() == null) return;

        // Draws the item.
        minecraft.getRenderItem().func_175042_a(itemStack, 1, 1);

        // Returns if zoom is off.
        if(!(Keyboard.isKeyDown(minecraft.gameSettings.ofKeyBindZoom.getKeyCode()) && minecraft.inGameHasFocus)) return;

        // Gets the enchantments of the itemstack.
        Map<Integer, Integer> enchantments = EnchantmentHelper.getEnchantments(itemStack);

        // The y offset.
        float y = 0;

        GL11.glEnable(GL11.GL_POLYGON_OFFSET_FILL);
        GL11.glPolygonOffset(1, -2000000);
        GlStateManager.scale(0.5, 0.5, 1);

        // Iterates through all the enchants.
        for(Integer enchantId : enchantments.keySet()){
            Enchantment enchantment = Enchantment.func_180306_c(enchantId);

            // Continue if the enchant isn't in the name map.
            if(!ENCHANT_NAMES.containsKey(enchantment)) continue;

            // The enchantment text.
            String text = String.format("%s \247a%s", ENCHANT_NAMES.get(enchantment), enchantments.get(enchantment));

            // Draws the enchant data.
            fontRenderer.drawStringWithShadow(text, 3, 3 + y, 0xFFFFFF);

            // Adds to the y offset.
            y += fontRenderer.FONT_HEIGHT + 2;
        }

        GlStateManager.scale(2, 2, 1);
        GL11.glPolygonOffset(1, 2000000);
        GL11.glDisable(GL11.GL_POLYGON_OFFSET_FILL);
    }
    
    private int getColour(){
        if(!nametags.color)
            return opt.isPresent() ? 0xFF00BFFF : 0xFFFFFFFF;

        return player.getName().equalsIgnoreCase("catgorl") ? 0xFFff7fd1 : opt.isPresent() ? 0xFF00BFFF : player.isSneaking() ? 0xFFFF4000 : 0xFFFFFFFF;
    }

    /**
     * @return the text displayed on the nametag.
     */
    private String getText(){
        return nametags.health ?
                String.format("%s \247a%s", opt.map(Friend::getName).orElse(player.getName()), this.getPlayerHealth())
                : opt.map(Friend::getName).orElse(player.getName());
    }

    /**
     * @return the location of the nametag in world space.
     */
    public Vector3f getWorldPosition(){
        float x = (float) (player.lastTickPosX
                + (player.posX - player.lastTickPosX) * minecraft.timer.renderPartialTicks
                - minecraft.getRenderManager().renderPosX);
        float y = (float) (player.lastTickPosY
                + (player.posY - player.lastTickPosY) * minecraft.timer.renderPartialTicks
                - minecraft.getRenderManager().renderPosY);
        float z = (float) (player.lastTickPosZ
                + (player.posZ - player.lastTickPosZ) * minecraft.timer.renderPartialTicks
                - minecraft.getRenderManager().renderPosZ);

        return new Vector3f(x, (float) (y + player.getEyeHeight() + 0.5 - (player.isSneaking() ? 0.2 : 0)), z);
    }

    /**
     * Checks if the mouse is over the nametag.
     *
     * @param x The x position of the user's cursor.
     * @param y The y position of the user's cursor.
     * @return if the mouse is over the nametag's bounds.
     */
    public boolean isMouseOver(int x, int y){
        return x >= this.x && y >= this.y && x <= this.x + width && y <= this.y + height;
    }

    /**
     * @return the player's health.
     */
    private double getPlayerHealth(){
        return Math.ceil(player.getHealth()) / 2;
    }

}
