package me.valkyrie.graphite.impl.module.visual.esp.modes;

import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.module.Mode;
import me.valkyrie.graphite.api.module.ModeManifest;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.api.util.render.OutlineShaderRenderPipeline;
import me.valkyrie.graphite.impl.event.render.RenderWorldEvent;
import me.valkyrie.graphite.impl.module.visual.esp.Esp;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

import java.awt.*;

import static org.lwjgl.opengl.GL11.*;

/**
 * @author Zeb.
 * @since 6/17/2017.
 */
@ModeManifest(label = "Tracers", parentClass = Esp.class)
public class Tracers extends Mode<Esp> {

    /**
     * The width of the tracer.
     */
    @Property(label = "Width")
    @Clamp(min = "1", max = "5")
    private int width = 1;

    @Property(label = "Spines")
    private boolean spines = false;

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(renderWorldEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(renderWorldEventHandler);
    }

    /**
     * Draws the tracers.
     */
    private EventHandler<RenderWorldEvent> renderWorldEventHandler = EventFactory.form(RenderWorldEvent.class, (event) -> {

        parent.getRenderableCollector().collect().forEach(renderable -> {

            Entity entity = (Entity) renderable;

            Color entityColor = parent.getColorFetcher().fetch(renderable);

            double x = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * minecraft.timer.renderPartialTicks
                    - minecraft.getRenderManager().renderPosX;
            double y = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * minecraft.timer.renderPartialTicks
                    - minecraft.getRenderManager().renderPosY;
            double z = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * minecraft.timer.renderPartialTicks
                    - minecraft.getRenderManager().renderPosZ;

            glPushMatrix();

            // Sets the line width.
            glLineWidth(width);

            // Enables line smoothing.
            glEnable(GL_LINE_SMOOTH);

            // Changes constants to get correct render.
            glDisable(GL_TEXTURE_2D);
            GlStateManager.disableLighting();
            GlStateManager.disableDepth();

            // Sets color.
            glColor3f(entityColor.getRed() / 255f, entityColor.getGreen() / 255f, entityColor.getBlue() / 255f);


            // Begins drawing the lines.
            glBegin(GL_LINE_STRIP);

            // Adds the basic verticies.
            glVertex3d(0,player.getEyeHeight(),0);
            glVertex3d(x,y,z);

            // Adds spines if enabled.
            if(spines)
                glVertex3d(x,y + entity.height,z);

            // Finishes the render.
            glEnd();

            // Restore states.
            GlStateManager.enableDepth();
            glEnable(GL_TEXTURE_2D);
            GlStateManager.enableLighting();

            glPopMatrix();
        });

    });

}
