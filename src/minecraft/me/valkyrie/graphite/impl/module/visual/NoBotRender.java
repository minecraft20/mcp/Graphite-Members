package me.valkyrie.graphite.impl.module.visual;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.helper.AntibotHelper;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.render.RenderEntityEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

@ModuleManifest(label = "No_Bot_Render", type = Category.VISUAL, visible = false)
public class NoBotRender extends Module {

    private EventHandler<RenderEntityEvent> renderEntityEventHandler = new EventHandler<RenderEntityEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return RenderEntityEvent.class;
        }

        @Override
        public void handle(RenderEntityEvent event){
            if(event.getEntity() instanceof EntityPlayer && AntibotHelper.isEntityBot(event.getEntity()))
                event.cancel();
        }

    };

    @Override
    public void enable(){
        EventManager.INSTANCE.register(renderEntityEventHandler);
    }

    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(renderEntityEventHandler);
    }

}
