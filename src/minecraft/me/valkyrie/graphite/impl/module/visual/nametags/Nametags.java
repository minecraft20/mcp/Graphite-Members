package me.valkyrie.graphite.impl.module.visual.nametags;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.helper.AntibotHelper;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Increment;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.render.RenderNametagEvent;
import me.valkyrie.graphite.impl.event.render.RenderScreenEvent;
import me.valkyrie.graphite.impl.module.visual.nametags.impl.Nametag;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayer;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Draws new nametags above all players.
 *
 * @author Zeb.
 * @since 5/1/2017
 */
@ModuleManifest(label = "Nametags", color = 0xFF69dbb3, type = Category.VISUAL, visible = false)
public class Nametags extends Module {

    /**
     * The scale of the nametag.
     */
    @Property(label = "Scale", aliases = {"Size"}, description = "The scale of the nametag.")
    @Clamp(min = "0.1", max = "10")
    @Increment("0.1")
    public float scale = 1;

    /**
     * If the health should be shown on the nametag.
     */
    @Property(label = "Health", aliases = {"Hearts"}, description = "If the health should be shown on the nametag.")
    public boolean health = true;

    /**
     * If the nametag health should use color.
     */
    @Property(label = "Color", description = "If the nametag should use color.")
    public boolean color = true;

    /**
     * If items should be on the nametags.
     */
    @Property(label = "Items", aliases = {"Armor"}, description = "If items should be on the nametags.")
    public boolean items = true;

    /**
     * Map that stores all of the nametags.
     */
    private Map<EntityPlayer, Nametag> nametagMap = new HashMap<>();

    /**
     * Renders all of the nametags.
     */
    private final EventHandler<RenderScreenEvent> renderScreenEventHandler = new EventHandler<RenderScreenEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return RenderScreenEvent.class;
        }

        /**
         * Renders all of the nametags.
         */
        @Override
        public void handle(RenderScreenEvent event){
            // Remakes the map.
            validatePlayers();

            // The games hud scale.
            int hudScale = minecraft.gameSettings.guiScale;

            // Ticks used for interpolation.
            float partialTicks = minecraft.timer.renderPartialTicks;

            // Changes the hud scale.
            minecraft.gameSettings.guiScale = 2;

            // Switches to 3d projection.
            minecraft.entityRenderer.setupCameraTransform(partialTicks, 0);

            // Changes the hud scale back.
            minecraft.gameSettings.guiScale = hudScale;

            // Draws all of the nametags.
            nametagMap.values().forEach(Nametag::update);

            // Changes the hud scale.
            minecraft.gameSettings.guiScale = 2;

            // Switches to 2d projection.
            minecraft.entityRenderer.setupOverlayRendering();

            // Changes the hud scale back.
            minecraft.gameSettings.guiScale = hudScale;

            // Draws all of the nametags.
            nametagMap.values().forEach(Nametag::draw);

            // Switches to normal rendering.
            minecraft.entityRenderer.setupOverlayRendering();
        }

    };

    /**
     * Stops player nametags from being rendered.
     */
    private final EventHandler<RenderNametagEvent> renderNametagEventHandler = new EventHandler<RenderNametagEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return RenderNametagEvent.class;
        }

        /**
         * Removes nametags.
         */
        @Override
        public void handle(RenderNametagEvent event){
            event.setCancelled(event.getEntity() instanceof EntityPlayer);
        }

    };

    /**
     * Removes all unused player and adds all of the new ones.
     */
    private void validatePlayers(){

        // Resets the map.
        nametagMap = null;
        nametagMap = new HashMap<>();

        // Adds the players.
        minecraft.world.playerEntities.stream()
                .sorted(Comparator.comparingDouble(entity -> player.getDistanceToEntity(entity)))
                .filter(p -> !(p instanceof EntityPlayerSP) && !AntibotHelper.isEntityBot(p))
                .forEach(player -> nametagMap.put(player, new Nametag(player)));
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(renderScreenEventHandler);
        EventManager.INSTANCE.register(renderNametagEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(renderScreenEventHandler);
        EventManager.INSTANCE.unregister(renderNametagEventHandler);
    }

}
