package me.valkyrie.graphite.impl.module.visual;

import me.valkyrie.graphite.api.event.Event;
import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMoveEvent;
import me.valkyrie.graphite.impl.event.render.RenderHandEvent;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.server.S08PacketPlayerPosLook;
import net.minecraft.util.Vec3;

/**
 * @author Zeb.
 * @author catgorl
 * @since 6/19/17
 */
@ModuleManifest(label = "Freecam", color = 0xFF9578a3, type = Category.VISUAL)
public class Freecam extends Module {

	/**
	 * How fast you fly.
	 */
	@Property(label = "Speed", aliases = { "Multiplier" }, description = "How fast you fly.")
	@Clamp(min = "1", max = "5")
	private double speed = 1;

	/**
	 * The position of the player.
	 */
	private Vec3 position;

	/**
	 * Handler for movement.
	 */
	private EventHandler<PlayerMoveEvent> playerMoveEventHandler = EventFactory.form(PlayerMoveEvent.class,
			(rawEvent) -> {

				PlayerMoveEvent event = (PlayerMoveEvent) rawEvent;

				minecraft.player.noClip = true;

				// Handles vertical movement.
				double y = (minecraft.gameSettings.keyBindJump.getIsKeyPressed() ? .5 : 0)
						+ (minecraft.gameSettings.keyBindSneak.getIsKeyPressed() ? -.5 : 0);

				// Sets the speed to the base walking speed.
				double speed = this.speed * 0.8;

				// Stops the player from moving if the player isn't moving.
				if (player.moveForward == 0 && player.moveStrafing == 0)
					speed = 0;

				// The horizontal velocities for the player.
				double x = -(Math.sin(player.getDirection()) * speed);
				double z = (Math.cos(player.getDirection()) * speed);

				// Sets the horizontal movement.
				event.x = x;
				event.y = player.motionY = y * this.speed;
				event.z = z;

			});

	/**
	 * Handler for outgoing packets.
	 */
	private EventHandler<PacketEvent> packetEventHandler = EventFactory.form(PacketEvent.class, (rawEvent) -> {

		PacketEvent event = (PacketEvent) rawEvent;

		if (event.getPacket() instanceof S08PacketPlayerPosLook) {

			// The packet being received.
			S08PacketPlayerPosLook packet = (S08PacketPlayerPosLook) event.getPacket();

			position = new Vec3(packet.getX(), packet.getY(), packet.getZ());

            event.cancel();
		}

		// Returns if the packet isn't a player packet or if the event type is
		// post or if the player is in a liquid, sneaking, or falling.
		if (!(event.getPacket() instanceof C03PacketPlayer))
			return;

		// The packet being sent.
		C03PacketPlayer packet = (C03PacketPlayer) event.getPacket();

		packet.setX(position.xCoord);
		packet.setY(position.yCoord);
		packet.setZ(position.zCoord);
	});

	/**
	 * Handler for hand rendering.
	 */
	private EventHandler<RenderHandEvent> renderHandEventHandler = EventFactory.form(RenderHandEvent.class,
			Event::cancel);

	/**
	 * Registers the listeners to the event system.
	 */
	@Override
	public void enable() {

		// Returns if player is null.
		if (player == null) {
			setRunning(false);
			return;
		}

		// Sets the starting position.
		position = player.getPositionVector();
		
		EntityOtherPlayerMP e = new EntityOtherPlayerMP(minecraft.world, player.getGameProfile());
		e.clonePlayer(player, true);
		e.copyLocationAndAnglesFrom(player);
		minecraft.world.addEntityToWorld(-1337, e);

		EventManager.INSTANCE.register(packetEventHandler);
		EventManager.INSTANCE.register(renderHandEventHandler);
		EventManager.INSTANCE.register(playerMoveEventHandler);
	}

	/**
	 * Unregisters the listeners from the event system.
	 */
	@Override
	public void disable() {
		minecraft.world.removeEntityFromWorld(-1337);
		player.setPosition(position.xCoord, position.yCoord, position.zCoord);
		EventManager.INSTANCE.unregister(packetEventHandler);
		EventManager.INSTANCE.unregister(renderHandEventHandler);
		EventManager.INSTANCE.unregister(playerMoveEventHandler);
	}

}
