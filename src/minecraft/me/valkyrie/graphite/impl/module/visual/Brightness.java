package me.valkyrie.graphite.impl.module.visual;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.player.PlayerUpdateEvent;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.world.World;

@ModuleManifest(label = "Brightness", type = Category.VISUAL, visible = false)
public class Brightness extends Module {

    private EventHandler<PlayerUpdateEvent> brightnessHandler = new EventHandler<PlayerUpdateEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerUpdateEvent.class;
        }

        @Override
        public void handle(PlayerUpdateEvent event){
            if(event.getEventType() == Type.POST) return;

            andThereShallBeLight(minecraft.world);
        }

    };

    @Override
    public void enable(){
        EventManager.INSTANCE.register(brightnessHandler);
    }

    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(brightnessHandler);
    }


    private void andThereShallBeLight(World world){
        float brightness = 1;
        for(int i = 0; i < world.provider.getLightBrightnessTable().length; i++){
            world.provider.getLightBrightnessTable()[i] = brightness;
        }

    }
}
