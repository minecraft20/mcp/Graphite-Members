package me.valkyrie.graphite.impl.module.visual.storageesp.modes;

import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.module.Mode;
import me.valkyrie.graphite.api.module.ModeManifest;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.api.util.render.OutlineShaderRenderPipeline;
import me.valkyrie.graphite.impl.event.render.RenderWorldEvent;
import me.valkyrie.graphite.impl.module.visual.storageesp.StorageEsp;

/**
 * @author Zeb.
 * @since 6/17/2017.
 */
@ModeManifest(label = "Outline", parentClass = StorageEsp.class)
public class Outline extends Mode<StorageEsp> {

    /**
     * The {@link OutlineShaderRenderPipeline} used to render all of the outlines.
     */
    private static OutlineShaderRenderPipeline renderPipeline;

    @Property(label = "Radius")
    @Clamp(min = "1", max = "10")
    private int radius = 1;

    @Property(label = "Resolution")
    @Clamp(min = "1", max = "5")
    private int resolution = 1;

    @Property(label = "Glow")
    private boolean glow = false;

    public Outline(){
        renderPipeline = new OutlineShaderRenderPipeline.Builder()
                .withRadius(radius)
                .withResolution(resolution)
                .withObjectRenderer(parent.getRenderableCollector())
                .withColorFetcher(parent.getColorFetcher())
                .withGlow(glow).create();
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(renderWorldEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(renderWorldEventHandler);
    }

    private EventHandler<RenderWorldEvent> renderWorldEventHandler = EventFactory.form(RenderWorldEvent.class, (event) -> {
        renderPipeline.setGlow(glow);
        renderPipeline.setRadius(radius);
        renderPipeline.setResolution(resolution);

        renderPipeline.draw();
    });

}
