package me.valkyrie.graphite.impl.module.visual.search;

import com.sun.org.apache.regexp.internal.RE;
import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.render.OutlineShaderRenderPipeline;
import me.valkyrie.graphite.api.util.render.Renderable;
import me.valkyrie.graphite.impl.event.render.RenderWorldEvent;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.BlockPos;
import net.minecraft.world.ChunkCoordIntPair;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.storage.ExtendedBlockStorage;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Zeb.
 * @since 6/18/2017.
 */
@ModuleManifest(label = "Search", color = 0xFFf7f096, type = Category.VISUAL)
public class Search extends Module {

    /**
     * A map of all of the blocks that the search knows of.
     */
    private Map<BlockPos, Integer> blockMap = new HashMap<>();

    /**
     * A map with all of the blocks that the search should render.
     */
    private Map<Block, Integer> BLOCK_COLOR_REGISTRY = new HashMap<>();

    /**
     * The render pipeline for the items.
     */
    private OutlineShaderRenderPipeline outlineShaderRenderPipeline;

    /**
     * The render event handler.
     */
    private EventHandler<RenderWorldEvent> renderWorldEventHandler = EventFactory.form(RenderWorldEvent.class, (event) -> outlineShaderRenderPipeline.draw());

    public Search(){
        BLOCK_COLOR_REGISTRY.put(Blocks.diamond_ore, 0xFF00CCCF);
        BLOCK_COLOR_REGISTRY.put(Blocks.gold_ore, 0xFFEFD14A);
        BLOCK_COLOR_REGISTRY.put(Blocks.iron_ore, 0xFF7F615F);
        BLOCK_COLOR_REGISTRY.put(Blocks.emerald_ore, 0xFF74DB87);
        BLOCK_COLOR_REGISTRY.put(Blocks.coal_ore, 0xFF1F1F1F);

        this.outlineShaderRenderPipeline = new OutlineShaderRenderPipeline.Builder()
                .withGlow(false)
                .withRadius(1)
                .withResolution(1)
                .withColorFetcher((renderable) ->
                    new Color(blockMap.get((BlockPos) renderable)))
                .withObjectRenderer(() -> {

                    Set<Renderable> renderables = new HashSet<>();

                    renderables.addAll(blockMap.keySet());

                    return renderables;

                }).create();
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(renderWorldEventHandler);

        blockMap.clear();

        Chunk centerChunk = minecraft.world.getChunkFromBlockCoords(player.getPosition());

        int renderDistance = minecraft.gameSettings.renderDistanceChunks;

        for(int x = -renderDistance; x < renderDistance; x++){
            for(int z = -renderDistance; z < renderDistance; z++){

                int cX = centerChunk.xPosition + x;
                int cZ = centerChunk.zPosition + z;

                Chunk chunk = minecraft.world.getChunkFromChunkCoords(cX, cZ);

                if(!chunk.isLoaded())
                    continue;

                scanChunkStorage(chunk);
            }
        }
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(renderWorldEventHandler);
    }

    /**
     * Puts the block given into the block map.
     *
     * @param pos The block's position.
     * @param block The block type.
     */
    private void add(BlockPos pos, Block block){
        blockMap.put(pos, BLOCK_COLOR_REGISTRY.get(block));
    }

    /**
     * Checks if the search is looking.
     *
     * @param type The block type.
     * @return if the search module is looking for the block type.
     */
    private boolean isLookingFor(Block type){
        return BLOCK_COLOR_REGISTRY.containsKey(type);
    }

    /**
     * Parse the chunk's block storage arrays for blocks
     * that we are searching for.
     *
     * @param chunk chunk to scan
     */
    private void scanChunkStorage(final Chunk chunk) {
        // get the array of block storages inside of the chunk
        final ExtendedBlockStorage[] blockStorages = chunk.getBlockStorageArray();

        // walk through all block storages (top down) registered in the chunk
        for (int storageIndex = blockStorages.length - 1; storageIndex >= 0; storageIndex--) {
            // get the storage from it's index
            final ExtendedBlockStorage storage = blockStorages[storageIndex];

            // check if the storage is null (shouldn't be)
            if (storage == null || !fastStorageDataScan(storage.getData()))
                continue;

            // get the coordinate pair of the chunk so we can get the proper coordinates
            final ChunkCoordIntPair coordPair = chunk.getChunkCoordIntPair();

            // iterate all block positions and check their type
            for (int x = 0; x < 16; x++) {
                for (int y = 0; y < 16; y++) {
                    for (int z = 0; z < 16; z++) {
                        // get the block at the position relative to the chunk
                        final Block block = storage.getBlockByExtId(x, y, z);

                        // check if we're searching for this block
                        if (isLookingFor(block))
                            add(new BlockPos(coordPair.getXStart() + x, storageIndex * 16 + y, coordPair.getZStart() + z), block);
                    }
                }
            }
        }
    }

    /**
     * Perform a quick scan on the data provided by the chunk
     * block storage to see if they have a target block
     * initially before iterating the whole chunk's block storage.
     *
     * @param chunkData data passed from block storage
     * @return true if contains block we scan for; false otherwise
     */
    private boolean fastStorageDataScan(final char[] chunkData) {
        // walk through every block state provided
        for (final char data : chunkData) {
            // get the block state from the data
            final IBlockState state = (IBlockState) Block.BLOCK_STATE_IDS.getByValue(data);
            if (state == null)
                continue;

            // check if we're searching for the block in this state
            if (isLookingFor(state.getBlock()))
                return true;
        }

        return false;
    }
}
