package me.valkyrie.graphite.impl.module.visual;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.Friend;
import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import me.valkyrie.graphite.api.util.render.font.TTFFontRenderer;
import me.valkyrie.graphite.impl.Graphite;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import me.valkyrie.graphite.impl.event.player.PlayerChatEvent;
import net.minecraft.event.ClickEvent;
import net.minecraft.network.play.client.C01PacketChatMessage;
import net.minecraft.network.play.server.S02PacketChat;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import java.net.URL;
import java.util.*;

/**
 * Replaces your friends name in chat with their alias.
 *
 * @author Zeb.
 * @author Camber.
 * @since 5/3/2017
 */
@ModuleManifest(label = "NameProtect", type = Category.VISUAL, visible = false)
public class NameProtect extends Module {

    /**
     * Handles incoming chat packets.
     */
    private final EventHandler<PacketEvent> packetEventHandler = new EventHandler<PacketEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PacketEvent.class;
        }

        /**
         * Replaces friend's name with the correct ones.
         * @param event The event called.
         */
        @Override
        public void handle(PacketEvent event){

            // Returns if packet or event type is wrong.
            if(!(event.getPacket() instanceof S02PacketChat) || event.getEventType() == Type.POST || minecraft.world == null)
                return;

            // The packet being received.
            S02PacketChat packet = (S02PacketChat) event.getPacket();

            if(packet.func_179841_c() != 1)
                return;

            // Iterator for the message.
            Iterator<IChatComponent> iterator = packet.func_148915_c().iterator();

            // The dumby message.
            IChatComponent chatComponent = new ChatComponentText("");

            // Iterates through all of the components.
            while(iterator.hasNext()){

                // The current component.
                IChatComponent iChatComponent = iterator.next();

                // Replaces the names inside of the original component.
                ChatComponentText c = new ChatComponentText(color(iChatComponent.getUnformattedTextForChat(), iChatComponent.getFormattedText()));

                // Copies the style.
                c.setChatStyle(iChatComponent.getChatStyle());

                // Appends the component to the dumby message.
                chatComponent.appendSibling(c);
            }

            // Cancels the event.
            event.setCancelled(true);

            // Adds the message.
            minecraft.ingameGUI.getChatGUI().printChatMessage(chatComponent);
        }

    };

    /**
     * Handles outgoing chat messages.
     */
    private final EventHandler<PlayerChatEvent> chatHandler = new EventHandler<PlayerChatEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerChatEvent.class;
        }

        /**
         * Handles changing friend names to actual names.
         */
        @Override
        public void handle(PlayerChatEvent event){

            // The message said.
            String message = event.getMessage();

            // Returns if message is null.
            if(message == null)
                return;

            // Returns if message is a command.
            if(Graphite.INSTANCE.getCommandManager().valid(message))
                return;

            // Handles name swapping.
            for(Friend friend : Graphite.INSTANCE.getFriendManager().getFriendList())
                if(message.contains(friend.getName()))
                    message = message.replaceAll(friend.getName(), friend.getPlayerName());

            // Cancels the chat event.
            event.setCancelled(true);

            // Sends the message.
            player.sendQueue.addToSendQueue(new C01PacketChatMessage(message));
        }
    };

    /**
     * Colors the given message will all player names.
     *
     * @param message The message said.
     * @return The colored message.
     */
    public String color(String message, String formatted){
        // The message said.
        String main = message;

        // Loops through friends and replaces their names.
        for(Friend friend : Graphite.INSTANCE.getFriendManager().getFriendList()){

            // Continues if message doesn't contain name.
            if(!main.contains(friend.getPlayerName())) continue;

            // Gets the formatting characters.
            List<Character> characters = getLastFormattingCharacters(formatted.split(friend.getPlayerName())[0]);

            // The formatted text.
            String formatting = "";

            // Adds the formatting characters.
            for(Character character : characters)
                formatting += "\247" + character;

            // Changes main to colored main.
            main = main.replaceAll(friend.getPlayerName(), (friend.getPlayerName().equalsIgnoreCase("catgorl") ? "\247d" : "\247b") + friend.getName() + formatting);
        }

        // Returns the main,
        return main;
    }

    /**
     * Gets the last formatting characters for color and text style.
     *
     * @param text The message to get the formatting of.
     * @return The last formatting characters.
     */
    public List<Character> getLastFormattingCharacters(String text){

        // The length of the text.
        int length = text.length();

        // The color character.
        char color = 'f';

        // Array of empty characters.
        List<Character> style = new ArrayList<>();

        // Loops through the text.
        for(int i = 0; i < length; i++){
            // The character at the index of 'i'.
            char character = text.charAt(i);

            // The previous character.
            char previous = i > 0 ? text.charAt(i - 1) : '.';

            // Continues if the previous color was the color invoker.
            if(previous == '\247') continue;

            // Sets the color if the character is the color invoker and the character index is less than the length.
            if(character == '\247' && i < length){

                // The color index of the character after the current character.
                int index = "0123456789abcdefklmnor".indexOf(text.toLowerCase(Locale.ENGLISH).charAt(i + 1));

                // If the color character index is of the normal color invoking characters.
                if(index < 16){
                    // Resets all the styles.
                    style.clear();

                    // Clamps the index just to be safe in case an odd character somehow gets in here.
                    if(index < 0 || index > 15) index = 15;

                    // Sets the color character.
                    color = "0123456789abcdef".charAt(index);
                } else if(index == 16)
                    style.add('k');
                else if(index == 17)
                    style.add('l');
                else if(index == 18)
                    style.add('m');
                else if(index == 19)
                    style.add('n');
                else if(index == 20)
                    style.add('o');
                else if(index == 21){
                    // Resets all the styles.
                    style.clear();
                    color = 'f';
                }
            }
        }

        // A list of all of the characters.
        List<Character> characters = new ArrayList<>();

        // Adds the color character.
        characters.add(color);

        // Adds the style characters.
        characters.addAll(style);

        return characters;
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(packetEventHandler);
        EventManager.INSTANCE.register(chatHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(packetEventHandler);
        EventManager.INSTANCE.unregister(chatHandler);
    }

}
