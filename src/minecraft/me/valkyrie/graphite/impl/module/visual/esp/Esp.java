package me.valkyrie.graphite.impl.module.visual.esp;

import me.valkyrie.graphite.api.helper.AntibotHelper;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.property.Child;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.api.util.render.Renderable;
import me.valkyrie.graphite.api.util.render.RenderableCollector;
import me.valkyrie.graphite.api.util.render.RenderableColorFetcher;
import me.valkyrie.graphite.impl.Graphite;
import me.valkyrie.graphite.impl.module.visual.esp.modes.Outline;
import me.valkyrie.graphite.impl.module.visual.esp.modes.Tracers;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityGolem;
import net.minecraft.entity.passive.EntityAmbientCreature;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntitySquid;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Zeb.
 * @since 6/17/2017.
 */
@ModuleManifest(label = "Esp", color = 0x88c9ef, type = Category.VISUAL,
        modes = {Outline.class, Tracers.class}, visible = false)
public class Esp extends Module {

    /**
     * If the esp should render on players.
     */
    @Property(label = "Players", aliases = "Humans", description = "If the esp should render on players.")
    private boolean players = true;

    /**
     * The red value of the esp for players.
     */
    @Child("Players")
    @Property(label = "Red", description = "The red value of the esp for players.")
    private int playerRed = 80;

    /**
     * The green value of the esp for players.
     */
    @Child("Players")
    @Property(label = "Green", description = "The green value of the esp for players.")
    private int playerGreen = 230;

    /**
     * The blue value of the esp for players.
     */
    @Child("Players")
    @Property(label = "Blue", description = "The blue value of the esp for players.")
    private int playerBlue = 180;

    /**
     * If the friend role should affect the esp.
     */
    @Child("Players")
    @Property(label = "Friends", description = "If the friend role should affect the esp.")
    private boolean friends = true;

    /**
     * The red value of the esp for players.
     */
    @Child("Friends")
    @Property(label = "Red", description = "The red value of the esp for players.")
    private int friendRed = 80;

    /**
     * The green value of the esp for players.
     */
    @Child("Friends")
    @Property(label = "Green", description = "The green value of the esp for players.")
    private int friendGreen = 220;

    /**
     * The blue value of the esp for players.
     */
    @Child("Friends")
    @Property(label = "Blue", description = "The blue value of the esp for players.")
    private int friendBlue = 220;

    @Property(label = "Mobs", description = "If the esp should render on mobs.")
    private boolean mobs = false;

    /**
     * If the passive role of entities should affect the esp.
     */
    @Child("Mobs")
    @Property(label = "Passive", description = "If the passive role of entities should affect the esp.")
    private boolean passive = true;

    /**
     * The red value of the esp for passive mobs.
     */
    @Child("Passive")
    @Property(label = "Red", description = "The red value of the esp for passive mobs.")
    private int passiveRed = 230;

    /**
     * The green value of the esp for passive mobs.
     */
    @Child("Passive")
    @Property(label = "Green", description = "The green value of the esp for passive mobs.")
    private int passiveGreen = 150;

    /**
     * The blue value of the esp for  passive mobs.
     */
    @Child("Passive")
    @Property(label = "Blue", description = "The blue value of the esp for passive mobs.")
    private int passiveBlue = 230;

    /**
     * If the passive role of entities should affect the esp.
     */
    @Child("Mobs")
    @Property(label = "Hostile", description = "If the passive role of entities should affect the esp.")
    private boolean hostile = true;

    /**
     * The red value of the esp for hostile mobs.
     */
    @Child("Hostile")
    @Property(label = "Red", description = "The red value of the esp for hostile mobs.")
    private int hostileRed = 200;

    /**
     * The green value of the esp for hostile mobs.
     */
    @Child("Hostile")
    @Property(label = "Green", description = "The green value of the esp for hostile mobs.")
    private int hostileGreen = 70;

    /**
     * The blue value of the esp for hostile mobs.
     */
    @Child("Hostile")
    @Property(label = "Blue", description = "The blue value of the esp for hostile mobs.")
    private int hostileBlue = 70;

    /**
     * If the passive role of entities should affect the esp.
     */
    @Child("Mobs")
    @Property(label = "Neutral", description = "If the passive role of entities should affect the esp.")
    private boolean neutralMobs = false;

    /**
     * The red value of the esp for neutral mobs.
     */
    @Child("Neutral")
    @Property(label = "Red", description = "The red value of the esp for neutral mobs.")
    private int neutralRed = 230;

    /**
     * The green value of the esp for neutral mobs.
     */
    @Child("Neutral")
    @Property(label = "Green", description = "The green value of the esp for neutral mobs.")
    private int neutralGreen = 230;

    /**
     * The blue value of the esp for neutral mobs.
     */
    @Child("Neutral")
    @Property(label = "Blue", description = "The blue value of the esp for neutral mobs.")
    private int neutralBlue = 130;

    /**
     * If the items should affect the esp.
     */
    @Property(label = "Items", description = "If the items should affect the esp.")
    private boolean items = false;

    /**
     * The red value of the esp for items.
     */
    @Child("Items")
    @Property(label = "Red", description = "The red value of the esp for items.")
    private int itemRed = 80;

    /**
     * The green value of the esp for items.
     */
    @Child("Items")
    @Property(label = "Green", description = "The green value of the esp for items.")
    private int itemGreen = 115;

    /**
     * The blue value of the esp for items.
     */
    @Child("Items")
    @Property(label = "Blue", description = "The blue value of the esp for items.")
    private int itemBlue = 160;

    /**
     * The renderable collector for the renderers.
     */
    private RenderableCollector renderableCollector;

    /**
     * The color fetcher for the renderers.
     */
    private RenderableColorFetcher colorFetcher;

    public Esp(){
        this.renderableCollector = new RenderableCollector() {
            @Override
            public Set<Renderable> collect(){

                Set<Renderable> renderables = new HashSet<>();

                for(Entity entity : minecraft.world.loadedEntityList)
                    if(valid(entity))
                        renderables.add(entity);


                return renderables;
            }
        };

        this.colorFetcher = renderable -> get((Entity) renderable);
    }

    public RenderableCollector getRenderableCollector(){
        return renderableCollector;
    }

    public RenderableColorFetcher getColorFetcher(){
        return colorFetcher;
    }

    /**
     * @param entity The entity to be checked.
     * @return If the entity should be drawn on the esp.
     */
    public boolean valid(Entity entity){
        if(!(entity instanceof EntityLivingBase) && !(entity instanceof EntityItem) || entity instanceof EntityPlayerSP) return false;

        switch(Type.getType(entity)){
            case ITEM:
                return items;
            case MOB_PASSIVE:
                return mobs && passive;
            case MOB_HOSTILE:
                return mobs && hostile;
            case MOB_NEUTRAL:
                return mobs && players;
            case PLAYER:
                return players && !AntibotHelper.isEntityBot(entity);
        }

        // Should never get called.
        return true;
    }

    /**
     * Gets the color of the entity type.
     *
     * @param entity The entity to get the color of.
     * @return The color of the entity type.
     */
    public Color get(Entity entity){
        switch(Type.getType(entity)){
            case ITEM:
                return new Color(itemRed, itemGreen, itemBlue);
            case MOB_PASSIVE:
                return new Color(passiveRed, passiveGreen, passiveBlue);
            case MOB_HOSTILE:
                return new Color(hostileRed, hostileGreen, hostileGreen);
            case MOB_NEUTRAL:
                return new Color(neutralRed, neutralGreen, neutralBlue);
            case PLAYER:
                // The default color of players.
                Color playerColor = new Color(playerRed, playerGreen, playerBlue);

                // Changes color if the roles are enabled.
                if(Graphite.INSTANCE.getFriendManager().isFriend(entity.getUniqueID()))
                    return friends ? new Color(friendRed, friendGreen, friendBlue) : playerColor;

                // Return the default color.
                return playerColor;
        }

        // Return white, should never get here.
        return Color.WHITE;
    }

    /**
     * The type of the entity.
     */
    public enum Type {

        PLAYER,
        MOB_PASSIVE,
        MOB_NEUTRAL,
        MOB_HOSTILE,
        ITEM;

        /**
         * Gets the type of the given entity.
         *
         * @param entity The entity to get the type of.
         * @return The type of the given entity.
         */
        public static Type getType(Entity entity){
            // Returns the item type if the entity is an item.
            if(entity instanceof EntityItem) return ITEM;

            // Return the player type if the entity is a player.
            if(entity instanceof EntityPlayer) return PLAYER;

            // Returns hostile if the mob is a boss.
            if(entity instanceof EntityWither || entity instanceof EntityDragon) return MOB_HOSTILE;

            // Handles behavior of wolves because they can be both hostile and passive.
            if(entity instanceof EntityWolf)
                return ((EntityWolf) entity).isAngry() ? MOB_HOSTILE : (((EntityWolf) entity).isTamed() ? MOB_PASSIVE : MOB_NEUTRAL);

            // Returns true because golems are neutral.
            if(entity instanceof EntityGolem)
                return MOB_NEUTRAL;

            return (entity instanceof EntityAnimal || entity instanceof EntityAmbientCreature || entity instanceof EntitySquid) ? MOB_PASSIVE : MOB_HOSTILE;
        }

    }

}
