package me.valkyrie.graphite.impl.module.self.retard.modes;

import me.valkyrie.graphite.api.module.Mode;
import me.valkyrie.graphite.impl.module.self.retard.Retard;

/**
 * @author Zeb.
 * @since 6/2/2017.
 */
public abstract class AbstractRetardMode extends Mode<Retard> {

    /**
     * @return the for the player to spoof.
     */
    public abstract float[] rotations();

}
