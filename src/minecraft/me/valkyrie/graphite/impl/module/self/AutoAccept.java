package me.valkyrie.graphite.impl.module.self;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import me.valkyrie.graphite.impl.Graphite;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import net.minecraft.block.Block;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.network.play.server.S02PacketChat;
import net.minecraft.util.BlockPos;

/**
 * Automatically accepts teleport requests from friends.
 *
 * @author Camber.
 * @since 4/25/2017
 */
@ModuleManifest(label = "Auto_Accept", color = 0xFF93dd1c, type = Category.SELF, visible = false)
public class AutoAccept extends Module {

    /**
     * The number of messages to block.
     */
    private int messagesToBlock = 0;

    /**
     * Handles automatically accepting teleport requests.
     */
    private EventHandler<PacketEvent> packetEventHandler = new EventHandler<PacketEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PacketEvent.class;
        }

        /**
         * Automatically accepts teleport requests.
         */
        @Override
        public void handle(PacketEvent event){
            // Returns if packet wasn't a chat packet.
            if(!(event.getPacket() instanceof S02PacketChat && event.getEventType() == Type.PRE))
                return;

            // The packet's message.
            String message = ((S02PacketChat) event.getPacket()).func_148915_c().getUnformattedText();

            // Blocks a message is block counter is higher than 0.
            if(messagesToBlock > 0){
                event.setCancelled(true);
                messagesToBlock--;
            }

            // Automatically accept's if the player is a friend.
            Graphite.INSTANCE.getFriendManager().getFriendList().stream()
                    .filter(friend -> (message.contains(friend.getPlayerName()) || message.contains(friend.getName())) && message.contains("has requested to teleport"))
                    .forEach(friend -> {
                        // Accepts the request.
                        player.sendChatMessage("/tpaccept");

                        // Cancels the event.
                        event.setCancelled(true);

                        // Displays teleport message.
                        clientChatMsg()
                                .appendText("Your friend ")
                                .appendText(friend.getName(), ChatColor.GRAY)
                                .appendText(" has teleported to you.").send();

                        // Sets block counter.
                        messagesToBlock = 4;
                    });

        }

    };

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(packetEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(packetEventHandler);
    }

}
