package me.valkyrie.graphite.impl.module.self;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import net.minecraft.block.Block;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.util.BlockPos;

/**
 * Automatically switches to the best tool when breaking blocks.
 *
 * @author Zeb.
 * @since 4/23/2017
 */
@ModuleManifest(label = "Autotool", color = 0xFF3be5cc, type = Category.SELF)
public class Autotool extends Module {

    /**
     * Handles the switching of items to the stronger item.
     */
    private EventHandler<PacketEvent> packetEventHandler = new EventHandler<PacketEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PacketEvent.class;
        }

        /**
         * Switches to the strongest item in the player's hotbar when a digging packet is sent.
         */
        @Override
        public void handle(PacketEvent event){
            // Returns if the packet isn't a digging packet and the event type isn't a pre event.
            if(!(event.getPacket() instanceof C07PacketPlayerDigging && event.getEventType() == Type.PRE))
                return;

            // The packet being sent in the form of a {@link C07PacketPlayerDigging}.
            C07PacketPlayerDigging packet = (C07PacketPlayerDigging) event.getPacket();

            // If the packet is starting the block break.
            if(packet.func_180762_c() == C07PacketPlayerDigging.Action.START_DESTROY_BLOCK)
                autotool(packet.func_179715_a());
        }

    };

    /**
     * Switches to the correct tool to mine the block.
     */
    private static void autotool(BlockPos position){
        // The block type being broken.
        Block block = minecraft.world.getBlockState(position).getBlock();

        // The item index of the strongest item in the player's hotbar.
        int itemIndex = getStrongestItem(block);

        // Returns if item index is less than 0 (Invalid).
        if(itemIndex < 0) return;

        // The item strength of the strongest item.
        float itemStrength = getStrengthAgainstBlock(block, minecraft.player.inventory.mainInventory[itemIndex]);

        // Returns if the held item is the strongest.
        if(minecraft.player.getHeldItem() != null && getStrengthAgainstBlock(block, minecraft.player.getHeldItem()) >= itemStrength)
            return;

        // Sets the held item.
        minecraft.player.inventory.currentItem = itemIndex;
        minecraft.player.sendQueue.addToSendQueue(new C09PacketHeldItemChange(itemIndex));
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(packetEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(packetEventHandler);
    }

    /**
     * Gets strongest item against the given block.
     *
     * @param block The block to get the strength against.
     * @return the strongest item in the player's hotbar.
     */
    private static int getStrongestItem(Block block){
        // The strength of the strongest item.
        float strength = Float.NEGATIVE_INFINITY;

        // The strongest item index.
        int strongest = -1;

        // Iterates through all items in the player's hotbar.
        for(int i = 0; i < 8; i++){
            // The itemstack in i slot.
            ItemStack itemStack = minecraft.player.inventory.mainInventory[i];

            // Continues if the slot is empty.
            if(itemStack == null || itemStack.getItem() == null) continue;

            // The strength of the item.
            float itemStrength = getStrengthAgainstBlock(block, itemStack);

            // Continues if the item strength is lower than the current highest strength.
            if(itemStrength <= strength || itemStrength == 1) continue;

            // Sets the strongest item index.
            strongest = i;

            // Sets the highest strength.
            strength = itemStrength;
        }

        return strongest;
    }

    /**
     * Gets the given item's strength against the given block.
     *
     * @param block The block to get the strength against.
     * @param item  The item to get the strength of.
     * @return The item strength with enchantments.
     */
    public static float getStrengthAgainstBlock(Block block, ItemStack item){
        // The item strength.
        float strength = item.getStrVsBlock(block);

        // Returns the strength if the item doesn't have a strength modifier or if the strength is 1.
        if(!EnchantmentHelper.getEnchantments(item).containsKey(Enchantment.efficiency.effectId) || strength == 1)
            return strength;

        // The enchantment level.
        int enchantLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.efficiency.effectId, item);

        // Returns the item strength.
        return strength + (enchantLevel * enchantLevel + 1);
    }

}
