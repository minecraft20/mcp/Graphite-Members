package me.valkyrie.graphite.impl.module.self.retard.modes;

import me.valkyrie.graphite.api.module.ModeManifest;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Increment;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.module.self.retard.Retard;

/**
 * @author Zeb.
 * @since 6/2/2017.
 */
@ModeManifest(label = "Spin", parentClass = Retard.class)
public class Spin extends AbstractRetardMode {

    /**
     * How fast the player spins.
     */
    @Property(label = "Speed", aliases = {"Rate", "Change"})
    @Clamp(min = "5", max = "20")
    @Increment("5")
    private float speed = 10;

    /**
     * The last yaw rotation of the player.
     */
    private float lastYaw = 0;

    /**
     * Sets the last yaw rotation.
     */
    @Override
    public void enable(){
        if(player == null)
            return;

        lastYaw = player.rotationYaw;
    }

    /**
     * @return the for the player to spoof.
     */
    @Override
    public float[] rotations(){

        // Sets the rotations.
        float[] rotations = new float[]{lastYaw + speed, player.rotationPitch};

        lastYaw += speed;

        return rotations;
    }
}
