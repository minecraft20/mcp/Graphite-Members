package me.valkyrie.graphite.impl.module.self.retard;

import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.Graphite;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import me.valkyrie.graphite.impl.module.self.retard.modes.*;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;

/**
 * @author Zeb.
 * @since 6/2/2017.
 */
@ModuleManifest(label = "Retard", color = 0xFFe0b296, type = Category.SELF, modes =
        {Spin.class, Random.class, Backwards.class})
public class Retard extends Module implements EventHandler<PacketEvent> {

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return PacketEvent.class;
    }

    /**
     * Handles the event.
     *
     * @param event The event called.
     */
    @Override
    public void handle(PacketEvent event){

        // Sends a correct packet if needed only if killaura isn't on.
        if(event.getPacket() instanceof C02PacketUseEntity || event.getPacket() instanceof C08PacketPlayerBlockPlacement
                )
            minecraft.getNetHandler().getNetworkManager().sendPacket(
                    new C03PacketPlayer.C05PacketPlayerLook(player.rotationYaw, player.rotationPitch, player.onGround));

    }

    /**
     * Handles rotating.
     */
    private final EventHandler<PlayerMotionUpdateEvent> playerMotionUpdateEventHandler = EventFactory.form(
            PlayerMotionUpdateEvent.class,
            (PlayerMotionUpdateEvent event) -> {

                // Returns if event type is wrong.
                if(event.getEventType() != Type.PRE)
                    return;

                // Gets the current mode.
                AbstractRetardMode retardMode = (AbstractRetardMode) getMode();

                // Gets the rotations.
                float[] rotations = retardMode.rotations();

                event.setYaw(rotations[0]);
                event.setPitch(rotations[1]);
            });

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(this);
        EventManager.INSTANCE.register(playerMotionUpdateEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(this);
        EventManager.INSTANCE.unregister(playerMotionUpdateEventHandler);
    }

}
