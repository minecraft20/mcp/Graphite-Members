package me.valkyrie.graphite.impl.module.self;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.player.PlayerUpdateEvent;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemPotion;
import net.minecraft.network.play.client.C03PacketPlayer;

/**
 * Allows the player to eat items faster.
 * <p>
 * Created by Zeb on 4/22/2017.
 */
@ModuleManifest(label = "Quick_Munch", color = 0xFFf2991d, type = Category.SELF)
public class QuickMunch extends Module {

    /**
     * How long the player has to eat before finishing the food.
     */
    @Property(label = "Delay", aliases = {"Time", "Ticks"}, description = "How long the player has to eat before finishing the food.")
    @Clamp(min = "8", max = "32")
    private int delay = 12;

    /**
     * Handles packet spam to eat faster.
     */
    private EventHandler<PlayerUpdateEvent> updateEventHandler = new EventHandler<PlayerUpdateEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerUpdateEvent.class;
        }

        /**
         * Forces the player to sprint if they can.
         */
        @Override
        public void handle(PlayerUpdateEvent event){
            // Returns if the event type is a pre update event.
            if(event.getEventType() == Type.POST) return;

            // Returns if not using a valid item or the delay hasn't been reached.
            if(!isUsingItem() || player.getItemInUseDuration() < delay) return;

            // The number of packets needed to be sent.
            int packets = 32 - player.getItemInUseDuration();

            // Spams packets.
            for(int i = 0; i < packets; i++)
                player.sendQueue.addToSendQueue(new C03PacketPlayer(player.onGround));

            // Stop using the item if the item.
            minecraft.playerController.onStoppedUsingItem(minecraft.player);
        }

    };

    /**
     * @return If the item being used is a potion or a usable item.
     */
    public boolean isUsingItem(){
        return player.getHeldItem() != null
                && (player.getHeldItem().getItem() instanceof ItemFood || player.getHeldItem().getItem() instanceof ItemPotion);
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(updateEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(updateEventHandler);
    }

}
