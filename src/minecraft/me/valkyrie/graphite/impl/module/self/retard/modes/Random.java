package me.valkyrie.graphite.impl.module.self.retard.modes;

import me.valkyrie.graphite.api.module.ModeManifest;
import me.valkyrie.graphite.impl.module.self.retard.Retard;

/**
 * @author Zeb.
 * @since 6/2/2017.
 */
@ModeManifest(label = "Random", parentClass = Retard.class)
public class Random extends AbstractRetardMode {

    /**
     * @return the for the player to spoof.
     */
    @Override
    public float[] rotations(){
        java.util.Random rand = new java.util.Random();

        return new float[]{rand.nextInt(360), rand.nextInt(180) - 90};
    }

}
