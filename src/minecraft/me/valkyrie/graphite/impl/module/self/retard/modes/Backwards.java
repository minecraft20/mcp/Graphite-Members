package me.valkyrie.graphite.impl.module.self.retard.modes;

import me.valkyrie.graphite.api.module.ModeManifest;
import me.valkyrie.graphite.impl.module.self.retard.Retard;

/**
 * @author Zeb.
 * @since 6/2/2017.
 */
@ModeManifest(label = "Backwards", parentClass = Retard.class)
public class Backwards extends AbstractRetardMode {

    /**
     * @return the for the player to spoof.
     */
    @Override
    public float[] rotations(){
        return new float[]{player.rotationYaw + 180 % 360, player.rotationPitch};
    }
}
