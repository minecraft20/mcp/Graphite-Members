package me.valkyrie.graphite.impl.module.self;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import net.minecraft.network.play.client.C0BPacketEntityAction;

/**
 * Cuts down on player exhaustion by spoofing
 * the outgoing on-ground states because players
 * do not lose hunger while in air.
 * <p>
 * (Thanks ddong for his docs that told me how hunger works.)
 *
 * @author Zeb.
 * @since 5/3/2017
 */
@ModuleManifest(label = "Anti_Hunger", color = 0xFF9ad1d1, type = Category.SELF)
public class Antihunger extends Module {

    /**
     * If the player was on the ground last tick. Used to
     * handle falling and not constantly flag for no-fall.
     */
    private boolean previousGroundState = false;

    /**
     * Spoofs all outgoing {@link net.minecraft.network.play.client.C03PacketPlayer}'s to set their on-ground
     * state to false to prevent hunger drain.
     */
    private final EventHandler<PlayerMotionUpdateEvent> motionUpdateEventHandler = new EventHandler<PlayerMotionUpdateEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerMotionUpdateEvent.class;
        }

        /**
         * Sets all outgoing {@link net.minecraft.network.play.client.C03PacketPlayer}'s on-ground states to false.
         */
        @Override
        public void handle(PlayerMotionUpdateEvent event){

            if(event.getEventType() == Type.PRE){

                // Sends a stop sprint packet.
                if(player.isSprinting())
                    player.sendQueue.addToSendQueue(new C0BPacketEntityAction(minecraft.player, C0BPacketEntityAction.Action.STOP_SPRINTING));

                // Returns if the player is mining a block.
                if(minecraft.playerController.curBlockDamageMP > 0)
                    return;

                // Sets the on-ground state to false to the game thinks we are in air, blocking hunger drain.
                event.setOnGround(false);

                // Sets the on-ground state to true to not flag no-fall.
                if(player.onGround && !previousGroundState)
                    event.setOnGround(true);
            } else{

                // Sends a start sprint packet.
                if(player.isSprinting())
                    player.sendQueue.addToSendQueue(new C0BPacketEntityAction(minecraft.player, C0BPacketEntityAction.Action.START_SPRINTING));

                // Sets the previous on-ground state.
                previousGroundState = player.onGround;
            }
        }

    };

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(motionUpdateEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(motionUpdateEventHandler);
    }

}
