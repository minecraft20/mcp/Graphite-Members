package me.valkyrie.graphite.impl.module.self;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import net.minecraft.network.play.server.S06PacketUpdateHealth;

/**
 * Automatically respawns the player when they die.
 *
 * @author Zeb.
 * @since 4/26/2017
 */
@ModuleManifest(label = "Auto_Respawn", color = 0xFFd67ae2, type = Category.SELF, visible = false)
public class AutoRespawn extends Module implements EventHandler<PacketEvent> {

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return PacketEvent.class;
    }

    /**
     * Respawns the player when they die.
     */
    @Override
    public void handle(PacketEvent event){
        // Returns if the packet isn't a health update or the event type is a pre-event.
        if(!(event.getPacket() instanceof S06PacketUpdateHealth))
            return;

        // The packet being received.
        S06PacketUpdateHealth packet = (S06PacketUpdateHealth) event.getPacket();

        // Re-spawns the player if their health is at or below 0.
        if(packet.getHealth() <= 0)
            player.respawnPlayer();
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(this);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(this);
    }

}
