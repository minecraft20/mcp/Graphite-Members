package me.valkyrie.graphite.impl.module.fight.killaura;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.sun.javafx.geom.Vec2f;
import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.helper.RotationHelper;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import me.valkyrie.graphite.api.util.game.GameTimer;
import me.valkyrie.graphite.api.util.property.Child;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Increment;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import me.valkyrie.graphite.impl.module.fight.killaura.mode.Single;
import me.valkyrie.graphite.impl.module.fight.killaura.mode.Switch;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemSword;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C0BPacketEntityAction;

/**
 * Automatically attacks nearby entities to the player.
 *
 * @author Zeb.
 * @since 4/22/2017
 */
@ModuleManifest(label = "Killaura", color = 0xFFce382d, type = Category.FIGHT, modes =
        {Single.class, Switch.class})
public class Killaura extends Module {

    /**
     * How fast the killaura attacks.
     */
    @Property(label = "Attack_rate", aliases = {"Attacks_Per_Second", "APS", "CPS",
            "Speed"})
    @Clamp(min = "1", max = "20")
    public int attackRate = 10;

    /**
     * How far you can hit an entity from.
     */
    @Property(label = "Reach", aliases = {"Range", "Distance"})
    @Clamp(min = "3", max = "8")
    @Increment("0.25")
    public double reach = 4;

    @Property(label = "Comparator", aliases = {"Sort", "Sorter"})
    public EntityComparator entityComparator = EntityComparator.ANGLE;

    /**
     * If criticals should be used.
     */
    @Property(label = "Criticals", aliases = {"Crits"})
    public boolean criticals = true;

    /**
     * The critical mode that should be used.
     */
    @Child("Criticals")
    @Property(label = "Critical_Mode", aliases = "Crit_Mode")
    public CriticalType criticalMode = CriticalType.PACKET;

    /**
     * If the killaura should automatically block.
     */
    @Property(label = "Auto_block", aliases = "Block")
    public boolean autoblock = true;

    /**
     * How far an entity has to be to start blocking.
     */
    @Child("Auto_block")
    @Property(label = "Range", aliases = {"Reach", "Distance"})
    @Clamp(min = "4", max = "10")
    @Increment("0.5")
    public double blockRange = 7.5;

    /**
     * If the killaura should use Anti_Bot.
     */
    @Property(label = "Anti_Bot", aliases = {"NoBot"})
    public boolean antibot = true;

    /**
     * Gives more knockback to the entity being hit.
     */
    @Property(label = "Push", aliases = {"Wtap", "W-tap"})
    public boolean push = true;

    /**
     * Decides what entity to hit depending on some trait.
     */
    @Property(label = "Players", aliases = "Humans")
    public boolean players = true;

    /**
     * If killaura should protect attack friendProtect.
     */
    @Child("Players")
    @Property(label = "Friend Protect", aliases = {"Friendly", "Friendlies"})
    private boolean friendProtect = true;

    /**
     * If killaura should attack mobs.
     */
    @Property(label = "Mobs", aliases = "Mob")
    private boolean mobs = true;

    /**
     * The entity that will be attacked.
     */
    private Optional<EntityLivingBase> target;

    /**
     * The standard entity predicate to check for entity validity.
     */
    private final Predicate<EntityLivingBase> sharedEntityPredicate = entity -> entity.getHealth() > 0 &&!(entity instanceof EntityPlayerSP);

    /**
     * Predicate used to filter entities.
     */
    public final Predicate<EntityLivingBase> entityPredicate = entity -> entity.getDistanceToEntity(player) < reach;

    /**
     * Predicate used to check for auto-blocking validity.
     */
    private final Predicate<EntityLivingBase> autoblockEntityPredicate = entity -> autoblock
            && entity.getDistanceToEntity(player) < blockRange;

    /**
     * The timer used to handle killaura delay.
     */
    private final GameTimer gameTimer = new GameTimer();

    /**
     * The server sided rotations to be used in angle sorting.
     *
     * TODO: Replace with some vector class or something similar.
     */
    private static float[] serverRotations = {0,0};

    public Killaura(){
        EventManager.INSTANCE.register(EventFactory.form(PacketEvent.class, (PacketEvent event) -> {

            if(isRunning())
                return;

            // Returns if packet isn't of the correct type.
            if(!(event.getPacket() instanceof C03PacketPlayer.C05PacketPlayerLook || event.getPacket() instanceof C03PacketPlayer.C06PacketPlayerPosLook))
                return;

            C03PacketPlayer packet = (C03PacketPlayer) event.getPacket();

            // Sets new server rotations.
            serverRotations[0] = packet.getYaw();
            serverRotations[1] = packet.getPitch();
        }));
    }

    /**
     * Attacks the target entity.
     */
    public void attack(){
        if(!target.isPresent())
            return;

        serverRotations = RotationHelper.INSTANCE.getRotations(target.get());

        // If the player is blocking.
        boolean blocking = minecraft.gameSettings.keyBindUseItem.isPressed();

        // If the player is sprinting and moving.
        boolean sprinting = player.isSprinting();

        // Stops then starts sneaking to do more knock-back.
        if(sprinting && push)
            sprint();

        // Does a critical hit.
        if(criticals)
            criticalMode.critical();

        // Sends an attack packet.
        player.sendQueue.addToSendQueue(new C02PacketUseEntity(target.get(), C02PacketUseEntity.Action.ATTACK));

        // Swings the player's arm.
        player.swingItem();

        // Stops then starts sneaking to do more knock-back.
        if(sprinting && push)
            sprint();

        // Resets the timer.
        gameTimer.reset();
    }

    /**
     * Stops and then restarts sprinting to deal more knock-back.
     */
    private void sprint(){
        player.sendQueue.addToSendQueue(
                new C0BPacketEntityAction(minecraft.player, C0BPacketEntityAction.Action.STOP_SPRINTING));
        player.sendQueue.addToSendQueue(
                new C0BPacketEntityAction(minecraft.player, C0BPacketEntityAction.Action.START_SPRINTING));
    }

    /**
     * @return the target to attack
     */
    public Optional<EntityLivingBase> getTarget(){
        return target;
    }

    /**
     * Sets the target to attack.
     *
     * @param target The entity to attack.
     */
    public void setTarget(Optional<EntityLivingBase> target){
        this.target = target;
    }

    /**
     * @return if the killaura can attack the target.
     */
    public boolean canAttack(){
        return target.isPresent() && gameTimer.reached(1000 / attackRate);
    }

    /**
     * Assigns the next target to attack.
     */
    public void assignNextLivingEntity(){
        this.target = findNextLivingEntity();
    }

    /**
     * @return the best {@link EntityLivingBase} to attack
     */
    private Optional<EntityLivingBase> findNextLivingEntity(){
        return sortedLivingEntityStream().filter(entityPredicate).findFirst();
    }

    /**
     * @return if the player should block when attacking their target.
     */
    public boolean shouldBlock(){
        return sortedLivingEntityStream().anyMatch(autoblockEntityPredicate)
                && player.getHeldItem() != null
                && player.getHeldItem().getItem() instanceof ItemSword;
    }

    /**
     * @return a stream of {@link EntityLivingBase}.
     */
    public Stream<EntityLivingBase> sortedLivingEntityStream(){
        return minecraft.world.loadedEntityList.stream()
                .filter(EntityLivingBase.class::isInstance)
                .map(EntityLivingBase.class::cast)
                .filter(sharedEntityPredicate)
                .sorted(entityComparator.comparator());
    }

    /**
     * All the critical types.
     */
    enum CriticalType {

        PACKET,
        HOP,
        JUMP;

        /**
         * Offsets to be used when spamming packets.
         */
        final double[] OFFSETS = {0.04, 0, 0.03, 0};

        /**
         * Forces a critical attack.
         */
        public void critical(){
            EntityPlayerSP player = Minecraft.getMinecraft().player;

            if(!player.onGround)
                return;

            switch(this){
                case HOP:
                    player.motionY = 0.3;
                    player.onGround = false;
                    break;
                case JUMP:
                    player.jump();
                    break;
                case PACKET:
                    Arrays.stream(OFFSETS).forEach(offset ->
                            player.sendQueue.getNetworkManager().sendPacket(
                                    new C03PacketPlayer.C04PacketPlayerPosition(player.posX, player.posY + offset, player.posZ, false)));
            }
        }

    }

    /**
     * Comparator for sorting entites.
     */
    public enum EntityComparator {

        DISTANCE,
        ANGLE,
        STRONGEST,
        WEAKEST;

        /**
         * @return a {@link Comparator} for sorting entities.
         */
        public Comparator<EntityLivingBase> comparator(){
            switch(this){
                case DISTANCE:
                    return Comparator.comparingDouble(entity -> entity.getDistanceToEntity(Minecraft.getMinecraft().player));
                case ANGLE:
                    return Comparator.comparingDouble(entity -> RotationHelper.INSTANCE.getDistance(serverRotations, entity));
                case WEAKEST:
                    return Comparator.comparingDouble(net.minecraft.entity.EntityLivingBase::getHealth);
                case STRONGEST:
                    return Comparator.comparingDouble(net.minecraft.entity.EntityLivingBase::getHealth);
                default:
                    return Comparator.comparingDouble(entity -> entity.getDistanceToEntity(Minecraft.getMinecraft().player));
            }
        }

    }

}