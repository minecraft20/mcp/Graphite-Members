package me.valkyrie.graphite.impl.module.fight.killaura.mode;

import java.lang.annotation.Target;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.Predicate;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.helper.RotationHelper;
import me.valkyrie.graphite.api.module.Mode;
import me.valkyrie.graphite.api.module.ModeManifest;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import me.valkyrie.graphite.impl.event.player.PlayerUpdateEvent;
import me.valkyrie.graphite.impl.module.fight.killaura.Killaura;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C03PacketPlayer.C05PacketPlayerLook;
import net.minecraft.network.play.client.C03PacketPlayer.C06PacketPlayerPosLook;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;

/**
 * @author deputy
 * @since 4/22/2017
 */
@ModeManifest(label = "Single", parentClass = Killaura.class)
public class Single extends Mode<Killaura> implements EventHandler<PlayerMotionUpdateEvent> {

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return PlayerMotionUpdateEvent.class;
    }

    /**
     * Handles the event.
     *
     * @param event The event called.
     */
    @Override
    public void handle(PlayerMotionUpdateEvent event){
        if(event.getEventType() == Type.PRE){

            // Finds the next target.
            parent.assignNextLivingEntity();

            // Looks at the entity if the target is present.
            parent.getTarget().ifPresent(target -> {

                // Gets the rotations to the target.
                float[] rotations = RotationHelper.INSTANCE.getRotations(target);

                event.setYaw(rotations[0]);
                event.setPitch(rotations[1]);
            });
        }else{

            // Attacks the target entity if delay has been reached.
            if(parent.canAttack())
                parent.attack();

            // Does auto-blocking.
            if(parent.shouldBlock() && !player.isBlocking())
                minecraft.playerController.sendUseItem(minecraft.player, minecraft.world, player.getHeldItem());
        }
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(this);
        parent.setTarget(Optional.empty());
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(this);
    }

}