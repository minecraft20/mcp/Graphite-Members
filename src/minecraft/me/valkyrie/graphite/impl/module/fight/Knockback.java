package me.valkyrie.graphite.impl.module.fight;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import net.minecraft.network.play.server.S12PacketEntityVelocity;
import net.minecraft.network.play.server.S27PacketExplosion;

/**
 * This module blocks all incoming knock-back.
 * <p>
 * Created by Zeb on 4/21/2017.
 */
@ModuleManifest(label = "Knockback", color = 0xFFb44b47, type = Category.FIGHT)
public class Knockback extends Module {

    /**
     * The listener that blocks all incoming knock-back.
     */
    private EventHandler<PacketEvent> packetEventHandler = new EventHandler<PacketEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PacketEvent.class;
        }

        /**
         * Blocks all incoming velocity packets.
         */
        @Override
        public void handle(PacketEvent event){
            // Cancels the packet event if the packet causes velocity.
            if(event.getPacket() instanceof S12PacketEntityVelocity){
                S12PacketEntityVelocity packet = (S12PacketEntityVelocity) event.getPacket();

                if(packet.func_149412_c() != player.getEntityId()) return;

                event.setCancelled(true);
            }
        }

    };

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(packetEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(packetEventHandler);
    }

}
