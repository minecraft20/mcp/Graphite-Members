package me.valkyrie.graphite.impl.module.fight;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import me.valkyrie.graphite.api.util.game.GameTimer;
import me.valkyrie.graphite.api.util.property.Child;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Increment;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.game.Direction;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemSoup;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.network.play.client.C0EPacketClickWindow;
import net.minecraft.network.play.server.S06PacketUpdateHealth;
import net.minecraft.network.play.server.S2FPacketSetSlot;
import net.minecraft.potion.PotionHelper;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;

/**
 * Automatically heals the minecraft.player with potions and soups.
 *
 * @author Zeb.
 * @since 4/28/2017
 */
@ModuleManifest(label = "Autoheal", color = 0xFFf298ef, type = Category.FIGHT)
public class AutoHeal extends Module {

    /**
     * The health the user should be healed at.
     */
    @Property(label = "Health", aliases = "Hearts", description = "How many hearts you should have before healing.")
    @Clamp(min = "0", max = "10")
    @Increment("0.5")
    private double health = 3;

    /**
     * How long autoheal should wait before healing again.
     */
    @Property(label = "Delay", aliases = "Time", description = "How long autoheal should wait before healing again.")
    @Clamp(min = "0", max = "2000")
    @Increment("50")
    private long delay = 1000;

    /**
     * If potions should be thrown.
     */
    @Property(label = "Potions", aliases = {"Potion", "Pot", "Pots"}, description = "If health potion should be thrown.")
    private boolean potions = true;

    /**
     * If potions should be thrown upwards.
     */
    @Child("Potions")
    @Property(label = "Upwards", aliases = {"Up", "Vertical"}, description = "If potions should be throw upwards")
    private boolean upwards = false;

    /**
     * If soups should be eaten for health.
     */
    @Property(label = "Soup", aliases = "Soups", description = "If mushroom soup should be eaten for health.")
    private boolean soup = true;

    /**
     * If used soups should be dropped.
     */
    @Child("Soup")
    @Property(label = "Drop", aliases = "ThrowAway", description = "If used soups should be thrown away.")
    private boolean drop = false;

    /**
     * The slot where the item to be used is.
     */
    private int slot = -1;

    /**
     * If the minecraft.player is healing.
     */
    private boolean healing = false;

    /**
     * Timer used to measure delay.
     */
    private GameTimer gameTimer = new GameTimer();

    /**
     * The packet slot being used to heal.
     */
    private int packetSlot = -1;

    /**
     * If the item used was a soup.
     */
    private boolean packetSoup = false;

    private ItemStack itemStack = null;

    /**
     * Handles potion throwing.
     */
    private final EventHandler<PlayerMotionUpdateEvent> playerMotionUpdateEventHandler = new EventHandler<PlayerMotionUpdateEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerMotionUpdateEvent.class;
        }

        /**
         * Handles the event.
         */
        @Override
        public void handle(PlayerMotionUpdateEvent event){
            if(event.getEventType() == Type.PRE){
                // Updates slot ifo and count.
                update();

                // Returns if the minecraft.player is fine.
                if(!shouldHeal()) return;

                // Return if slot is invalid.
                if(slot == -1) return;

                // Breaks the event so nothing else is fired.
                event.breakEvent();

                // If the player should heal upwards.
                boolean up = upwards && player.onGround;

                // Does potion stuff if the slot has a potion in it.
                if(player.inventory.mainInventory[slot].getItem() instanceof ItemPotion){
                    // Jumps if the potion should be thrown upwards.
                    if(up) player.jump();

                    // Sets the pitch to up or down depending if the potion should go up or down.
                    event.setPitch(up ? -90 : 90);
                }

                // Sets the state to healing.
                healing = true;
            } else{
                // Returns if not healing.
                if(!healing) return;

                // Returns if the player is fine.
                if(!shouldHeal()) return;

                // Returns if slot is invalid.
                if(slot == -1) return;

                if(player.inventory.mainInventory[8] != null)
                    itemStack = player.inventory.mainInventory[8].copy();

                // If the item was a soup.
                packetSoup = player.inventory.mainInventory[slot].getItem() == Items.mushroom_stew;

                // Sets the packet slot to the current slot.
                packetSlot = slot;

                // If the slot is in the hotbar.
                if(slot < 9){

                    // Changes hotbar slot to the item.
                    player.sendQueue.addToSendQueue(new C09PacketHeldItemChange(slot));

                    // Uses the item.
                    player.sendQueue.getNetworkManager().sendPacket(new C08PacketPlayerBlockPlacement(player.getHeldItem()));

                    // Drops the item.
                    if(drop && packetSoup)
                       player.sendQueue.addToSendQueue(new C07PacketPlayerDigging(
                               C07PacketPlayerDigging.Action.DROP_ALL_ITEMS,
                               BlockPos.ORIGIN, EnumFacing.DOWN));

                    // Swaps back to the item being held.
                    player.sendQueue.addToSendQueue(new C09PacketHeldItemChange(player.inventory.currentItem));
                } else{

                    // Changes hotbar slot to the item.
                    player.sendQueue.addToSendQueue(new C09PacketHeldItemChange(8));

                    // Moves the item.
                    minecraft.playerController.windowClick(player.inventoryContainer.windowId, slot, 8, 2, minecraft.player);

                    // Uses the item.
                    minecraft.getNetHandler().getNetworkManager().sendPacket(new C08PacketPlayerBlockPlacement(player.getHeldItem()));

                    // Drops the item.
                    if(drop && packetSoup)
                        player.sendQueue.addToSendQueue(new C07PacketPlayerDigging(
                                C07PacketPlayerDigging.Action.DROP_ALL_ITEMS,
                                BlockPos.ORIGIN, EnumFacing.DOWN));

                    // Moves the item.
                    minecraft.playerController.windowClick(player.inventoryContainer.windowId, packetSlot, 8, 2, minecraft.player);

                    // Swaps back to the item being held.
                    player.sendQueue.addToSendQueue(new C09PacketHeldItemChange(player.inventory.currentItem));
                }

                // Resets the timer.
                gameTimer.reset();

                // Breaks the event.
                event.breakEvent();
            }
        }

    };

    /**
     * Handles switching of items.
     */
    private final EventHandler<PacketEvent> packetEventHandler = new EventHandler<PacketEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PacketEvent.class;
        }

        /**
         * Handles switching of items.
         */
        @Override
        public void handle(PacketEvent event){

            if(!(!(event.getPacket() instanceof S2FPacketSetSlot)
                    || event.getEventType() == Type.POST
                    || packetSlot == -1
                    || gameTimer.reached(1000))){

                S2FPacketSetSlot packet = (S2FPacketSetSlot) event.getPacket();

                if(packet.func_149173_d() == 44){
                    packet.setItem(itemStack);
                }

                packetSlot = -1;
            }
        }

    };

    /**
     * Updates the count and finds an item to use.
     */
    public void update(){
        // The number of healing items in the inventory.
        int count = 0;

        // Sets the slot to -1 so nothing is used.
        slot = -1;

        // Iterates throw the inventory.
        for(int i = 0; i <= 35; i++){

            // Continues threw list if item is air.
            if(player.inventory.mainInventory[i] == null || player.inventory.mainInventory[i].getItem() == null)
                continue;

            // The item stack in the slot.
            ItemStack stack = player.inventory.mainInventory[i];

            // The type of the stack.
            Item item = stack.getItem();

            // If the item is a potion or souplayer.
            if(item instanceof ItemPotion && potions){
                // Casts the item to a potion.
                ItemPotion potion = (ItemPotion) item;

                // Continues with loop if potion data isn't valid.
                if(stack.getMetadata() != 16421) continue;

                // Sets the current slot.
                slot = i;

                // Increases the count.
                count++;
            } else if(item instanceof ItemSoup && soup){
                // Sets the current slot.
                slot = i;

                // Increases the count.
                count++;
            }
        }

        // Sets the suffix.
        suffix = count + "";
    }

    /**
     * @return If the player should be healed.
     */
    private boolean shouldHeal(){
        return player.getHealth() <= health*2 && gameTimer.reached(delay);
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(playerMotionUpdateEventHandler);
        EventManager.INSTANCE.register(packetEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(playerMotionUpdateEventHandler);
        EventManager.INSTANCE.unregister(packetEventHandler);
    }

}
