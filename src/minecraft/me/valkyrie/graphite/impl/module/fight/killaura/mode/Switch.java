package me.valkyrie.graphite.impl.module.fight.killaura.mode;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.helper.RotationHelper;
import me.valkyrie.graphite.api.module.Mode;
import me.valkyrie.graphite.api.module.ModeManifest;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import me.valkyrie.graphite.impl.module.fight.killaura.Killaura;
import net.minecraft.entity.EntityLivingBase;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author deputy
 * @since 4/22/2017
 */
@ModeManifest(label = "Switch", parentClass = Killaura.class)
public class Switch extends Mode<Killaura> implements EventHandler<PlayerMotionUpdateEvent> {

    /**
     * A set of all of the targets to attack.
     */
    private List<EntityLivingBase> targets = new ArrayList<>();

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return PlayerMotionUpdateEvent.class;
    }

    /**
     * Handles the event.
     *
     * @param event The event called.
     */
    @Override
    public void handle(PlayerMotionUpdateEvent event){
        if(event.getEventType() == Type.PRE){

            // Fills the target list if empty or removes invalid entities if not.
            if(targets.isEmpty())
                targets = parent.sortedLivingEntityStream().filter(parent.entityPredicate).collect(Collectors.toList());
            else
                targets = targets.stream().filter(parent.entityPredicate).sorted(parent.entityComparator.comparator()).collect(Collectors.toList());

            // Sets the target to attack.
            parent.setTarget(targets.isEmpty() ? Optional.empty() : Optional.of(targets.get(0)));

            // Looks at the entity if the target is present.
            parent.getTarget().ifPresent(target -> {

                // Gets the rotations to the target.
                float[] rotations = RotationHelper.INSTANCE.getRotations(target);

                event.setYaw(rotations[0]);
                event.setPitch(rotations[1]);
            });
        }else{

            // Attacks the target entity if delay has been reached.
            if(parent.canAttack()){
                // Removes the target.
                targets.remove(parent.getTarget().get());

                parent.attack();

                parent.setTarget(Optional.empty());
            }

            // Does auto-blocking.
            if(parent.shouldBlock() && !player.isBlocking())
                minecraft.playerController.sendUseItem(minecraft.player, minecraft.world, player.getHeldItem());
        }
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(this);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(this);
    }

}