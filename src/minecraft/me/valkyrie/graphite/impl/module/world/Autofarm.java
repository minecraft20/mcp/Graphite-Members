package me.valkyrie.graphite.impl.module.world;

import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.helper.RotationHelper;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.MathUtil;
import me.valkyrie.graphite.api.util.game.GameTimer;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Increment;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import me.valkyrie.graphite.impl.event.player.PlayerMoveEvent;
import me.valkyrie.graphite.impl.module.move.Parkour;
import net.minecraft.block.*;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.util.*;

import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Zeb.
 * @since 12/18/2016.
 */
@ModuleManifest(label = "Auto_farm", color = 0xFF38D83A, type = Category.WORLD)
public class Autofarm extends Module {

    /**
     * How far you can harvest a crop from.
     */
    @Property(label = "Radius", description = "How big the crop search radius is.")
    @Clamp(min = "0", max = "100")
    @Increment("5")
    private int radius = 50;

    /**
     * The delay between breaking or planting.
     */
    @Property(label = "Delay", description = "The delay between breaking or planting.")
    @Clamp(min = "0", max = "5")
    private int delay = 1;

    /**
     * The crop being harvested.
     */
    @Property(label = "Crop", description = "The crop being harvested.")
    private Crop crop = Crop.GENERIC;

    private GameTimer timer = new GameTimer();

    /**
     * If the autofarm should deposit its items in a nearby chest.
     */
    private boolean deposit = false;

    /**
     * Where the player should walk to.
     */
    private Vec3 destination = null;

    /**
     * All the fully grown plants nearby.
     */
    private List<Vec3> grown = new ArrayList<>();

    /**
     * All the empty land nearby.
     */
    private List<Vec3> empty = new ArrayList<>();

    /**
     * The state of the auto farm.
     */
    private State state = State.RESTING;

    /**
     * The ticks used to handle delay.
     */
    private int ticks = 0;

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        // Disables the module if the player is null.
        if(player == null)
            setRunning(false);

        destination = null;

        // Clears the grown crops.
        grown.clear();
        empty.clear();

        EventManager.INSTANCE.register(playerMotionUpdateEventHandler);
        EventManager.INSTANCE.register(playerMotionEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(playerMotionUpdateEventHandler);
        EventManager.INSTANCE.unregister(playerMotionEventHandler);
    }

    private EventHandler<PlayerMotionUpdateEvent> playerMotionUpdateEventHandler =
            EventFactory.form(PlayerMotionUpdateEvent.class, (PlayerMotionUpdateEvent event) -> {

                if(timer.reached(20 * 60 * 10)){
                    player.sendChatMessage("/blocks");
                    timer.reset();
                }

                // Returns for hit delay.
                if(ticks > 0){
                    // Decrease ticks if its a post event.
                    if(event.getEventType() == Type.POST) ticks--;

                    // Returns.
                    return;
                }

                if(event.getEventType() == Type.PRE){
                    // Scans for crops.
                    if(player.ticksExisted % 20 == 0) scan();

                    // Finds the destination.
                    destination = find();

                    // Finds entities that can be picked up.
                    Optional<Entity> entityItem = minecraft.world.loadedEntityList.stream()
                            .filter(entity -> entity instanceof EntityItem
                                    && ((EntityItem) entity).getEntityItem().getItem().getUnlocalizedName().toLowerCase().contains("wheat")
                                    && entity.getDistanceToEntity(player) < radius
                                    && Math.abs(entity.posY - player.posY) <= 0.6)
                            .sorted((first, second) -> Math.round(first.getDistanceToEntity(player) - second.getDistanceToEntity(player)))
                            .findFirst();

                    if(entityItem.isPresent()){
                        // Sets the state to collecting.
                        state = State.COLLECTING;

                        // Returns the entity's position.
                        destination = entityItem.get().getPositionVector();
                    }

                    // Return if the state is resting.
                    if(state == State.RESTING) return;

                    // Returns if too far away.
                    if(destination.distanceTo(player.getPositionVector()) > 4) return;

                    // Calculates the rotations to the destination.
                    float[] rotations =
                            RotationHelper.INSTANCE.
                                    getRotations(destination.addVector(0, state == State.HARVESTING ? 0.7 : (crop == Crop.GENERIC ? -0.1 : 0), 0));

                    // Sets rotations to look at the destination.
                    event.setYaw(rotations[0]);
                    event.setPitch(rotations[1]);
                }else{
                    // Return if the state is resting.
                    if(state == State.RESTING) return;

                    // Finds the distance to the destination.
                    double distance = player.getPositionVector().distanceTo(destination);

                    // Forms a block pos from the destination.
                    BlockPos position = new BlockPos(destination);

                    // Handles interaction depending on state.
                    switch(state){
                        case HARVESTING:
                            // Return if the destination is out of range.
                            if(distance > 4) return;

                            // Swings the player's arm.
                            player.swingItem();

                            // Starts and stops destroying the crop, breaking it.
                            player.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, position, EnumFacing.UP));
                            player.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.STOP_DESTROY_BLOCK, position, EnumFacing.UP));

                            // Adds the destination to the empty list if sugarcane isn't the crop.
                            if(crop != Crop.SUGARCANE) empty.add(destination);

                            // Removes the destination from the grown list.
                            grown.remove(destination);

                            // Nulls the destination.
                            destination = null;

                            // Sets the delay.
                            ticks = delay;
                            break;
                        case PLANTING:
                            // Return if the destination is out of range.
                            if(distance > 4) return;

                            // Swings the player's arm.
                            player.swingItem();

                            // Places the seed on the block.
                            player.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(position.offsetDown(), EnumFacing.UP.ordinal(), player.getHeldItem(),
                                    1, 1, 1));

                            // Removes the destination from the grown list.
                            empty.remove(destination);

                            // Nulls the destination.
                            destination = null;

                            // Sets the delay.
                            ticks = 0;
                            break;
                    }
                }
            });

    /**
     * Handler to handle movement.
     */
    private EventHandler<PlayerMoveEvent> playerMotionEventHandler = EventFactory.form(PlayerMoveEvent.class, (PlayerMoveEvent event) -> {
        // Sets the movement to 0 if there is no destination.
        if(destination == null){
            event.x = 0;
            event.z = 0;
            return;
        }

        player.setSprinting(player.getFoodStats().getFoodLevel() > 6);

        // The movement speed of the player.
        double speed = player.isSneaking() ? 0.3 : 1 * (player.isSprinting() ? 0.2 : 0.2);

        player.setSprinting(false);

        // Finds the distance to the destination.
        double distance = player.getPositionVector().distanceTo(destination);

        // Will set the speed to the destination difference so it doesn't over shoot.
        speed = Math.min(speed, distance);

        // Calculates the rotations to the destination.
        float[] rotations = RotationHelper.INSTANCE.getRotations(destination.addVector(0, state == State.HARVESTING ? 1 : 0, 0));

        // Sets the horizontal movement.
        event.x = -(Math.sin(Math.toRadians(rotations[0])) * speed);
        event.z = (Math.cos(Math.toRadians(rotations[0])) * speed);

        // Nulls the destination if the state is collecting.
        if(state == State.COLLECTING) destination = null;
    });

    /**
     * Finds the destination to go to.
     *
     * @return The destination to go to.
     */
    public Vec3 find(){
        // Sorts the lists.
        grown.sort(Comparator.comparingDouble(player.getPositionVector()::distanceTo));
        empty.sort(Comparator.comparingDouble(player.getPositionVector()::distanceTo));

        // Raytraces and removes non visible crops.
        if(crop == Crop.SUGARCANE)
            grown = grown.stream().filter(position -> !MathUtil.rayTrace(player.getPositionEyes(1), position.addVector(0, 0.5, 0))).collect(Collectors.toList());

        // Sets the state to planting if no fully grown crop.
        if(!empty.isEmpty()){
            // Sets the start to harvesting.
            state = State.PLANTING;

            // Returns the empty plot that needs to be seeded.
            return empty.get(0);
        }

        // Sets the state to planting if no fully grown crop.
        if(!grown.isEmpty()){

            // Sets the start to harvesting.
            state = State.HARVESTING;

            // Returns the first fully grown crop.
            return grown.get(0);
        }

        // Sets the state to resting.
        state = State.RESTING;

        // Will return a null destination.
        return null;
    }

    /**
     * Finds the nearest chest that can be deposited in.
     *
     * @return the nearest chest that can be deposited in.
     */
    public Vec3 findDepositChest(){
        // A set of all the adjacent chests.
        Set<String> adjacents = new HashSet<>();

        // Lists of all valid positions.
        List<Vec3> positions = new ArrayList<>();

        // Iterates through all tile entities.
        for(TileEntity tileEntity : minecraft.world.loadedTileEntityList){
            // Continues if the tile entity isn't a chest or the tile entity is a known adjacent.
            if(!(tileEntity instanceof TileEntityChest) || adjacents.contains(tileEntity.getPos().toString())) continue;

            double y = tileEntity.getPos().getY() + 0.5;

            // Continues if out of vertical reach.
            if(Math.abs(y - player.getPositionEyes(1).yCoord) > 4.5) continue;

            // The tile entity as a chest.
            TileEntityChest chest = (TileEntityChest) tileEntity;

            // The sign above the chest.
            IBlockState above = minecraft.world.getBlockState(tileEntity.getPos().offsetUp());

            if(above.getBlock() instanceof BlockSign){
                // Gets the sign.
                TileEntitySign sign = (TileEntitySign) minecraft.world.getTileEntity(tileEntity.getPos().offsetUp());

                // If the sign contains the keyword.
                boolean valid = false;

                // Iterates through all the text to make the chest valid.
                for(IChatComponent textComponent : sign.signText)
                    if(textComponent.getUnformattedText().toLowerCase().contains("<Deposit>"))
                        valid = true;

                // The adjacent chest. (If any)
                TileEntityChest adjacent = null;

                // Checks for adjacent chests.
                for(EnumFacing enumFacing : new EnumFacing[]{EnumFacing.NORTH, EnumFacing.SOUTH, EnumFacing.EAST, EnumFacing.WEST})
                    if(chest.getAdjacentChest(enumFacing) != null)
                        adjacent = chest.getAdjacentChest(enumFacing);

                // Adds the adjacent position.
                if(adjacent != null) adjacents.add(adjacent.getPos().toString());

                // The position of the chest.
                Vec3 position = chest.getPos().toVector();

                // Adds the adjacent.
                if(adjacent != null)
                    position = position.add(adjacent.getPos().toVector()).scale(0.5);

                // Continues if the chest can't be seen.
                if(MathUtil.rayTrace(player.getPositionEyes(1), position)) continue;

                // Adds the chest.
                positions.add(position);
            }
        }

        // Returns the closest chest.
        return positions.stream()
                .sorted((first, second) -> (int) (second.distanceTo(player.getPositionVector()) - first.distanceTo(player.getPositionVector())))
                .findFirst().orElse(null);
    }

    /**
     * If the block state is a valid crop.
     *
     * @param blockState The given block state.
     * @return If the block state is a valid crop.
     */
    private static boolean isCropValid(IBlockState blockState){
        // Return false if the block isn't a crop.
        if(!(blockState.getBlock() instanceof BlockCrops)) return false;

        // The block state in crop form.
        BlockCrops crop = (BlockCrops) blockState.getBlock();

        // Returns if the crop is fully mature.
        return false;
    }

    /**
     * Scans for crops.
     */
    public void scan(){
        // Iterates through a square with the side length of 2 * radius.
        for(int x = -radius; x < radius; x++){
            for(int z = -radius; z < radius; z++){
                // The position of the block.
                BlockPos position = new BlockPos(MathHelper.floor(player.posX) + x, MathHelper.floor(player.posY + 0.1), MathHelper.floor(player.posZ) + z);

                // The block state at the position.
                IBlockState blockState = minecraft.world.getBlockState(position);

                // The type of the block under the current position.
                Block under = minecraft.world.getBlockState(position.offsetDown()).getBlock();

                // The destination to go to.
                Vec3 destination = new Vec3(position.getX() + 0.5, position.getY(), position.getZ() + 0.5);

                switch(crop){
                    case GENERIC:
                        if(under instanceof BlockFarmland && blockState.getBlock() instanceof BlockAir){
                            BlockFarmland blockFarmland = (BlockFarmland) under;

                            if(blockFarmland.getMetaFromState(minecraft.world.getBlockState(position.offsetDown())) == 0) continue;

                            // Add the crop to the list.
                            if(!empty.contains(destination)) empty.add(destination);
                        }

                        // Continue searching if the crop isn't valid.
                        if(!isCropValid(blockState)) continue;

                        // Add the crop to the list.
                        if(!grown.contains(destination)) grown.add(destination);
                        break;
                    case SUGARCANE:
                        // Continues if the block under the position ion't sand or water.
                        if(!(under instanceof BlockSand)) continue;

                        // If water is present nearby.
                        boolean water = false;

                        // Checks for water.
                        for(EnumFacing direction : new EnumFacing[]{EnumFacing.NORTH, EnumFacing.SOUTH, EnumFacing.EAST, EnumFacing.WEST})
                            if(minecraft.world.getBlockState(position.offsetDown().offset(direction)).getBlock() == Blocks.water)
                                water = true;

                        // Continues if no water is present.
                        if(!water) continue;

                        // Adds the empty plot to the list and returns if no reeds are found.
                        if(!(blockState.getBlock() instanceof BlockReed)){
                            empty.add(destination);
                        }else{
                            // Adds the position above if the crop is grown.
                            if(minecraft.world.getBlockState(position.offsetUp()).getBlock() instanceof BlockReed)
                                grown.add(destination.addVector(0, 1, 0));
                        }
                        break;
                }
            }
        }
    }

    /**
     * The crop to be harvested with the current task.
     */
    enum Crop {

        // The generic wheat, carrots, potatoes, etc.
        GENERIC,
        SUGARCANE

    }

    /**
     * States used to handle movement.
     */
    enum State {

        RESTING,
        HARVESTING,
        COLLECTING,
        DEPOSITING,
        PLANTING

    }

}