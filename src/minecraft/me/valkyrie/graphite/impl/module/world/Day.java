package me.valkyrie.graphite.impl.module.world;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import net.minecraft.network.play.server.S03PacketTimeUpdate;
import net.minecraft.network.play.server.S06PacketUpdateHealth;

/**
 * Makes the time of day noon.
 *
 * @author Zeb.
 * @since 4/26/2017
 */
@ModuleManifest(label = "Day", color = 0xFFf9f0a9, type = Category.WORLD, visible = false)
public class Day extends Module implements EventHandler<PacketEvent> {

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return PacketEvent.class;
    }

    /**
     * Makes the time of day noon.
     */
    @Override
    public void handle(PacketEvent event){
        // Makes the time noon.
        if(minecraft.world != null)
            minecraft.world.setWorldTime(6000);

        // Returns if the packet isn't a time update.
        if(!(event.getPacket() instanceof S03PacketTimeUpdate))
            return;

        // The packet being received.
        S03PacketTimeUpdate packet = (S03PacketTimeUpdate) event.getPacket();

        // Sets the time to noon.
        packet.setWorldTime(6000);
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(this);

        // Makes the time noon.
        if(minecraft.world != null)
            minecraft.world.setWorldTime(6000);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(this);
    }

}
