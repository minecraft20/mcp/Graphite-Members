package me.valkyrie.graphite.impl.module.world;

import java.util.ArrayList;
import java.util.List;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.helper.RotationHelper;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Increment;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import net.minecraft.block.*;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;

/**
 * Automatically mines the blocks nearby the player.
 *
 * @author Zeb.
 * @since 4/23/2017
 */
@ModuleManifest(label = "Nuker", color = 0xFFef600e, type = Category.WORLD)
public class Nuker extends Module {

    /**
     * The reach of the nuker.
     */
    @Property(label = "Delay", aliases = "Speed", description = "How long the nuker waits between blocks.")
    @Clamp(min = "0", max = "6")
    private int delay = 1;

    /**
     * The reach of the nuker.
     */
    @Property(label = "Reach", aliases = "Range", description = "The reach of the nuker.")
    @Clamp(min = "1", max = "6")
    @Increment("0.5")
    private double reach = 4.5;

    /**
     * If the nuker mines flat.
     */
    @Property(label = "Flat", description = "If the nuker mines flat.")
    private boolean flat = true;

    /**
     * The direction to interact with the block with.
     */
    private EnumFacing facing = null;

    /**
     * The current block being mined.
     */
    private BlockPos position = null;

    /**
     * Delay used to managed cooldown.
     */
    private int ticks = 0;

    /**
     * The last damage.
     */
    private float damage = 0;

    /**
     * Handles breaking.
     */
    private EventHandler<PlayerMotionUpdateEvent> motionUpdateEventHandler = new EventHandler<PlayerMotionUpdateEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerMotionUpdateEvent.class;
        }

        /**
         * Handles block breaking.
         */
        @Override
        public void handle(PlayerMotionUpdateEvent event){

            // Check to handle cooldown.
            if(ticks > 0){
                // Lowers the delay.
                ticks--;

                // Returns so nothing is mined.
                return;
            }

            // Handles event differently depending on event type.
            if(event.getEventType() == Type.PRE){
                // Finds a new block if the block has been broken or there is no previous block.
                if(position == null) position = scan();

                // Returns if no block can be found.
                if(position == null) return;

                // Calculates the rotations to the block.
                float[] rotations = RotationHelper.INSTANCE.getRotations(getPositionByFace(position, facing));

                // Faces the block server side.
                event.setYaw(rotations[0]);
                event.setPitch(rotations[1]);
            } else{
                // Return if no block is found.
                if(position == null) return;

                // The block type being mined.
                Block block = minecraft.world.getBlockState(position).getBlock();

                // Swings the players item.
                player.swingItem();

                // Please the breaking sound.
                if(player.ticksExisted % 4 == 0){
                    minecraft.getSoundHandler().playSound(new PositionedSoundRecord(
                            new ResourceLocation(block.stepSound.getStepSound()),
                            (block.stepSound.getVolume() + 1.0F) / 8.0F,
                            block.stepSound.getFrequency() * 0.5F,
                            (float) position.getX() + 0.5F,
                            (float) position.getY() + 0.5F,
                            (float) position.getZ() + 0.5F));
                }

                // Starts breaking the block if the damage is 0.
                if(damage == 0)
                    player.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, position, facing));

                // Sets the damage to 1 if the player is in creative mode, so the block is broken instantly.
                if(minecraft.playerController.isInCreativeMode()) damage = 1;

                // The block state at the position in the world.
                IBlockState blockState = minecraft.world.getBlockState(position);

                // Increments the damage.
                damage += blockState.getBlock().getPlayerRelativeBlockHardness(minecraft.player, minecraft.world, position);

                // Updates the block damage.
                minecraft.world.sendBlockBreakProgress(player.getEntityId(), position, (int) (damage * 10) - 1);

                // Sets the player controller damage to the damage.
                minecraft.playerController.curBlockDamageMP = damage;

                // Return if the damage is less than 100%.
                if(damage < 1) return;

                // Plays the destroy sound.
                minecraft.world.playAuxSFX(2001, position, Block.getStateId(blockState));

                // Sends a block break packet.
                player.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.STOP_DESTROY_BLOCK, position, facing));

                // Handles block break client sided.
                minecraft.playerController.onPlayerDamageBlock(position, EnumFacing.UP);

                // Resets the damage.
                damage = 0;

                // Resets the block position so a new block is scanned.
                position = null;

                // Sets the delay to the cooldown.
                ticks = delay;
            }
        }
    };

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(motionUpdateEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(motionUpdateEventHandler);

        // Resets the block position so a new block is scanned.
        position = null;

        facing = null;
    }

    /**
     * Scans for a block position nearby to mine.
     *
     * @return The block position to mine.
     */
    private BlockPos scan(){
        // All the valid block positions to be sorted.
        List<BlockPos> positions = new ArrayList<>();

        // How far a block can be hit from, increased for scanning.
        double radius = this.reach + 0.5;

        // Looks through -radius -radius -radius to radius radius radius offset by the players position.
        for(int y = MathHelper.ceiling_double_int(radius); y > -1; y--){
            for(int x = -MathHelper.floor(radius); x < MathHelper.ceiling_double_int(radius); x++){
                for(int z = -MathHelper.floor(radius); z < MathHelper.ceiling_double_int(radius); z++){

                    // The position of the block.
                    BlockPos position = new BlockPos(MathHelper.floor(player.posX) + x,
                            MathHelper.floor(player.posY + 0.02) + y,
                            MathHelper.floor(player.posZ) + z);

                    // The center of the block.
                    Vec3 center = new Vec3(position.getX() + 0.5, position.getY() + 0.5, position.getZ() + 0.5);

                    // The block state at the position.
                    IBlockState blockState = minecraft.world.getBlockState(position);

                    // Continues if the block is air or liquid.
                    if(blockState.getBlock() instanceof BlockAir || blockState.getBlock() instanceof BlockLiquid ||
                            blockState.getBlock() instanceof BlockTallGrass || blockState.getBlock() instanceof BlockFlower)
                        continue;

                    // Continues searching if the block is too far away.
                    if(new Vec3(Math.floor(player.posX) + 0.5,
                            player.posY + player.getEyeHeight(),
                            Math.floor(player.posZ) + 0.5).distanceTo(center) > reach)
                        continue;

                    // Continues if the block isn't visible.
                    if(rayTraceFaces(position) == null) continue;

                    // Adds the position to the list of valid positions.
                    positions.add(position);
                }
            }
        }

        // Look under the player if there are no blocks at aor above them.
        if(positions.isEmpty()){
            // Looks through (-radius, -radius, -radius) to (radius, radius, radius,) offset by the players position.
            for(int y = -1; y > -MathHelper.floor(radius); y--){

                // Breaks the loop if flat mode is enabled.
                if(flat) break;

                for(int x = -MathHelper.floor(radius); x < MathHelper.ceiling_double_int(radius); x++){
                    for(int z = -MathHelper.floor(radius); z < MathHelper.ceiling_double_int(radius); z++){

                        // The position of the block.
                        BlockPos position = new BlockPos(MathHelper.floor(player.posX) + x,
                                MathHelper.floor(player.posY) + y,
                                MathHelper.floor(player.posZ) + z);

                        // The center of the block.
                        Vec3 center = new Vec3(position.getX() + 0.5, position.getY() + 0.5, position.getZ() + 0.5);

                        // The block state at the position.
                        IBlockState blockState = minecraft.world.getBlockState(position);

                        // Continues if the block is air or liquid.
                        if(blockState.getBlock() instanceof BlockAir || blockState.getBlock() instanceof BlockLiquid)
                            continue;

                        // Continues searching if the block is too far away.
                        if(new Vec3(Math.floor(player.posX) + 0.5,
                                player.posY + player.getEyeHeight(),
                                Math.floor(player.posZ) + 0.5).distanceTo(center) > reach)
                            continue;

                        // Continues if the block isn't visible.
                        if(rayTraceFaces(position) == null) continue;

                        // Adds the position to the list of valid positions.
                        positions.add(position);
                    }
                }
            }
        }

        // Sorts the block positions by distance.
        positions.sort((first, second) -> {
            // The center of the first block.
            Vec3 firstCenter = new Vec3(first.getX() + 0.5, first.getY() + 0.5, first.getZ() + 0.5);

            // The center of the second block.
            Vec3 secondCenter = new Vec3(second.getX() + 0.5, second.getY() + 0.5, second.getZ() + 0.5);

            // Returns the index of the closer block to the player.
            return player.getPositionEyes(1).distanceTo(firstCenter) > player.getPositionEyes(1).distanceTo(secondCenter) ? 1 : -1;
        });

        if(!positions.isEmpty()){
            // The position of the block.
            BlockPos position = positions.get(0);

            // Finds the facing direction.
            this.facing = rayTraceFaces(position);

            // Returns the block position.
            return positions.get(0);
        } else{
            // Sets the facing to null.
            this.facing = null;

            // Returns no block.
            return null;
        }
    }

    /**
     * Ray traces to see if the position is visible from the origin.
     *
     * @param origin   The origin that the ray will be checked from.
     * @param position The position to have the visibility checked.
     * @return If the position is visible.
     */
    private boolean rayTrace(Vec3 origin, Vec3 position){

        // Gets the difference between the vectors.
        Vec3 difference = position.subtract(origin);

        // The number of steps to check.
        int steps = 10;

        // The x step.
        double x = difference.xCoord / steps;

        // The y step.
        double y = difference.yCoord / steps;

        // The z step.
        double z = difference.zCoord / steps;

        // The point used to check collision.
        Vec3 point = origin;

        // A bounding box with the corners being the origin and the position.
        AxisAlignedBB collisionChunk = new AxisAlignedBB(origin, position);

        // Iterates through all the steps
        for(int i = 0; i < steps; i++){
            // Adds the step value to the point.
            point = point.addVector(x, y, z);
            // The block position at the vector.
            BlockPos blockPosition = new BlockPos(point);

            // The block pos of the position.
            BlockPos destination = new BlockPos(position);

            // The block state at the point.
            IBlockState blockState = minecraft.world.getBlockState(blockPosition);

            // Continue if the block is liquid or air.
            if(blockState.getBlock() instanceof BlockLiquid || blockState.getBlock() instanceof BlockAir) continue;

            // The block bounding box.
            AxisAlignedBB boundingBox = blockState.getBlock().getCollisionBoundingBox(minecraft.world, blockPosition, blockState);

            // Sets the bounding box to an empty bounding box if the box is null.
            if(boundingBox == null)
                boundingBox = new AxisAlignedBB(0, 0, 0, 0, 0, 0);

            // Offsets the bounding box by the block position.
            boundingBox = boundingBox.offset(blockPosition);

            // Returns false if the point is in a block.
            if(boundingBox.isVecInside(point)) return false;
        }

        // Returns true at the moment for testing purposes.
        return true;
    }

    /**
     * Ray traces all the block faces on a block.
     *
     * @param position The block position to check.
     */
    private EnumFacing rayTraceFaces(BlockPos position){
        // Iterates through all the facing values.
        for(EnumFacing facing : EnumFacing.values()){

            // Returns that side if no obstruction was found.
            if(rayTrace(player.getPositionEyes(1), getPositionByFace(position, facing)))
                return facing;
        }

        // Return null if all sides had some obstruction.
        return null;
    }

    /**
     * Gets the position of the block offset by the facing direction.
     *
     * @param position The block position.
     * @param facing   The facing direction to get the position of.
     * @return The position of the block offset by the facing direction.
     */
    private Vec3 getPositionByFace(BlockPos position, EnumFacing facing){
        // The offset for the facing direction.
        Vec3 offset = new Vec3(facing.getDirectionVec().getX() / 1.99d, facing.getDirectionVec().getY() / 1.99d, facing.getDirectionVec().getZ() / 1.99d);

        // The point in the center of the block
        Vec3 point = new Vec3(position.getX() + 0.5, position.getY() + 0.5, position.getZ() + 0.5);

        // Returns the point offset by the facing offset.
        return point.add(offset);
    }

}
