package me.valkyrie.graphite.impl.module.world;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.helper.IHelper;
import me.valkyrie.graphite.api.helper.ProfileHelper;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import me.valkyrie.graphite.impl.event.game.Direction;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import net.minecraft.network.play.server.S38PacketPlayerListItem;

import java.util.UUID;

/**
 * Detects when staff members on the current server go into vanish mode.
 *
 * @author Zeb.
 * @since 4/28/2017
 */
@ModuleManifest(label = "Vanish_detector", color = 0xFF4fb9d1, type = Category.WORLD, visible = false)
public class VanishDetector extends Module implements EventHandler<PacketEvent> {

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return PacketEvent.class;
    }

    /**
     * Searches for player's not found in the tab list.
     */
    @Override
    public void handle(PacketEvent event){
        // Returns if packet, type, or direction isn't the desired value.
        if(event.getEventType() != Type.PRE || event.getDirection() != Direction.INCOMING || !(event.getPacket() instanceof S38PacketPlayerListItem))
            return;

        // The packet being received.
        S38PacketPlayerListItem packet = (S38PacketPlayerListItem) event.getPacket();

        // Goes through all of the player data.
        packet.func_179767_a().forEach(object -> {

            // Continues with for each if object isn't a add player data packet.
            if(!(object instanceof S38PacketPlayerListItem.AddPlayerData)) return;

            // The player data.
            S38PacketPlayerListItem.AddPlayerData data = (S38PacketPlayerListItem.AddPlayerData) object;

            // If a matching player is found in the tab-list.
            boolean match = minecraft.getNetHandler().func_175106_d().stream().anyMatch(networkPlayerInfo
                    -> networkPlayerInfo.func_178845_a().getId().equals(data.func_179962_a().getId()));

            // Starts the vanish detection thread if the player isn't found.
            if(!match && packet.func_179768_b() == S38PacketPlayerListItem.Action.UPDATE_LATENCY)
                new VanishDetectionThread(data.func_179962_a().getId()).start();

        });
    }

    /**
     * Thread used to run uuid checks.
     */
    class VanishDetectionThread extends Thread {

        /**
         * The player's uuid.
         */
        private UUID uuid;


        public VanishDetectionThread(UUID uuid){
            this.uuid = uuid;
        }

        /**
         * Runs uuid check to get the name of the player.
         */
        @Override
        public void run(){
            // The name of the player.
            String name = ProfileHelper.INSTANCE.getName(uuid);

            // Return if there is no name.
            if(name == "")
                return;

            // TODO: TEMP TEMP TEMP REPLACE WITH NOTIFICATION.
            clientChatMsg().appendText("Player ").appendText(name, ChatColor.GRAY).appendText(" has recently entered vanish.").send();
        }

    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(this);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(this);
    }

}
