package me.valkyrie.graphite.impl.module.world;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.event.Type;
import me.valkyrie.graphite.api.helper.RotationHelper;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import me.valkyrie.graphite.api.util.game.GameTimer;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.player.PlayerMotionUpdateEvent;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.util.*;

import java.util.Random;

/**
 * Automatically places blocks under the player.
 *
 * @author Zeb.
 * @since 5/8/2017
 */
@ModuleManifest(label = "Scaffold", color = 0xFF48eae2, type = Category.WORLD)
public class Scaffold extends Module {

    /**
     * If the scaffold should tower up.
     */
    @Property(label = "Tower", aliases = "Vertical", description = "If the scaffold should tower up.")
    private boolean tower = true;

    /**
     * Timer used to handle block place delay.
     */
    private GameTimer timer = new GameTimer();

    /**
     * The block entry used to place the block.
     */
    private BlockEntry blockEntry = null;

    /**
     * Handles breaking.
     */
    private EventHandler<PlayerMotionUpdateEvent> motionUpdateEventHandler = new EventHandler<PlayerMotionUpdateEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PlayerMotionUpdateEvent.class;
        }

        /**
         * Handles block breaking.
         */
        @Override
        public void handle(PlayerMotionUpdateEvent event){

            // Returns if the player isn't holding a block.
            if(getSlotWithBlock() == -1)
                return;

            // Handles towering, this is a bit of a botched fix but it works.
            if(!player.onGround && player.motionY < 0.24 && player.motionY > 0 && tower
                    && player.rotationPitch > 85 && tower)
                player.motionY = -10;

            if(event.getEventType() == Type.PRE){

                // Jumps if tower is enabled, the player is on ground and looking down.
                if(tower && player.rotationPitch >= 85 && player.onGround)
                    player.jump();

                // Tries to find a new block to place on.
                blockEntry = find();

                // Returns if no block is found.
                if(blockEntry == null) return;

                // Gets the rotations to the
                float[] rotations = RotationHelper.INSTANCE.getRotations(getPositionByFace(blockEntry.getPosition(), blockEntry.getFacing()));

                // Sets the rotations.
                event.setYaw(rotations[0]);
                event.setPitch(rotations[1]);

            } else{
                // Returns if no block is found.
                if(blockEntry == null) return;

                // Gets the rotations to the
                float[] rotations = RotationHelper.INSTANCE.getRotations(getPositionByFace(blockEntry.getPosition(), blockEntry.getFacing()));

                int currentSlot = player.inventory.currentItem;

                int slot = getSlotWithBlock();

                player.inventory.currentItem = slot;
                player.sendQueue.addToSendQueue(new C09PacketHeldItemChange(slot));

                // Swings the player's hand.
                player.swingItem();

                // Places the block server side.
                minecraft.playerController.func_178890_a(minecraft.player, minecraft.world, player.getHeldItem(), blockEntry.getPosition(),
                        blockEntry.getFacing(), new Vec3(blockEntry.getPosition().getX(), blockEntry.getPosition().getY(), blockEntry.getPosition().getZ()));

                player.inventory.currentItem = currentSlot;
                player.sendQueue.addToSendQueue(new C09PacketHeldItemChange(currentSlot));

                // Nulls the block entry.
                blockEntry = null;

                timer.reset();
            }
        }

    };

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(motionUpdateEventHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(motionUpdateEventHandler);
    }

    /**
     * Finds a position and face to place the block against.
     *
     * @return The block entry with position and face.
     */
    public BlockEntry find(){
        // All the inverted directions.
        EnumFacing[] invert = new EnumFacing[]{EnumFacing.UP, EnumFacing.DOWN, EnumFacing.SOUTH, EnumFacing.NORTH, EnumFacing.EAST, EnumFacing.WEST};

        // The block that is empty under the player.
        BlockPos position = new BlockPos(player.getPositionVector()).offset(EnumFacing.DOWN);

        // Return null if there is already a block there.
        if(!(minecraft.world.getBlockState(position).getBlock() instanceof BlockAir)) return null;

        // Iterates through all the directions.
        for(EnumFacing facing : EnumFacing.values()){

            // The position offset by the face.
            BlockPos offset = position.offset(facing);

            // The block state of the offset.
            IBlockState blockState = minecraft.world.getBlockState(offset);


            // Returns the block entry with the offset position and the inverted facing if the block can be collided with.
            if(!(minecraft.world.getBlockState(offset).getBlock() instanceof BlockAir)
                    && rayTrace(player.getPositionEyes(0f), getPositionByFace(offset, invert[facing.ordinal()])))
                return new BlockEntry(offset, invert[facing.ordinal()]);
        }

        BlockPos[] offsets = new BlockPos[]{new BlockPos(-1, 0, 0), new BlockPos(1, 0, 0), new BlockPos(0, 0, -1), new BlockPos(0, 0, 1)};

        if(player.onGround){
            for(BlockPos offset : offsets){

                // The position offset by the face.
                BlockPos offsetPos = position.add(offset.getX(), 0, offset.getZ());

                // The block state of the offset.
                IBlockState blockState = minecraft.world.getBlockState(offsetPos);

                // Returns the block entry with the offset position and the inverted facing if the block can be collided with.
                if(minecraft.world.getBlockState(offsetPos).getBlock() instanceof BlockAir){

                    // Iterates through all the directions.
                    for(EnumFacing facing : EnumFacing.values()){

                        // The position offset by the face.
                        BlockPos offset2 = offsetPos.offset(facing);

                        // The block state of the offset.
                        IBlockState blockState2 = minecraft.world.getBlockState(offset2);

                        // Returns the block entry with the offset position and the inverted facing if the block can be collided with.
                        if(!(minecraft.world.getBlockState(offset2).getBlock() instanceof BlockAir))
                            return new BlockEntry(offset2, invert[facing.ordinal()]);
                    }
                }
            }
        }

        // Returns null if no block is found.
        return null;
    }

    /**
     * Ray traces to see if the position is visible from the origin.
     *
     * @param origin   The origin that the ray will be checked from.
     * @param position The position to have the visibility checked.
     * @return If the position is visible.
     */
    private boolean rayTrace(Vec3 origin, Vec3 position){

        // Gets the difference between the vectors.
        Vec3 difference = position.subtract(origin);

        // The number of steps to check.
        int steps = 10;

        // The x step.
        double x = difference.xCoord / steps;

        // The y step.
        double y = difference.yCoord / steps;

        // The z step.
        double z = difference.zCoord / steps;

        // The point used to check collision.
        Vec3 point = origin;

        // A bounding box with the corners being the origin and the position.
        AxisAlignedBB collisionChunk = new AxisAlignedBB(origin, position);

        // Iterates through all the steps
        for(int i = 0; i < steps; i++){
            // Adds the step value to the point.
            point = point.addVector(x, y, z);
            // The block position at the vector.
            BlockPos blockPosition = new BlockPos(point);

            // The block pos of the position.
            BlockPos destination = new BlockPos(position);

            // The block state at the point.
            IBlockState blockState = minecraft.world.getBlockState(blockPosition);

            // Continue if the block is liquid or air.
            if(blockState.getBlock() instanceof BlockLiquid || blockState.getBlock() instanceof BlockAir) continue;

            // The block bounding box.
            AxisAlignedBB boundingBox = blockState.getBlock().getCollisionBoundingBox(minecraft.world, blockPosition, blockState);

            // Sets the bounding box to an empty bounding box if the box is null.
            if(boundingBox == null)
                boundingBox = new AxisAlignedBB(0, 0, 0, 0, 0, 0);

            // Offsets the bounding box by the block position.
            boundingBox = boundingBox.offset(blockPosition);

            // Returns false if the point is in a block.
            if(boundingBox.isVecInside(point)) return false;
        }

        // Returns true at the moment for testing purposes.
        return true;
    }

    private int getSlotWithBlock(){
        for(int i = 0; i < 9; i++){
            ItemStack itemStack = player.inventory.mainInventory[i];

            if(itemStack != null && itemStack.getItem() instanceof ItemBlock)
                return i;
        }

        return -1;
    }

    /**
     * Gets the position of the block offset by the facing direction.
     *
     * @param position The block position.
     * @param facing   The facing direction to get the position of.
     * @return The position of the block offset by the facing direction.
     */
    public Vec3 getPositionByFace(BlockPos position, EnumFacing facing){
        // The offset for the facing direction.
        Vec3 offset = new Vec3(facing.getDirectionVec().getX() / 2d, facing.getDirectionVec().getY() / 2d, facing.getDirectionVec().getZ() / 2d);

        // The point in the center of the block
        Vec3 point = new Vec3(position.getX() + 0.5, position.getY() + 0.5, position.getZ() + 0.5);

        // Returns the point offset by the facing offset.
        return point.add(offset);
    }

    /**
     * Class used to handle side scanning.
     */
    class BlockEntry {

        /**
         * The block to place against.
         */
        private BlockPos position;

        /**
         * The side to interact with.
         */
        private EnumFacing facing;

        public BlockEntry(BlockPos position, EnumFacing facing){
            this.position = position;
            this.facing = facing;
        }

        /**
         * @return The block to place against.
         */
        public BlockPos getPosition(){
            return position;
        }

        /**
         * @return The face to interact with.
         */
        public EnumFacing getFacing(){
            return facing;
        }
    }

}
