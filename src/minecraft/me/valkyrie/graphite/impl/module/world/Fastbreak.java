package me.valkyrie.graphite.impl.module.world;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Increment;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import me.valkyrie.graphite.impl.event.player.PlayerUpdateEvent;
import net.minecraft.network.play.server.S03PacketTimeUpdate;

/**
 * Makes the time of day noon.
 *
 * @author Zeb.
 * @since 4/26/2017
 */
@ModuleManifest(label = "Fast_break", color = 0xFFd17ae2, type = Category.WORLD)
public class Fastbreak extends Module implements EventHandler<PlayerUpdateEvent> {

    @Property(label = "Minimum")
    @Clamp(min="0", max = "1")
    @Increment("0.05")
    private float minimum = 0.7f;

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return PlayerUpdateEvent.class;
    }

    /**
     * Makes the time of day noon.
     */
    @Override
    public void handle(PlayerUpdateEvent event){
        if(minecraft.playerController.curBlockDamageMP > minimum)
            minecraft.playerController.curBlockDamageMP = 1;

        minecraft.playerController.blockHitDelay = 0;
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        EventManager.INSTANCE.register(this);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(this);
    }

}
