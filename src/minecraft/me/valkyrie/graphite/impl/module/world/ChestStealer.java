package me.valkyrie.graphite.impl.module.world;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.module.ModuleManifest;
import me.valkyrie.graphite.api.util.property.Clamp;
import me.valkyrie.graphite.api.util.property.Property;
import me.valkyrie.graphite.impl.event.game.GameTickEvent;
import me.valkyrie.graphite.impl.event.game.PacketEvent;
import net.minecraft.block.BlockChest;
import net.minecraft.block.BlockEnderChest;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Items;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.server.S2DPacketOpenWindow;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Zeb.
 * @since 5/18/2017
 */
@ModuleManifest(label = "Chest_Stealer", color = 0xFF138493, type = Category.WORLD)
public final class ChestStealer extends Module {

    /**
     * How long the cheststealer should wait before picking up another item.
     */
    @Property(label = "Delay", aliases = {"Speed", "Ticks"},
            description = "How long the cheststealer should wait before picking up another item.")
    @Clamp(min = "1", max = "5")
    private int delay = 1;

    /**
     * If the cheststealer should take items randomly.
     */
    @Property(label = "Random", description = "If the cheststealer should take items randomly.")
    private boolean random = true;

    /**
     * The current slot the player is iterating through.
     */
    private int currentSlot = 0;

    /**
     * Tick counter to handle cool-downs.
     */
    private int tickDelay;

    private final EventHandler<GameTickEvent> eventGameTickHandler = new EventHandler<GameTickEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return GameTickEvent.class;
        }

        /**
         * Handles item taking.
         */
        @Override
        public void handle(GameTickEvent event){

            // Returns if the player isn't in game.
            if(player == null) return;

            if(player.openContainer != null && player.openContainer instanceof ContainerChest){

                // The chest the player is looking in.
                ContainerChest container = (ContainerChest) player.openContainer;

                // Returns if the inventory is empty.
                if(isEmpty()){
                    player.closeScreen();
                    return;
                }

                // Returns if cool-down hasn't been finished.
                if(tickDelay > 0){
                    tickDelay--;
                    return;
                }

                // Gets the slot to take from
                if(random)
                    currentSlot = getRandomSlot();
                else
                    currentSlot++;

                // Returns if slot is negative.
                if(currentSlot == -1)
                    return;

                // Takes the item.
                if(currentSlot < container.getLowerChestInventory().getSizeInventory()){

                    // Swaps the item into the player's inventory.
                    minecraft.playerController.windowClick(player.inventoryContainer.windowId, currentSlot, 0, 2, player);

                    // Handles cool-down delay.
                    tickDelay = delay;
                }
            }
        }

    };

    /**
     * Handles slot resetting.
     */
    private EventHandler<PacketEvent> packetEventHandler = new EventHandler<PacketEvent>() {

        /**
         * @return the class of the event that should be handled.
         */
        @Override
        public Class type(){
            return PacketEvent.class;
        }

        /**
         * Handles slot resetting.
         */
        @Override
        public void handle(PacketEvent event){
            // Returns if the packet isn't a right click packet.
            if(!(event.getPacket() instanceof S2DPacketOpenWindow))
                return;

            // The packet being sent.
            S2DPacketOpenWindow packet = (S2DPacketOpenWindow) event.getPacket();

            // Resets the slot.
            currentSlot = -1;
        }
    };

    /**
     * @return if the current chest is empty.
     */
    private boolean isEmpty(){
        if(player.openContainer != null && player.openContainer instanceof ContainerChest){
            ContainerChest container = (ContainerChest) player.openContainer;
            for(int i = 0; i < container.getLowerChestInventory().getSizeInventory(); i++){
                ItemStack itemStack = container.getLowerChestInventory().getStackInSlot(i);

                if(itemStack != null && itemStack.getItem() != null) return false;
            }
        }

        return true;
    }

    /**
     * @return a random slot that the chest stealer should take.
     */
    private int getRandomSlot(){
        if(isEmpty()) return -1;

        if(player.openContainer != null && player.openContainer instanceof ContainerChest){
            ContainerChest container = (ContainerChest) player.openContainer;

            List<Integer> filled = new ArrayList<>();

            for(int i = 0; i < container.getLowerChestInventory().getSizeInventory(); ++i){
                ItemStack itemStack = container.getLowerChestInventory().getStackInSlot(i);

                if(itemStack != null && itemStack.getItem() != null)
                    filled.add(i);
            }

            return filled.get(new Random().nextInt(filled.size()));
        }

        return -1;
    }

    /**
     * Registers the listeners to the event system.
     */
    @Override
    public void enable(){
        currentSlot = 0;

        EventManager.INSTANCE.register(packetEventHandler);
        EventManager.INSTANCE.register(eventGameTickHandler);
    }

    /**
     * Unregisters the listeners from the event system.
     */
    @Override
    public void disable(){
        EventManager.INSTANCE.unregister(packetEventHandler);
        EventManager.INSTANCE.unregister(eventGameTickHandler);
    }

}
