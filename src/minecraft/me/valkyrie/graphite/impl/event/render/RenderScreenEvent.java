package me.valkyrie.graphite.impl.event.render;

import me.valkyrie.graphite.api.event.Event;

/**
 * Called when the screen is rendered.
 * <p>
 * Created by Zeb on 4/21/2017.
 */
public class RenderScreenEvent extends Event {
}
