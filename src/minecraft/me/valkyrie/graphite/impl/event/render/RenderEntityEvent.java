package me.valkyrie.graphite.impl.event.render;

import me.valkyrie.graphite.api.event.Event;
import net.minecraft.entity.Entity;

/**
 * @author Zeb.
 * @since 6/28/2017.
 */
public class RenderEntityEvent extends Event {

    private Entity entity;

    public RenderEntityEvent(Entity entity){
        this.entity = entity;
    }

    public Entity getEntity(){
        return entity;
    }
}
