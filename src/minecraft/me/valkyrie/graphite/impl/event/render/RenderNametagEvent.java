package me.valkyrie.graphite.impl.event.render;

import me.valkyrie.graphite.api.event.Event;
import net.minecraft.entity.Entity;

/**
 * Called when a nametag is rendered.
 *
 * @author Zeb.
 * @since 5/1/2017
 */
public class RenderNametagEvent extends Event {

    /**
     * The entity who's nametag is being rendered.
     */
    private Entity entity;

    public RenderNametagEvent(Entity entity){
        this.entity = entity;
    }

    /**
     * @return the entity who's nametag is being rendered.
     */
    public Entity getEntity(){
        return entity;
    }

}
