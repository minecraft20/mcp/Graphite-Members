package me.valkyrie.graphite.impl.event.entity;

import me.valkyrie.graphite.api.event.Event;
import me.valkyrie.graphite.api.event.Type;
import net.minecraft.entity.Entity;

/**
 * @author Zeb.
 * @since 6/1/2017.
 */
public class EntityStepEvent extends Event {

    /**
     * The type of the event.
     */
    private Type type;

    /**
     * The entity stepping.
     */
    private Entity entity;

    /**
     * The step height.
     */
    private float height;

    public EntityStepEvent(Entity entity, Type type){
        this.entity = entity;
        this.type = type;
        this.height = entity.stepHeight;
    }

    public Entity getEntity(){
        return entity;
    }

    public float getHeight(){
        return height;
    }

    public void setHeight(float height){
        this.height = height;
    }

    public Type getType(){
        return type;
    }
}
