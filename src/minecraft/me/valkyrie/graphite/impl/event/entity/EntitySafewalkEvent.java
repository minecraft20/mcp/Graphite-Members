package me.valkyrie.graphite.impl.event.entity;

import me.valkyrie.graphite.api.event.Event;
import net.minecraft.entity.Entity;

/**
 * @author Zeb.
 * @since 5/17/2017
 */
public class EntitySafewalkEvent extends Event {

    private Entity entity;

    public EntitySafewalkEvent(Entity entity){
        this.entity = entity;
    }

    public Entity getEntity(){
        return entity;
    }
}
