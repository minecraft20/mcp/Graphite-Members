package me.valkyrie.graphite.impl.event.entity;

import me.valkyrie.graphite.api.event.Event;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;

/**
 * Called when an entity collides with a block.
 *
 * @author Zeb.
 * @since 4/24/2017
 */
public class EntityCollisionEvent extends Event {

    /**
     * The entity being collided.
     */
    private Entity entity;

    /**
     * The bounding box in the collision.
     */
    private AxisAlignedBB boundingBox;

    /**
     * The position of the collision.
     */
    private BlockPos position;

    public EntityCollisionEvent(Entity entity, AxisAlignedBB boundingBox, BlockPos position){
        this.entity = entity;
        this.boundingBox = boundingBox;
        this.position = position;
    }

    /**
     * @return the entity in the collision.
     */
    public Entity getEntity(){
        return entity;
    }

    /**
     * Sets the event bounding box.
     *
     * @param boundingBox The new bounding box.
     */
    public void setBoundingBox(AxisAlignedBB boundingBox){
        this.boundingBox = boundingBox;
    }

    /**
     * @return the collision's bounding box.
     */
    public AxisAlignedBB getBoundingBox(){
        return boundingBox;
    }

    /**
     * @return the location of the collision.
     */
    public BlockPos getPosition(){
        return position;
    }
}
