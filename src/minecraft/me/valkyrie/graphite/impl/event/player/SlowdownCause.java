package me.valkyrie.graphite.impl.event.player;

/**
 * Created by Zeb on 4/21/2017.
 */
public enum SlowdownCause {

    ITEM,
    SOUL_SAND

}
