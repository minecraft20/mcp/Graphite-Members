package me.valkyrie.graphite.impl.event.player;

import me.valkyrie.graphite.api.event.Event;
import me.valkyrie.graphite.api.event.Type;

/**
 * Created by Zeb on 10/2/2016.
 */
public class PlayerMotionUpdateEvent extends Event {

    /**
     * X position of the event.
     */
    private double x;

    /**
     * Y position of the event.
     */
    private double y;

    /**
     * Z position of the event.
     */
    private double z;

    /**
     * Yaw of the event.
     */
    private float yaw;

    /**
     * Pitch of the event.
     */
    private float pitch;

    /**
     * If the event is onground.
     */
    private boolean onGround;

    /**
     * When the event was called in relation to the sending of the position packets.
     */
    private Type eventType;

    public PlayerMotionUpdateEvent(double x, double y, double z, float yaw, float pitch, boolean onGround, Type eventType){
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
        this.onGround = onGround;
        this.eventType = eventType;
    }

    /**
     * Returns the x position.
     *
     * @return x position.
     */
    public double getX(){
        return x;
    }

    /**
     * Changes the x position.
     *
     * @param x new position x.
     */
    public void setX(double x){
        this.x = x;
    }

    /**
     * Returns the y position.
     *
     * @return y position.
     */
    public double getY(){
        return y;
    }

    /**
     * Changes the y position.
     *
     * @param y new position y.
     */
    public void setY(double y){
        this.y = y;
    }

    /**
     * Returns the z position.
     *
     * @return z position.
     */
    public double getZ(){
        return z;
    }

    /**
     * Changes the z position.
     *
     * @param z new position z.
     */
    public void setZ(double z){
        this.z = z;
    }

    /**
     * Returns the yaw rotation.
     *
     * @return yaw.
     */
    public float getYaw(){
        return yaw;
    }

    /**
     * Sets the yaw rotation.
     *
     * @param yaw new yaw.
     */
    public void setYaw(float yaw){
        this.yaw = yaw % 360;
    }

    /**
     * Returns the yaw rotation.
     *
     * @return yaw.
     */
    public float getPitch(){
        return pitch;
    }

    /**
     * Sets the pitch rotation.
     *
     * @param pitch new pitch.
     */
    public void setPitch(float pitch){
        this.pitch = pitch;
    }

    /**
     * If the player was onGround.
     *
     * @return if the player is onGround.
     */
    public boolean isOnGround(){
        return onGround;
    }

    /**
     * Sets the packet's onground state.
     *
     * @param onGround new onGround state.
     */
    public void setOnGround(boolean onGround){
        this.onGround = onGround;
    }

    /**
     * When the event was fired.
     *
     * @return when the event was fired.
     */
    public Type getEventType(){
        return eventType;
    }
}
