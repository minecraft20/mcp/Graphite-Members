package me.valkyrie.graphite.impl.event.player;

import me.valkyrie.graphite.api.event.Event;
import me.valkyrie.graphite.api.event.Type;

/**
 * Created by Zeb on 4/21/2017.
 */
public class PlayerUpdateEvent extends Event {

    /**
     * The type of the event
     */
    private Type eventType;

    public PlayerUpdateEvent(Type eventType){
        this.eventType = eventType;
    }

    /**
     * @return the event type.
     */
    public Type getEventType(){
        return eventType;
    }
}
