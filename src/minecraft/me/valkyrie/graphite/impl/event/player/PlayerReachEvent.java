package me.valkyrie.graphite.impl.event.player;

import me.valkyrie.graphite.api.event.Event;

/**
 * @author Zeb.
 * @since 6/16/2017.
 */
public class PlayerReachEvent extends Event {

    private float reach;

    public PlayerReachEvent(float reach){
        this.reach = reach;
    }

    public void setReach(float reach){
        this.reach = reach;
    }

    public float getReach(){
        return reach;
    }
}
