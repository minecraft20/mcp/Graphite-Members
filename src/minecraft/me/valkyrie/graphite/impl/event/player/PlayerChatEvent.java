package me.valkyrie.graphite.impl.event.player;

import me.valkyrie.graphite.api.event.Event;

/**
 * Created by Zeb on 4/21/2017.
 */
public class PlayerChatEvent extends Event {

    /**
     * The message said.
     */
    private String message;

    public PlayerChatEvent(String message){
        this.message = message;
    }

    /**
     * @return the message the player said.
     */
    public String getMessage(){
        return message;
    }

}
