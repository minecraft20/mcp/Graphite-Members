package me.valkyrie.graphite.impl.event.player;

import me.valkyrie.graphite.api.event.Event;

/**
 * Handles player movement.
 *
 * @author Zeb.
 * @since 5/8/2017
 */
public class PlayerMoveEvent extends Event {

    /**
     * The x velocity.
     */
    public double x;

    /**
     * The y velocity.
     */
    public double y;

    /**
     * The z velocity.
     */
    public double z;

    public PlayerMoveEvent(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
