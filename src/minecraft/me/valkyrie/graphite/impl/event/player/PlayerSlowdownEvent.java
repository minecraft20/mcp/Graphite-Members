package me.valkyrie.graphite.impl.event.player;

import me.valkyrie.graphite.api.event.Event;

/**
 * Created by Zeb on 4/21/2017.
 */
public class PlayerSlowdownEvent extends Event {

    /**
     * The cause of the slowdown
     */
    private SlowdownCause slowdownCause;

    public PlayerSlowdownEvent(SlowdownCause slowdownCause){
        this.slowdownCause = slowdownCause;
    }

    public SlowdownCause getSlowdownCause(){
        return slowdownCause;
    }
}
