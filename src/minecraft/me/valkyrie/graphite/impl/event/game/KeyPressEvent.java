package me.valkyrie.graphite.impl.event.game;

import me.valkyrie.graphite.api.event.Event;

/**
 * Called when the user presses a key.
 *
 * @author Zeb.
 * @since 4/28/2017
 */
public class KeyPressEvent extends Event {

    /**
     * The key the user pressed.
     */
    private int key;

    public KeyPressEvent(int key){
        this.key = key;
    }

    /**
     * @return the key the user pressed.
     */
    public int getKeyPressed(){
        return key;
    }
}
