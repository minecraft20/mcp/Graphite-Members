package me.valkyrie.graphite.impl.event.game;

import me.valkyrie.graphite.api.event.Event;
import net.minecraft.util.BlockPos;

/**
 * @author Zeb.
 * @since 4/23/2017
 */
public class ClickBlockEvent extends Event {

    /**
     * The block position.
     */
    private BlockPos position;

    public ClickBlockEvent(BlockPos position){
        this.position = position;
    }

    /**
     * @return the block position.
     */
    public BlockPos getPosition(){
        return position;
    }
}
