package me.valkyrie.graphite.impl.event.game;

import me.valkyrie.graphite.api.event.Event;
import net.minecraft.entity.player.EntityPlayer;

/**
 * Called when a player is spawned.
 *
 * @author Zeb.
 * @since 5/8/2017
 */
public class PlayerSpawnEvent extends Event {

    /**
     * The player spawned.
     */
    private EntityPlayer player;

    public PlayerSpawnEvent(EntityPlayer player){
        this.player = player;
    }

    /**
     * @return the player spawned.
     */
    public EntityPlayer getPlayer(){
        return player;
    }

}
