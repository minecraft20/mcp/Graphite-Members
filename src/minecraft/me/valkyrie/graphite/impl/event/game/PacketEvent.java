package me.valkyrie.graphite.impl.event.game;

import me.valkyrie.graphite.api.event.Event;
import me.valkyrie.graphite.api.event.Type;
import net.minecraft.network.Packet;

/**
 * Created by Zeb on 4/21/2017.
 */
public class PacketEvent extends Event {

    /**
     * The packet of the event.
     */
    private Packet packet;

    /**
     * The type of the event.
     */
    private Type eventType;

    /**
     * The direction of the packet.
     */
    private Direction direction;

    public PacketEvent(Packet packet, Type eventType, Direction direction){
        this.packet = packet;
        this.eventType = eventType;
        this.direction = direction;
    }

    /**
     * @return the packet which the event belongs to.
     */
    public Packet getPacket(){
        return packet;
    }

    /**
     * Swaps the packet to the new given packet.
     *
     * @param packet The new packet.
     */
    public void setPacket(Packet packet){
        this.packet = packet;
    }

    /**
     * @return the type of the event.
     */
    public Type getEventType(){
        return eventType;
    }

    /**
     * @return the direction of the packet.
     */
    public Direction getDirection(){
        return direction;
    }

}
