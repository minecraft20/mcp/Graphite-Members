package me.valkyrie.graphite.impl.event.game;

/**
 * Created by Zeb on 4/21/2017.
 */
public enum Direction {

    INCOMING,
    OUTGOING

}
