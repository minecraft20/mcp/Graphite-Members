package me.valkyrie.graphite.impl.event.game;

import me.valkyrie.graphite.api.event.Event;
import net.minecraft.client.gui.GuiScreen;

/**
 * @author Zeb.
 * @since 6/24/2017.
 */
public class GuiChangeEvent extends Event {

    private GuiScreen guiScreen;

    public GuiChangeEvent(GuiScreen guiScreen){
        this.guiScreen = guiScreen;
    }

    public GuiScreen getGuiScreen(){
        return guiScreen;
    }

    public void setGuiScreen(GuiScreen guiScreen){
        this.guiScreen = guiScreen;
    }
}
