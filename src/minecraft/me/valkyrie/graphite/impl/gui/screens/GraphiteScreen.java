package me.valkyrie.graphite.impl.gui.screens;

import me.valkyrie.graphite.api.gui.GameScreen;
import me.valkyrie.graphite.api.helper.IHelper;
import me.valkyrie.graphite.api.helper.RenderHelper;
import me.valkyrie.graphite.api.util.asset.Asset;
import me.valkyrie.graphite.api.util.render.font.TTFFontRenderer;
import me.valkyrie.graphite.api.util.render.shader.Shader;
import me.valkyrie.graphite.impl.Graphite;
import me.valkyrie.graphite.impl.gui.screens.containers.MainMenuContainer;
import me.valkyrie.graphite.impl.gui.screens.elements.CircleButton;
import me.valkyrie.graphite.impl.gui.screens.elements.TextureElement;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiSelectWorld;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.util.MouseHelper;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.util.stream.IntStream;;

import static org.lwjgl.opengl.GL11.*;

/**
 * @author Zeb.
 * @since 6/24/2017.
 */
public class GraphiteScreen extends GameScreen implements IHelper {

    private static TTFFontRenderer fontRenderer;

    /**
     * The background texture.
     */
    private TextureElement background = new TextureElement(0,0,0,0,new Asset("/textures/background.png"));

    public GraphiteScreen(){
        fontRenderer = new TTFFontRenderer(new Font("Have Heart One", Font.PLAIN, 172));

        this.add(new MainMenuContainer());
    }

    /**
     * Called when the elements need to be organized.
     */
    @Override
    public void organize(){
        // The screen resolution.
        ScaledResolution scaledResolution = new ScaledResolution(minecraft, minecraft.displayWidth, minecraft.displayHeight);

        getElements().forEach(element -> {
            element.setX(0);
            element.setY(0);
            element.resize(scaledResolution.getScaledWidth(), scaledResolution.getScaledHeight());
        });
    }

    /**
     * Draws the element.
     */
    @Override
    public void draw(){

        // The screen resolution.
        ScaledResolution scaledResolution = new ScaledResolution(minecraft, minecraft.displayWidth, minecraft.displayHeight);

        // Resizes the background.
        background.resize(scaledResolution.getScaledWidth(), scaledResolution.getScaledHeight());

        // Draws the background.
        background.draw();

        organizeChildren();
        drawChildren();

        float x = scaledResolution.getScaledWidth() / 2f;

        float y = scaledResolution.getScaledHeight() / 5f;

        fontRenderer.drawString("Graphite", (x - fontRenderer.getWidth("Graphite")/2) + 1,
                (y  - fontRenderer.getHeight("Graphite")/2) + 2, 0x64000000);
        fontRenderer.drawString("Graphite", x - fontRenderer.getWidth("Graphite")/2,
                y  - fontRenderer.getHeight("Graphite")/2, 0xFF444444);
    }

}
