package me.valkyrie.graphite.impl.gui.screens.elements;

import me.valkyrie.graphite.api.gui.Element;
import me.valkyrie.graphite.api.helper.RenderHelper;
import me.valkyrie.graphite.api.util.asset.Asset;
import me.valkyrie.graphite.api.util.game.GameTimer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.util.MouseHelper;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

import static org.lwjgl.opengl.GL11.*;

/**
 * @author Zeb.
 * @since 6/12/2017.
 */
public class CircleButton extends Element {

    /**
     * The texture to be drawn.
     */
    private Asset textureAsset;

    /**
     * The id of the texture.
     */
    private int textureId;

    /**
     * The radius of the button.
     */
    private float radius = 0;

    private double motion = 0;

    /**
     * Timer used for animations.
     */
    private GameTimer animationTimer = new GameTimer();

    /**
     * The previous hovered state.
     */
    private boolean prevHovered = false;

    /**
     * Creates an element at the given position with the given radius.
     *
     * @param x The element's x position.
     * @param y The element's y position.
     */
    public CircleButton(float x, float y, float radius){
        super(x, y);
        this.radius = radius;
    }

    /**
     * Creates an element at the given position with the given radius.
     *
     * @param x The element's x position.
     * @param y The element's y position.
     */
    public CircleButton(float x, float y, float radius, Asset asset){
        super(x, y);
        this.radius = radius;
        this.textureAsset = asset;

        setupTexture();
    }

    /**
     * Creates the texture.
     */
    private void setupTexture(){
        try{
            // Generates a new texture id.
            textureId = GlStateManager.generateTexture();

            // Gets the asset as an image.
            BufferedImage bufferedimage = TextureUtil.readBufferedImage(textureAsset.asInputStream());

            // Allocates the texture in opengl.
            TextureUtil.uploadTextureImageAllocate(textureId, bufferedimage, false, false);
        }catch(IOException exception){
            textureId = -1;
        }
    }

    /**
     * Draws the element.
     */
    @Override
    public void draw(){
        glTranslated(x, y, 0);

        RenderHelper.drawCircle(0,0,(float) (radius + Math.max(motion/3, 0)), new Color(230,230,230));

        glPushMatrix();

        // Enables textures.
        glEnable(GL_TEXTURE_2D);
        GlStateManager.disableDepth();
        GlStateManager.depthMask(false);
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.disableAlpha();

        // Binds the texture.
        GlStateManager.bindTexture(textureId);

        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

        // Begins drawing the quad.
        glBegin(GL_QUADS);
        {
            // Maps out where the texture should be drawn.
            glTexCoord2f(0, 0);
            glVertex2d(-32, -32);
            glTexCoord2f(0, 1);
            glVertex2d(-32, 32);
            glTexCoord2f(1, 1);
            glVertex2d(32, 32);
            glTexCoord2f(1, 0);
            glVertex2d(32, -32);
        }
        // Ends the quad.
        glEnd();
        glPopMatrix();


        glTranslated(-x, -y, 0);
    }

    /**
     * Draws in dark to be shadowed.
     */
    @Override
    public void drawShadowed(){
        if(isMouseOver(MouseHelper.deltaX, MouseHelper.deltaY) && !prevHovered){
            motion = Math.round(radius/5);
            Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.createPositionedSoundRecord(
                    new ResourceLocation("gui.button.press"), 2F));
        }

        prevHovered = isMouseOver(MouseHelper.deltaX, MouseHelper.deltaY);

        float deltaTime = animationTimer.getTimePassed();
        animationTimer.reset();

        motion *= 34 / deltaTime;
        motion -= 0.05 * deltaTime;

        float currentY = y;

        y -= motion;

        y = Math.min(y, currentY);

        glTranslated(x, y, 0);
        RenderHelper.drawCircle(0, 0, (float) (radius + Math.max(motion/3, 0)), new Color(0,0,0,100));
        glTranslated(-x, -y, 0);
    }

    /**
     * Checks if the given position is over the element.
     *
     * @param x The mouse's x position.
     * @param y The mouse's y position.
     * @return if the mouse is over the button.
     */
    @Override
    public boolean isMouseOver(float x, float y){
        float dx = this.x - x;
        float dy = this.y - y;

        return Math.hypot(dx, dy) <= radius;
    }
}
