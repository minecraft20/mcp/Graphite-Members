package me.valkyrie.graphite.impl.gui.screens.elements;

import me.valkyrie.graphite.api.gui.Element;
import me.valkyrie.graphite.api.util.asset.Asset;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.texture.TextureUtil;
import org.lwjgl.opengl.GL11;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created by Zeb on 12/9/2016.
 */
public class TextureElement extends Element {

    /**
     * The texture of the element.
     */
    private Asset texture;

    /**
     * The id of the texture.
     */
    private int textureId;

    public TextureElement(float x, float y, float width, float height, Asset texture){
        super(x, y, width, height);
        this.texture = texture;
        setupTexture();
    }

    /**
     * Creates the texture.
     */
    private void setupTexture(){
        try{
            // Generates a new texture id.
            textureId = GlStateManager.generateTexture();

            // Gets the asset as an image.
            BufferedImage bufferedimage = TextureUtil.readBufferedImage(texture.asInputStream());

            // Allocates the texture in opengl.
            TextureUtil.uploadTextureImageAllocate(textureId, bufferedimage, false, false);
        }catch(IOException exception){
            textureId = -1;
        }
    }

    /**
     * Binds the texture.
     */
    private void bindTexture(){
        // Binds the opengl texture by the texture id.
        GlStateManager.bindTexture(textureId);
    }

    @Override
    public void draw(){
        // Enables textures.
        GL11.glEnable(GL11.GL_TEXTURE_2D);

        // Binds the texture.
        bindTexture();

        GlStateManager.disableDepth();
        GlStateManager.depthMask(false);
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.disableAlpha();

        // Begins drawing the quad.
        GL11.glBegin(GL11.GL_QUADS);
        {
            // Maps out where the texture should be drawn.
            GL11.glTexCoord2f(0, 0);
            GL11.glVertex2d(x, y);
            GL11.glTexCoord2f(0, 1);
            GL11.glVertex2d(x, y + height);
            GL11.glTexCoord2f(1, 1);
            GL11.glVertex2d(x + width, y + height);
            GL11.glTexCoord2f(1, 0);
            GL11.glVertex2d(x + width, y);
        }
        // Ends the quad.
        GL11.glEnd();
    }

}
