package me.valkyrie.graphite.impl.gui.screens.containers;

import me.valkyrie.graphite.api.gui.Container;
import me.valkyrie.graphite.api.helper.IHelper;
import me.valkyrie.graphite.api.util.asset.Asset;
import me.valkyrie.graphite.api.util.render.shader.Shader;
import me.valkyrie.graphite.impl.Graphite;
import me.valkyrie.graphite.impl.gui.screens.elements.CircleButton;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiSelectWorld;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.shader.Framebuffer;
import org.lwjgl.util.vector.Vector2f;

import java.util.stream.IntStream;

import static org.lwjgl.opengl.GL11.glTranslated;

/**
 * @author Zeb.
 * @since 6/26/2017.
 */
public class MainMenuContainer extends Container implements IHelper {

    /**
     * The blur shader that makes shadows.
     */
    private Shader blurShader;

    /**
     * The buffer that handles shadows.
     */
    private Framebuffer shadowBuffer;

    public MainMenuContainer(){
        String[] assetPath = {"Home-128.png", "Globe-128.png", "Account-128.png", "Gear-128.png", "Power-128.png"};

        IntStream.range(0, 5).forEach(i -> add(new CircleButton(0,0,35, new Asset("/textures/" + assetPath[4-i]))));
    }

    /**
     * Initiates the buffers.
     */
    private void setup(){
        // Creates the buffer if wrong size or null.
        if(shadowBuffer == null
                || shadowBuffer.framebufferWidth != minecraft.getFramebuffer().framebufferWidth
                || shadowBuffer.framebufferHeight != minecraft.getFramebuffer().framebufferHeight)
            shadowBuffer = new Framebuffer(minecraft.getFramebuffer().framebufferWidth, minecraft.getFramebuffer().framebufferHeight, true);

        // Creates the shader if null.
        if(blurShader == null){
            blurShader = new Shader(new Asset("/shader/blur/blur.vert"), new Asset("/shader/blur/blur.frag"));
        }
    }

    /**
     * Called when the elements need to be organized.
     */
    @Override
    public void organize(){

        // The screen resolution.
        ScaledResolution scaledResolution = new ScaledResolution(minecraft, minecraft.displayWidth, minecraft.displayHeight);

        float x = scaledResolution.getScaledWidth() / 2f;

        float y = scaledResolution.getScaledHeight() / 1.5f;

        // Aligns all of the elements.
        getElements().forEach(element -> {
            int index = 2 - getElements().indexOf(element);

            element.setX(x + (index * 100));
            element.setY(y);
        });
    }

    /**
     * Draws the element.
     */
    @Override
    public void draw(){
        setup();

        // The screen resolution.
        ScaledResolution scaledResolution = new ScaledResolution(minecraft, minecraft.displayWidth, minecraft.displayHeight);

        // Clears the buffer/
        shadowBuffer.framebufferClear();

        // Binds the blur buffer.
        shadowBuffer.bindFramebuffer(false);

        glTranslated(0,1,0);
        drawChildrenShadowed();
        glTranslated(0,-1,0);

        // Binds the minecraft buffer.
        minecraft.getFramebuffer().bindFramebuffer(true);

        // Resets view.
        Minecraft.getMinecraft().entityRenderer.setupOverlayRendering();

        GlStateManager.enableAlpha();

        // Binds the blur shader.
        blurShader.bind();

        // Uploads the sampler as the buffer texture.
        blurShader.setSampler2d("DiffuseSamper", shadowBuffer.framebufferTexture);

        // Uploads the texel size.
        blurShader.setVector("TexelSize",
                new Vector2f(1f / Minecraft.getMinecraft().getFramebuffer().framebufferWidth, 1f / Minecraft.getMinecraft().getFramebuffer().framebufferHeight));

        // Sets the radius.
        blurShader.setInteger("radius", 4);

        GlStateManager.enableBlend();

        // Renders the framebuffer.
        shadowBuffer.framebufferRender();

        // Resets the view.
        Minecraft.getMinecraft().entityRenderer.setupOverlayRendering();

        // Unbinds the shader.
        blurShader.unbind();

        // Binds the minecraft buffer.
        minecraft.getFramebuffer().bindFramebuffer(true);

        // Resets view.
        Minecraft.getMinecraft().entityRenderer.setupOverlayRendering();

        minecraft.getFramebuffer().bindFramebuffer(false);

        // Switch to 2d camera.
        minecraft.entityRenderer.setupOverlayRendering();

        drawChildren();
    }

    @Override
    public void mouseClick(float x, float y, int button){
        getElements()
                .stream()
                .filter(element -> element.isMouseOver(x,y))
                .forEach(element -> {

                    int index = 4-getElements().indexOf(element);

                    switch(index){
                        case 0:
                            minecraft.displayGuiScreen(new GuiSelectWorld(Graphite.INSTANCE.getMainMenu()));
                            break;
                        case 1:
                            minecraft.displayGuiScreen(new GuiMultiplayer(Graphite.INSTANCE.getMainMenu()));
                            break;
                        case 2:
                            break;
                        case 3:
                            minecraft.displayGuiScreen(new GuiOptions(Graphite.INSTANCE.getMainMenu(), minecraft.gameSettings));
                            break;
                        case 4:
                            minecraft.shutdown();
                            break;
                    }

                });
    }
}
