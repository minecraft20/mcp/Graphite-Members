package me.valkyrie.graphite.impl.gui.tabgui.items.base;

import me.valkyrie.graphite.impl.gui.tabgui.Tabgui;
import me.valkyrie.graphite.impl.gui.tabgui.items.Panel;
import org.lwjgl.input.Keyboard;

/**
 * @author Zeb.
 * @since 5/15/2017
 */
public abstract class AbstractExpandingTabItem extends AbstractTabItem {

    /**
     * Handles opening of the item.
     *
     * @param key The key pressed.
     */
    @Override
    public void keyPress(int key){
        if(key == Keyboard.KEY_RIGHT)
            Tabgui.INSTANCE.add(open());
    }

    /**
     * Called when the panel is opened.
     *
     * @return the new panel formed on opening of the item.
     */
    public abstract Panel open();

}
