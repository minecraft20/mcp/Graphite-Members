package me.valkyrie.graphite.impl.gui.tabgui.items.impl.property;

import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.util.property.types.BooleanPropertyValue;
import me.valkyrie.graphite.impl.gui.tabgui.items.Panel;
import me.valkyrie.graphite.impl.gui.tabgui.items.base.AbstractPropertyValueTabItem;
import org.lwjgl.input.Keyboard;

/**
 * @author Zeb.
 * @since 5/16/2017
 */
public class BooleanPropertyValueTabItem extends AbstractPropertyValueTabItem<BooleanPropertyValue> {

    public BooleanPropertyValueTabItem(Module module, BooleanPropertyValue property){
        super(module, property);
    }

    @Override
    public String getText(){
        return String.format("\247%s%s", property.getValue() ? 'f' : '7', property.getLabel().replaceAll("_", " "));
    }

    /**
     * Handles opening of the item.
     *
     * @param key The key pressed.
     */
    @Override
    public void keyPress(int key){
        if(key == Keyboard.KEY_RETURN){
            property.setValue(!property.getValue());

            // Saves the module.
            module.save();
        }

        super.keyPress(key);
    }

}
