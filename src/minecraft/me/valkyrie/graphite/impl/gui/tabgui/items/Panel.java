package me.valkyrie.graphite.impl.gui.tabgui.items;

import me.valkyrie.graphite.api.helper.DepthHelper;
import me.valkyrie.graphite.api.helper.RenderHelper;
import me.valkyrie.graphite.api.util.game.GameTimer;
import me.valkyrie.graphite.impl.gui.tabgui.Tabgui;
import me.valkyrie.graphite.impl.gui.tabgui.items.base.AbstractTabItem;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * @author Zeb.
 * @since 5/15/2017
 */
public final class Panel {

    /**
     * The selection index of the tab panel.
     */
    private int selectionIndex;

    /**
     * The current y position of the selector.
     */
    private double selectorPosition = 0;

    /**
     * All of the items inside of the panel.
     */
    private List<AbstractTabItem> tabItemSet = new ArrayList<>();

    /**
     * Timer used to handle selection.
     */
    private GameTimer timer = new GameTimer();

    /**
     * The time since opening the tab folder.
     */
    private long time;

    public Panel(){
        // Resets the opening time.
        this.time = System.currentTimeMillis();
    }

    /**
     * Handles key presses.
     *
     * @param key The key pressed.
     */
    public void keyPress(int key){

        // Handles upwards selection.
        if((key == Keyboard.KEY_UP || key == Keyboard.KEY_DOWN) && !getSelectedPart().isFocused())
            selectionIndex -= key == Keyboard.KEY_UP ? 1 : -1;
        else
            getSelectedPart().keyPress(key);

        // Handles key overflow.
        selectionIndex %= tabItemSet.size();

        if(selectionIndex < 0)
            selectionIndex = tabItemSet.size() - 1;
    }

    /**
     * Draws the panel.
     */
    public void draw(){
        // Begins masking.
        DepthHelper.INSTANCE.begin();
        DepthHelper.INSTANCE.beginMaskRender();

        float percent = (float) Math.pow(((System.currentTimeMillis() - time) / 10D), 1.4) / Tabgui.design().animationTime;

        percent = Math.min(1, percent);

        float border = Tabgui.design().panelBorder;

        // Renders the effect.
        if(Tabgui.design().openingAnimation)
            RenderHelper.drawCircle(0, (getHeight()) / 2f,
                    percent * (float) Math.hypot(getWidth(), (getHeight())), Color.WHITE);
        else
            // Draws the background.
            RenderHelper.drawRect(-border, -border, getWidth() + border, getHeight() + border, Tabgui.design().backgroundColor);

        // Begins the normal render.
        DepthHelper.INSTANCE.beginNormalRender();

        // Draws the background.
        RenderHelper.drawRect(-border, -border, getWidth() + border, getHeight() + border, Tabgui.design().backgroundColor);

        // Where the selector needs to go.
        double destination = selectionIndex * Tabgui.design().textHeight;

        // The difference of percentage.
        double difference = destination - selectorPosition;

        // Multiplies the difference by the time passed.
        difference *= timer.getTimePassed();
        timer.reset();

        // Adds the difference divided by the delay.
        this.selectorPosition += difference / (Tabgui.design().scrollSpeed * 10f);

        // Draws the background.
        RenderHelper.drawRect(0, selectorPosition, getWidth(), selectorPosition + Tabgui.design().textHeight, Tabgui.design().foregroundColor);

        // Draws all of the children.
        for(int i = 0; i < tabItemSet.size(); i++){
            AbstractTabItem tabItem = tabItemSet.get(i);

            // Translates into position.
            GlStateManager.translate(Tabgui.design().fontPadding, i * Tabgui.design().textHeight, 0);

            // Draws the item.
            tabItem.draw();

            // Translates into position.
            GlStateManager.translate(-Tabgui.design().fontPadding, -i * Tabgui.design().textHeight, 0);
        }

        // Ends the depth masking.
        DepthHelper.INSTANCE.end();
    }

    /**
     * @return the width of the panel.
     */
    public float getWidth(){
        float width = Tabgui.design().panelWidth;

        for(AbstractTabItem tabItem : tabItemSet){
            float w = Tabgui.fontRenderer.getWidth(tabItem.getText()) + Tabgui.design().fontPadding + 6;

            if(w > width)
                width = w;
        }

        return width;
    }

    /**
     * @return the height of the main panel.
     */
    public float getHeight(){
        return tabItemSet.size() * Tabgui.design().textHeight;
    }

    /**
     * @return the {@link AbstractTabItem} currently selected.
     */
    private AbstractTabItem getSelectedPart(){
        return this.tabItemSet.get(selectionIndex);
    }

    /**
     * @return the items of the panel.
     */
    public List<AbstractTabItem> getTabItemSet(){
        return tabItemSet;
    }

    /**
     * Adds the given {@link AbstractTabItem} into the {@link Panel}.
     *
     * @param item The item to be added.
     */
    public void add(AbstractTabItem item){
        item.setParent(this);

        this.tabItemSet.add(item);
    }

}
