package me.valkyrie.graphite.impl.gui.tabgui.items.base;

import me.valkyrie.graphite.impl.gui.tabgui.Tabgui;
import me.valkyrie.graphite.impl.gui.tabgui.items.Panel;
import net.minecraft.client.Minecraft;

/**
 * @author Zeb.
 * @since 5/15/2017
 */
public abstract class AbstractTabItem {

    /**
     * The {@link Panel} the item belongs to.
     */
    protected Panel container;

    /**
     * The y position of the element.
     */
    private int y;

    /**
     * If the element is focused to be changed.
     */
    protected boolean focused;

    /**
     * @return the text of the tab item.
     */
    public abstract String getText();

    /**
     * Handles key presses.
     *
     * @param key The key pressed.
     */
    public abstract void keyPress(int key);

    /**
     * Sets the parent of the item.
     *
     * @param container The items parent.
     */
    public void setParent(Panel container){
        this.container = container;
    }

    /**
     * @return if the element is focused.
     */
    public boolean isFocused(){
        return focused;
    }

    /**
     * Draws the item.
     */
    public void draw(){
        // The height of the text.
        float height = Tabgui.fontRenderer.getHeight(getText().toLowerCase());

        // Renders the text.
        Tabgui.fontRenderer.drawStringWithShadow(getText().toLowerCase(), 0, (Tabgui.design().textHeight - height) / 2f + 0.5f, 0xFFFFFFFF);
    }

}
