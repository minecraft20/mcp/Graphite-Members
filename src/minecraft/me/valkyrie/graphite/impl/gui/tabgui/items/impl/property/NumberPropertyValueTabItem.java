package me.valkyrie.graphite.impl.gui.tabgui.items.impl.property;

import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import me.valkyrie.graphite.api.util.property.types.NumberPropertyValue;
import me.valkyrie.graphite.impl.gui.tabgui.items.Panel;
import me.valkyrie.graphite.impl.gui.tabgui.items.base.AbstractPropertyValueTabItem;
import org.lwjgl.input.Keyboard;

/**
 * @author Zeb.
 * @since 5/16/2017
 */
public class NumberPropertyValueTabItem extends AbstractPropertyValueTabItem<NumberPropertyValue> {

    public NumberPropertyValueTabItem(Module module, NumberPropertyValue property){
        super(module, property);
    }

    @Override
    public void keyPress(int key){
        if(key == Keyboard.KEY_RETURN)
            focused = !focused;

        // Checks key and focus state to handle value manipulation.
        if((key == Keyboard.KEY_UP || key == Keyboard.KEY_DOWN) && focused){

            // Handles value changing.
            if(key == Keyboard.KEY_UP)
                property.increase();
            else
                property.decrease();

            // Saves the module.
            module.save();
        }
    }

}
