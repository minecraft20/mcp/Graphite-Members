package me.valkyrie.graphite.impl.gui.tabgui.items.base;

/**
 * @author Zeb.
 * @since 5/15/2017
 */
public abstract class AbstractToggleableExpandingTabItem extends AbstractExpandingTabItem {

    /**
     * The state of the item.
     */
    private boolean state;

    /**
     * Toggles the item.
     */
    public void toggle(){
        setState(!getState());
    }

    /**
     * @return the item's state.
     */
    public boolean getState(){
        return state;
    }

    /**
     * Sets the state of the {@link AbstractToggleableExpandingTabItem}.
     *
     * @param state The state of the item.
     */
    public void setState(boolean state){
        this.state = state;
    }

}
