package me.valkyrie.graphite.impl.gui.tabgui.items.impl.property;

import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.util.property.types.EnumPropertyValue;
import me.valkyrie.graphite.impl.gui.tabgui.items.base.AbstractPropertyValueTabItem;
import org.lwjgl.input.Keyboard;

/**
 * @author Zeb.
 * @since 5/16/2017
 */
public class EnumPropertyValueTabItem extends AbstractPropertyValueTabItem<EnumPropertyValue> {

    public EnumPropertyValueTabItem(Module module, EnumPropertyValue property){
        super(module, property);
    }

    /**
     * Handles opening of the item.
     *
     * @param key The key pressed.
     */
    @Override
    public void keyPress(int key){

        // Handles value changing
        if(key == Keyboard.KEY_RETURN){

            // The enum of the property.
            Enum e = property.getValue();

            try{

                // Handles ordinal incrementing.
                int ordinal = e.ordinal() + 1;

                ordinal %= property.values().length;

                // Sets the value.
                property.setValue(property.values()[ordinal]);
            }catch(Exception exception){}

            // Saves the module.
            module.save();
        }

        super.keyPress(key);
    }
}
