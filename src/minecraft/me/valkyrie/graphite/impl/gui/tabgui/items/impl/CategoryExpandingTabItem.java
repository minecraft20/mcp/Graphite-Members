package me.valkyrie.graphite.impl.gui.tabgui.items.impl;

import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.impl.Graphite;
import me.valkyrie.graphite.impl.gui.tabgui.items.base.AbstractExpandingTabItem;
import me.valkyrie.graphite.impl.gui.tabgui.items.Panel;

/**
 * @author Zeb.
 * @since 5/15/2017
 */
public class CategoryExpandingTabItem extends AbstractExpandingTabItem {

    /**
     * The {@link Category} that the item belongs to.
     */
    private Category category;

    public CategoryExpandingTabItem(Category category){
        this.category = category;
    }

    /**
     * @return the text of the tab item.
     */
    @Override
    public String getText(){
        return category.toString().substring(0, 1).toUpperCase() + category.toString().substring(1);
    }

    /**
     * Called when the panel is opened.
     *
     * @return the new panel formed on opening of the item.
     */
    @Override
    public Panel open(){
        Panel panel = new Panel();

        Graphite.INSTANCE.getModuleManager().getMap().values().stream()
                .filter(module -> module.getType() == category).forEach(module -> panel.add(new ModuleExpandingTabItem(module)));

        return panel;
    }

}
