package me.valkyrie.graphite.impl.gui.tabgui.items.base;

import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.util.property.PropertyValue;
import me.valkyrie.graphite.api.util.property.types.BooleanPropertyValue;
import me.valkyrie.graphite.api.util.property.types.EnumPropertyValue;
import me.valkyrie.graphite.api.util.property.types.NumberPropertyValue;
import me.valkyrie.graphite.impl.gui.tabgui.items.Panel;
import me.valkyrie.graphite.impl.gui.tabgui.items.impl.property.BooleanPropertyValueTabItem;
import me.valkyrie.graphite.impl.gui.tabgui.items.impl.property.EnumPropertyValueTabItem;
import me.valkyrie.graphite.impl.gui.tabgui.items.impl.property.NumberPropertyValueTabItem;

/**
 * @author Zeb.
 * @since 5/16/2017
 */
public abstract class AbstractPropertyValueTabItem<T extends PropertyValue> extends AbstractExpandingTabItem {

    /**
     * The {@link Module} the property belongs to.
     */
    protected Module module;

    /**
     * The {@link PropertyValue} the tab item controls.
     */
    protected T property;

    public AbstractPropertyValueTabItem(Module module, T property){
        this.module = module;
        this.property = property;
    }

    /**
     * Called when the panel is opened.
     *
     * @return the new panel formed on opening of the item.
     */
    @Override
    public Panel open(){
        if(property.getChildren().isEmpty())
            return null;

        Panel panel = new Panel();

        property.getChildren().forEach(child ->
                panel.add(of((PropertyValue) child, module)));

        return panel;
    }

    /**
     * @return the text of the tab item.
     */
    @Override
    public String getText(){
        return String.format("%s: \2477%s", property.getLabel().replaceAll("_", " "), property.getValue());
    }

    /**
     * Factory method to create all of the property tab items.
     *
     * @param propertyValue The property to create the tab item of.
     * @param module        The {@link Module} that the property belongs to.
     */
    public static AbstractPropertyValueTabItem of(PropertyValue propertyValue, Module module){
        if(propertyValue instanceof NumberPropertyValue)
            return new NumberPropertyValueTabItem(module, (NumberPropertyValue) propertyValue);
        else if(propertyValue instanceof EnumPropertyValue)
            return new EnumPropertyValueTabItem(module, (EnumPropertyValue) propertyValue);
        else if(propertyValue instanceof BooleanPropertyValue)
            return new BooleanPropertyValueTabItem(module, (BooleanPropertyValue) propertyValue);

        return null;
    }

}
