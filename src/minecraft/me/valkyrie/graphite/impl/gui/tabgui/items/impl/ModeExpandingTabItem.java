package me.valkyrie.graphite.impl.gui.tabgui.items.impl;

import me.valkyrie.graphite.api.helper.RenderHelper;
import me.valkyrie.graphite.api.module.Mode;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.util.property.PropertyValue;
import me.valkyrie.graphite.impl.gui.tabgui.Tabgui;
import me.valkyrie.graphite.impl.gui.tabgui.items.Panel;
import me.valkyrie.graphite.impl.gui.tabgui.items.base.AbstractPropertyValueTabItem;
import me.valkyrie.graphite.impl.gui.tabgui.items.base.AbstractToggleableExpandingTabItem;
import org.lwjgl.input.Keyboard;

import java.util.Arrays;

/**
 * @author Zeb.
 * @since 5/31/2017.
 */
public class ModeExpandingTabItem extends AbstractToggleableExpandingTabItem {

    /**
     * The {@link Mode} that the item controls.
     */
    private Mode mode;

    /**
     * The {@link Module} the mode belongs to.
     */
    private Module module;

    public ModeExpandingTabItem(Mode mode, Module module){
        this.mode = mode;
        this.module = module;
    }

    /**
     * Handles mode setting
     *
     * @param key The key pressed.
     */
    @Override
    public void keyPress(int key){
        if(key == Keyboard.KEY_RETURN)
            module.setMode(mode);

        super.keyPress(key);
    }

    /**
     * @return the item's state.
     */
    @Override
    public boolean getState(){
        return module.getMode() == mode;
    }

    /**
     * @return the text of the tab item.
     */
    @Override
    public String getText(){
        return String.format("\247%s%s", getState() ? "f" : "7", mode.getLabel().replaceAll("_", " "));
    }

    /**
     * Called when the panel is opened.
     *
     * @return the new panel formed on opening of the item.
     */
    @Override
    public Panel open(){
        // The panel where the module shit goes.
        Panel panel = new Panel();

        mode.getProperties().forEach(propertyValue ->
                panel.add(AbstractPropertyValueTabItem.of((PropertyValue) propertyValue, module)));

        return panel.getTabItemSet().isEmpty() ? null : panel;
    }

    /**
     * Draws the item.
     */
    @Override
    public void draw(){
        // Draws hints if enabled.
        if(Tabgui.design().itemClues && getState())
            RenderHelper.drawRect(-Tabgui.design().fontPadding, 0, 1 - Tabgui.design().fontPadding,
                    Tabgui.design().textHeight, Tabgui.design().foregroundColor);

        super.draw();
    }

}
