package me.valkyrie.graphite.impl.gui.tabgui.items.impl;

import me.valkyrie.graphite.api.helper.RenderHelper;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.impl.gui.tabgui.Tabgui;
import me.valkyrie.graphite.impl.gui.tabgui.items.*;
import me.valkyrie.graphite.impl.gui.tabgui.items.base.AbstractPropertyValueTabItem;
import me.valkyrie.graphite.impl.gui.tabgui.items.base.AbstractToggleableExpandingTabItem;
import org.lwjgl.input.Keyboard;

import java.util.Arrays;

/**
 * @author Zeb.
 * @since 5/15/2017
 */
public class ModuleExpandingTabItem extends AbstractToggleableExpandingTabItem {

    /**
     * The {@link Module} the tab items controls.
     */
    private Module module;

    public ModuleExpandingTabItem(Module module){
        this.module = module;
    }

    /**
     * @return the text of the tab item.
     */
    @Override
    public String getText(){
        return String.format("\247%s%s", getState() ? "f" : "7", module.getLabel().replaceAll("_", " "));
    }

    /**
     * Handles module toggling.
     *
     * @param key The key pressed.
     */
    @Override
    public void keyPress(int key){
        if(key == Keyboard.KEY_RETURN)
            module.toggle();

        super.keyPress(key);
    }

    /**
     * @return the item's state.
     */
    @Override
    public boolean getState(){
        return module.isRunning();
    }

    /**
     * Called when the panel is opened.
     *
     * @return the new panel formed on opening of the item.
     */
    @Override
    public Panel open(){
        // The panel where the module shit goes.
        Panel panel = new Panel();

        Arrays.stream(module.getModes()).forEach(mode -> panel.add(new ModeExpandingTabItem(mode, module)));

        module.getProperties().forEach(propertyValue -> panel.add(AbstractPropertyValueTabItem.of(propertyValue, module)));

        return panel.getTabItemSet().isEmpty() ? null : panel;
    }

    /**
     * Draws the item.
     */
    @Override
    public void draw(){
        // Draws hints if enabled.
        if(Tabgui.design().itemClues && getState())
            RenderHelper.drawRect(-Tabgui.design().fontPadding, 0, 1 - Tabgui.design().fontPadding,
                    Tabgui.design().textHeight, Tabgui.design().foregroundColor);

        super.draw();
    }
}
