package me.valkyrie.graphite.impl.gui.tabgui;

import com.google.gson.*;
import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.helper.RenderHelper;
import me.valkyrie.graphite.api.module.Category;
import me.valkyrie.graphite.api.util.Friend;
import me.valkyrie.graphite.api.util.Jsonable;
import me.valkyrie.graphite.api.util.render.font.IFontRenderingStrategy;

import me.valkyrie.graphite.api.util.render.font.MinecraftFontRenderer;
import me.valkyrie.graphite.api.util.render.font.TTFFontRenderer;
import me.valkyrie.graphite.impl.event.game.KeyPressEvent;
import me.valkyrie.graphite.impl.gui.tabgui.items.*;
import me.valkyrie.graphite.impl.gui.tabgui.items.Panel;
import me.valkyrie.graphite.impl.gui.tabgui.items.impl.CategoryExpandingTabItem;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * @author Zeb.
 * @since 5/15/2017
 */
public enum Tabgui implements EventHandler<KeyPressEvent> {

    /**
     * The instance of the tabgui, enum to ensure singleton status.
     */
    INSTANCE;

    /**
     * An instance of {@link Gson} to help with pretty printing of the json to the design files.
     */
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    /**
     * The font-rendering strategy the tabgui is currently using,
     */
    public static IFontRenderingStrategy fontRenderer;

    /**
     * The design of the tabgui.
     */
    private static Design design = new Design();

    /**
     * The file where the design properties are saved.
     */
    private File file;

    /**
     * A {@link Stack} of all of the panel in the tabgui.
     */
    private Stack<Panel> panels = new Stack<>();

    /**
     * The main panel.
     */
    private Panel mainPanel = new Panel();

    /**
     * @return the tabgui's design.
     */
    public static Design design(){
        return design;
    }

    /**
     * Initializes the tabgui.
     */
    public void init(File directory){
        // Sets the file for design properties.
        this.file = new File(directory + File.separator + "design.json");

        // Sets up the tab gui.
        setup();

        // Adds all of the category tabs.
        Arrays.stream(Category.values()).forEach(category -> mainPanel.add(new CategoryExpandingTabItem(category)));

        panels.add(mainPanel);

        EventManager.INSTANCE.register(this);
    }

    /**
     * Draws all of the panels.
     */
    public void draw(){

        GlStateManager.pushMatrix();

        // Iterator for all of the panels.
        Iterator<Panel> panelIterator = panels.iterator();

        // The current panel index.
        float x = 0;

        // Starts drawing all of the panels.
        while(panelIterator.hasNext()){

            Panel panel = panelIterator.next();

            // Translates into position.
            GlStateManager.translate(2 + x, 17, 0);

            // Draws the panels.
            panel.draw();

            // Translates out of position.
            GlStateManager.translate(-(2 + x), -17, 0);

            x += panel.getWidth() + design().panelSeparation;
        }

        GlStateManager.popMatrix();
    }

    /**
     * @return the main panel.
     */
    public Panel getMainPanel(){
        return mainPanel;
    }

    /**
     * Sets up the friend manager.
     */
    public void setup(){
        // Tries to create the design's file.
        try{
            // Creates the design file if it doesn't exist.
            if(!file.exists()){

                // Creates the design's file.
                file.createNewFile();

                // Saves the design file.
                save();

                // Sets the font renderer.
                fontRenderer = new MinecraftFontRenderer();

                // Returns because there is no need to load.
                return;
            }

            // Loads the file.
            load();
        } catch(IOException exception){
            System.exit(0);
        }
    }

    /**
     * Saves the current design.
     */
    public void save(){
        // Tries to write data, might fail so statement is in try statement.
        try{
            // The print writer of the file that is used to write the json data.
            PrintWriter printWriter = new PrintWriter(file);

            // Prints the json into the file correctly formatted.
            printWriter.print(GSON.toJson(design.toJson()));

            // Closes the writer.
            printWriter.close();
        } catch(IOException | NullPointerException ignored){
        }
    }

    /**
     * Loads the design.
     */
    public void load(){
        // Tries to read data, might fail so statement is in try statement.
        try{
            // Parses the json read from the file.
            JsonObject jsonObject = new JsonParser().parse(new FileReader(file)).getAsJsonObject();

            design.fromJson(jsonObject);

            if(design.font.equalsIgnoreCase("default") || design.font.equalsIgnoreCase("minecraft"))
                fontRenderer = new MinecraftFontRenderer();
            else
                fontRenderer = new TTFFontRenderer(new Font(design.font, 0, design.fontSize));

        } catch(IOException ignored){
        }
    }

    /**
     * Adds the {@link Panel} to the queue of panel.
     *
     * @param panel The panel to be added.
     */
    public void add(Panel panel){
        if(panel == null)
            return;

        this.panels.add(panel);
    }

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return KeyPressEvent.class;
    }

    /**
     * Handles the event.
     *
     * @param event The event called.
     */
    @Override
    public void handle(KeyPressEvent event){
        if(event.getKeyPressed() == Keyboard.KEY_LEFT && panels.size() > 1)
            panels.pop();
        else
            panels.peek().keyPress(event.getKeyPressed());
    }

    /**
     * The design of the tabgui.
     */
    public static final class Design implements Jsonable {


        /**
         * The font to be used when rendering the items.
         */
        public String font = "minecraft";

        /**
         * The size of the font, does not apply to minecraft font.
         */
        public int fontSize = 18;

        /**
         * The primary color of the tabgui.
         */
        public int foregroundColor = 0xFFCFCFCF;

        /**
         * The background color of the tabgui.
         */
        public int backgroundColor = 0x64000000;

        /**
         * The width for a panel.
         */
        public int panelWidth = 60;

        /**
         * The height of text items in the panels.
         */
        public int textHeight = 16;

        /**
         * The horizontal font padding for text inside of tab items.
         */
        public float fontPadding = 5;

        /**
         * The amount of separation between panels.
         */
        public float panelSeparation = 2;

        /**
         * The border of the panel.
         */
        public float panelBorder = 0.5f;

        /**
         * The scrolling speed of the tabgui bar.
         */
        public int scrollSpeed = 6;

        /**
         * If the opening animation should play.
         */
        public boolean openingAnimation = true;

        /**
         * How long the opening animation plays.
         */
        public long animationTime = 200; /* 200 milliseconds */

        /**
         * If item clues should help determine item activity.
         */
        public boolean itemClues = true;

        /**
         * @return the object in json format.
         */
        @Override
        public JsonObject toJson(){
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("font", font);
            jsonObject.addProperty("fontSize", fontSize);
            jsonObject.addProperty("foregroundColor", foregroundColor);
            jsonObject.addProperty("backgroundColor", backgroundColor);
            jsonObject.addProperty("panelWidth", panelWidth);
            jsonObject.addProperty("textHeight", textHeight);
            jsonObject.addProperty("fontPadding", fontPadding);
            jsonObject.addProperty("panelSeparation", panelSeparation);
            jsonObject.addProperty("panelBorder", panelBorder);
            jsonObject.addProperty("scrollSpeed", scrollSpeed);
            jsonObject.addProperty("openingAnimation", openingAnimation);
            jsonObject.addProperty("animationTime", animationTime);
            jsonObject.addProperty("itemClues", itemClues);

            return jsonObject;
        }

        /**
         * Creates the object from json.
         *
         * @param json The json data.
         */
        @Override
        public void fromJson(JsonObject json){
            this.font = json.get("font").getAsString();
            this.fontSize = json.get("fontSize").getAsInt();
            this.foregroundColor = json.get("foregroundColor").getAsInt();
            this.backgroundColor = json.get("backgroundColor").getAsInt();
            this.panelWidth = json.get("panelWidth").getAsInt();
            this.textHeight = json.get("textHeight").getAsInt();
            this.fontPadding = json.get("fontPadding").getAsFloat();
            this.panelBorder = json.get("panelBorder").getAsFloat();
            this.panelSeparation = json.get("panelSeparation").getAsFloat();
            this.scrollSpeed = json.get("scrollSpeed").getAsInt();
            this.openingAnimation = json.get("openingAnimation").getAsBoolean();
            this.animationTime = json.get("animationTime").getAsLong();
            this.itemClues = json.get("itemClues").getAsBoolean();
        }

    }

}
