package me.valkyrie.graphite.impl.gui;

import me.valkyrie.graphite.api.event.EventHandler;
import me.valkyrie.graphite.api.helper.DepthHelper;
import me.valkyrie.graphite.api.helper.IHelper;
import me.valkyrie.graphite.api.helper.RenderHelper;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.util.Stencil;
import me.valkyrie.graphite.api.util.render.font.IFontRenderingStrategy;
import me.valkyrie.graphite.api.util.render.font.TTFFontRenderer;
import me.valkyrie.graphite.impl.Graphite;
import me.valkyrie.graphite.impl.event.render.RenderScreenEvent;
import me.valkyrie.graphite.impl.gui.tabgui.Tabgui;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Zeb.
 * @since 4/21/2017
 */
public class GameOverlay implements EventHandler<RenderScreenEvent>, IHelper {

    /**
     * @return the class of the event that should be handled.
     */
    @Override
    public Class type(){
        return RenderScreenEvent.class;
    }

    /**
     * Render's the client's overlay.
     */
    @Override
    public void handle(RenderScreenEvent event){

        // Returns if the debug screen is present.
        if(minecraft.gameSettings.showDebugInfo)
            return;

        // The font renderer for the hud.
        IFontRenderingStrategy fontRenderer = Tabgui.fontRenderer;

        // Draws the client watermark.
        fontRenderer.drawStringWithShadow(String.format("graphite \2477(%s)", Graphite.INSTANCE.VERSION), 2, 3,
                0xFFFFFFFF);

        // Draws the tabgui.
        Tabgui.INSTANCE.draw();

        // The y offset of the array-list.
        float y = 20 + Tabgui.INSTANCE.getMainPanel().getHeight();

        // Sorts the modules.
        List<Module> mods = Graphite.INSTANCE.getModuleManager().getMap().values().stream()
                .filter(module -> module.isRunning() && module.isVisible())
                .sorted(Comparator.comparingDouble(module -> -fontRenderer.getWidth(module.getDisplayLabel())))
                .collect(Collectors.toList());

        // Renders all of the modules to the array-list.
        for(Module module : mods){

            // Draws the module label.
            fontRenderer.drawStringWithShadow(module.getDisplayLabel().toLowerCase(), 2, y, module.getColor());

            // Increments the y offset.
            y += 11;
        }

        GlStateManager.enableAlpha();
        GlStateManager.enableBlend();
    }
}
