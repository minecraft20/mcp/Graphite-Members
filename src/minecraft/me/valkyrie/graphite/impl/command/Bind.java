package me.valkyrie.graphite.impl.command;

import org.lwjgl.input.Keyboard;

import me.valkyrie.graphite.api.command.Command;
import me.valkyrie.graphite.api.command.CommandManifest;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import me.valkyrie.graphite.impl.Graphite;

/**
 * Command used to bind module's key-binds.
 *
 * @author Camber.
 * @since 4/30/2017
 */
@CommandManifest(label = "Bind", aliases = {"B"})
public class Bind extends Command {

    @Override
    public void execute(String[] arguments, String raw){

        // Returns if arguments are invalid.
        if(arguments.length != 2){
            // Prints error.
            this.clientChatMsg().appendText("Invalid arguments, enter module and keybind.").send();

            return;
        }

        // The module specified.
        Module module = Graphite.INSTANCE.getModuleManager().get(arguments[0]);

        // Checks to see if module exists.
        if(module == null){

            // Prints module does not exist error.
            this.clientChatMsg().appendText("Invalid arguments, specified module does not exist.").send();

            // Returns.
            return;
        }

        // Sets the module bind.
        module.setBind(Keyboard.getKeyIndex(arguments[1].toUpperCase()));

        // Prints the success message.
        this.clientChatMsg()
                .appendText(module.getLabel().replaceAll("_", " ") + "'s", ChatColor.GRAY)
                .appendText(" keybind has been set to ")
                .appendText(arguments[1].toUpperCase(), ChatColor.GRAY)
                .appendText(".").send();

        // Saves the modules.
        module.save();
    }
}