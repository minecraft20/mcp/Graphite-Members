package me.valkyrie.graphite.impl.command;

import me.valkyrie.graphite.api.command.Command;
import me.valkyrie.graphite.api.command.CommandManifest;
import me.valkyrie.graphite.impl.Graphite;

/**
 * Command that handles changing the client's prefix.
 *
 * @author Camber.
 * @since 4/25/2017.
 */
@CommandManifest(label = "Prefix")
public class Prefix extends Command {

    /**
     * Sets the client's prefix.
     */
    @Override
    public void execute(String[] arguments, String raw){

        // If there is only one argument.
        if(arguments.length == 1){

            // If the argument only has one character.
            if(arguments[0].length() == 1){

                // Sets the prefix.
                Graphite.INSTANCE.getCommandManager().setPrefix(arguments[0].charAt(0));

                // Displays that prefix has changed.
                this.clientChatMsg().appendTextF("Client prefix has been set to \2477%s\247f.", arguments[0]).send();

                // Saves the command manager.
                Graphite.INSTANCE.getCommandManager().save();
            }
        } else

            // Prints the current prefix.
            this.clientChatMsg().appendTextF("Client prefix is \2477%s\247f.", Graphite.INSTANCE.getCommandManager().getPrefix());
    }
}