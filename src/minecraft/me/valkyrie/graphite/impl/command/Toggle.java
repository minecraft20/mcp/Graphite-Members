package me.valkyrie.graphite.impl.command;

import me.valkyrie.graphite.api.command.Command;
import me.valkyrie.graphite.api.command.CommandManifest;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import me.valkyrie.graphite.impl.Graphite;

/**
 * @author Zeb.
 * @since 4/21/2017
 */
@CommandManifest(label = "Toggle", aliases = {"T"})
public class Toggle extends Command {

    /**
     * Toggles modules that are found.
     */
    @Override
    public void execute(String[] arguments, String raw){
        // Returns if argument lengths aren't correct.
        if (arguments.length != 1){
            // Prints an error.
            this.clientChatMsg().appendText("Invalid arguments, please enter the module name.").send();

            // Returns.
            return;
        }

        // The module entered.
        Module module = Graphite.INSTANCE.getModuleManager().get(arguments[0]);

        if (module == null){
            // Prints an error.
            this.clientChatMsg().appendText("Invalid arguments, please enter the module name.").send();

            // Returns.
            return;
        }

        // Toggle's the module.
        module.toggle();

        // Prints the success message.
        this.clientChatMsg().appendText("Toggled ")
                .appendText(module.getLabel().replaceAll("_", " "), ChatColor.GRAY)
                .appendText(module.isRunning() ? " on." : " off.").send();
    }
}