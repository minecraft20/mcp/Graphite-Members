package me.valkyrie.graphite.impl.command;

import me.valkyrie.graphite.api.command.Command;
import me.valkyrie.graphite.api.command.CommandManifest;
import me.valkyrie.graphite.api.module.CommandModule;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import me.valkyrie.graphite.impl.Graphite;
import sun.security.pkcs11.Secmod;

import java.util.stream.Collectors;

import static me.valkyrie.graphite.api.util.ValkIsAStinkyFart.not;

/**
 * Displays the module list.
 *
 * @author Zeb.
 * @since 5/2/2017
 */
@CommandManifest(label = "Modules", aliases = {"Mods", "M"})
public class Mods extends Command {

    /**
     * Executes the command.
     *
     * @param arguments The command arguments.
     * @param raw       The raw command message.
     */
    @Override
    public void execute(String[] arguments, String raw){
        this.clientChatMsg().appendText(Graphite.INSTANCE.getModuleManager().getMap().values().stream()
                .map(Module::getLabel).collect(Collectors.joining(", ")))
                .appendText(".").send();
    }

}
