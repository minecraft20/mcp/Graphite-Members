package me.valkyrie.graphite.impl.command;

import me.valkyrie.graphite.api.command.Command;
import me.valkyrie.graphite.api.command.CommandManifest;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import me.valkyrie.graphite.impl.Graphite;
import net.minecraft.client.main.Main;
import org.lwjgl.input.Keyboard;

/**
 * SUPER DUPER TEMP DONT EVEN PAY ATTENTION
 *
 * @author Zeb.
 * @since 6/29/2017
 */
@CommandManifest(label = "Login")
public class Login extends Command {

    @Override
    public void execute(String[] arguments, String raw){

        // Returns if arguments are invalid.
        if(arguments.length != 2){
            // Prints error.
            this.clientChatMsg().appendText("Invalid arguments, enter username and password.").send();

            return;
        }

        minecraft.session = Main.authenticate(arguments[0], arguments[1]);
    }
}