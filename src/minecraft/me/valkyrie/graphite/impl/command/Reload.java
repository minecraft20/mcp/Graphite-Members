package me.valkyrie.graphite.impl.command;

import me.valkyrie.graphite.api.command.Command;
import me.valkyrie.graphite.api.command.CommandManifest;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import me.valkyrie.graphite.api.util.game.GameTimer;
import me.valkyrie.graphite.impl.Graphite;
import me.valkyrie.graphite.impl.gui.tabgui.Tabgui;

/**
 * @author Zeb.
 * @since 5/16/2017
 */
@CommandManifest(label = "Reload", aliases = {"rl", "rload", "load"})
public class Reload extends Command {

    /**
     * Executes the command.
     *
     * @param arguments The command arguments.
     * @param raw       The raw command message.
     */
    @Override
    public void execute(String[] arguments, String raw){
        clientChatMsg().appendText("Reloading...").send();

        GameTimer gameTimer = new GameTimer();
        Tabgui.INSTANCE.setup();
        clientChatMsg().appendText("Reloaded in ").appendText(gameTimer.getTimePassed() + "", ChatColor.GRAY).appendText(" milliseconds.").send();
    }
}
