package me.valkyrie.graphite.impl.command;

import me.valkyrie.graphite.api.command.Command;
import me.valkyrie.graphite.api.command.CommandManifest;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import me.valkyrie.graphite.impl.Graphite;

/**
 * Command used to set the visibility state of modules.
 *
 * @author Zeb.
 * @since 5/3/2017
 */
@CommandManifest(label = "Visible", aliases = {"Vis", "V"})
public class Visible extends Command {

    /**
     * Executes the command.
     *
     * @param arguments The command arguments.
     * @param raw       The raw command message.
     */
    @Override
    public void execute(String[] arguments, String raw){

        // Returns if arguments are invalid.
        if (arguments.length != 1 && arguments.length != 2){
            // Sends error message.
            clientChatMsg().appendText("Invalid arguments, please enter module name.").send();

            // Returns.
            return;
        }

        // Gets the module.
        Module module = Graphite.INSTANCE.getModuleManager().get(arguments[0]);

        // Checks if module is valid.
        if (module == null){
            // Sends error message.
            clientChatMsg().appendText("Module ").appendText(arguments[0], ChatColor.GRAY).appendText(" is not found.").send();

            // Returns.
            return;
        }

        // Handles arguments.
        if (arguments.length == 2){

            // Checks arguments.
            if (arguments[1].equalsIgnoreCase("true") || arguments[1].equalsIgnoreCase("false")){
                // Sets module visibility.
                module.setVisible(arguments[1].equalsIgnoreCase("true"));

                // Tells user the module visibility.
                clientChatMsg()
                        .appendText("Set ")
                        .appendText(module.getLabel().replaceAll("_", " ") + "'s ", ChatColor.GRAY)
                        .appendText("visibility to ")
                        .appendText(module.isVisible() + "", ChatColor.GRAY)
                        .appendText(".").send();
            } else
                clientChatMsg().appendText("Invalid arguments, please enter a true or false value.").send();

        } else
            // Tells user the module visibility.
            clientChatMsg()
                    .appendText(module.getLabel().replaceAll("_", " ") + "'s ", ChatColor.GRAY)
                    .appendText("visibility is ")
                    .appendText(module.isVisible() + "", ChatColor.GRAY)
                    .appendText(".").send();

    }

}
