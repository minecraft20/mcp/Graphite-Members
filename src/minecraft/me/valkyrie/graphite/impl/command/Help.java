package me.valkyrie.graphite.impl.command;

import me.valkyrie.graphite.api.command.Command;
import me.valkyrie.graphite.api.command.CommandManifest;
import me.valkyrie.graphite.api.module.CommandModule;
import me.valkyrie.graphite.api.util.chat.ChatBuilder;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import me.valkyrie.graphite.impl.Graphite;
import static me.valkyrie.graphite.api.util.ValkIsAStinkyFart.not;

import java.util.stream.Collectors;

/**
 * Displays the command list.
 * 
 * ps. ur gay ^_^
 * pss. fuk u
 *
 * @author catgorl
 * @since 6/19/17
 */
@CommandManifest(label = "Help", aliases = { "commands", "coms" })
public final class Help extends Command {

	@Override
	public void execute(String[] arguments, String raw) {
		this.clientChatMsg().appendText(Graphite.INSTANCE.getCommandManager().getContent().stream()
				.filter(not(CommandModule.class::isInstance)).map(Command::getLabel).collect(Collectors.joining(", ")))
				.appendText(".").send();
	}
}