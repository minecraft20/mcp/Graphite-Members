package me.valkyrie.graphite.impl.command;

import me.valkyrie.graphite.api.command.Command;
import me.valkyrie.graphite.api.command.CommandManifest;
import me.valkyrie.graphite.api.helper.IHelper;
import me.valkyrie.graphite.api.helper.ProfileHelper;
import me.valkyrie.graphite.api.management.FriendManager;
import me.valkyrie.graphite.api.module.Module;
import me.valkyrie.graphite.api.util.chat.ChatColor;
import me.valkyrie.graphite.impl.Graphite;
import net.minecraft.client.network.NetworkPlayerInfo;

import java.util.Optional;
import java.util.UUID;

/**
 * Handles friend interactions in the client.
 *
 * @author Zeb.
 * @since 4/25/2017.
 */
@CommandManifest(label = "Friend", aliases = {"f"})
public class Friend extends Command implements IHelper {

    /**
     * Handles friend interactions in the client.
     */
    @Override
    public void execute(String[] arguments, String raw){
        // Checks arguments.
        if(arguments.length < 2 || arguments.length > 3){
            // Prints an error.
            this.clientChatMsg().appendText("Invalid arguments, sub-command and name.").send();

            // Returns due to invalid arguments.
            return;
        }

        // Handles sub-commands.
        switch(arguments[0].toLowerCase()){
            case "a":
            case "add":

                // Adds the friend.
                new AddFriendThread(arguments[1], arguments.length == 3 ? arguments[2] : arguments[1]).start();
                break;
            case "r":
            case "rem":
            case "delete":
            case "del":
            case "remove":

                // Removes the friend.
                new RemoveFriendThread(arguments[1]).start();
        }
    }

    /**
     * Adds the friend with the given name if
     */
    class AddFriendThread extends Thread {

        /**
         * The name of the friend to be added.
         */
        private String name;

        /**
         * The nickname to be given to the friend.
         */
        private String nickname;

        AddFriendThread(String name, String nickname){
            this.name = name;
            this.nickname = nickname;
        }

        @Override
        public void run(){
            // Return if the friend already exists.
            if(Graphite.INSTANCE.getFriendManager().get(nickname).isPresent()){

                // Tells the user the player is already a friend.
                clientChatMsg().appendText("Player ").appendText(nickname, ChatColor.GRAY).appendText(" is already a friend.").send();

                return;
            } else{
                // The uuid of the player.
                UUID uuid = ProfileHelper.INSTANCE.getUUID(name);

                // If no player has that name.
                if(uuid == null){
                    // Returns because uuid is null.
                    return;
                }

                // Return if the friend already exists.
                if(Graphite.INSTANCE.getFriendManager().get(uuid).isPresent()){

                    // Tells the user the player is already a friend.
                    clientChatMsg().appendText("Player ").appendText(nickname, ChatColor.GRAY).appendText(" is already a friend.").send();

                    return;
                }
            }

            // Checks if the user is in the game.
            if(minecraft.getNetHandler().func_175104_a(name) != null){

                // The player with the given name.
                NetworkPlayerInfo player = minecraft.getNetHandler().func_175104_a(name);

                // Return if the friend already exists.
                if(Graphite.INSTANCE.getFriendManager().get(player.func_178845_a().getId()).isPresent()){

                    // Tells the user the player is already a friend.
                    clientChatMsg().appendText("Player ").appendText(nickname, ChatColor.GRAY).appendText(" is already a friend.").send();

                    return;
                }

                // Adds the player to the friend manager.
                Graphite.INSTANCE.getFriendManager().add(player.func_178845_a().getId(), nickname);

                // Shows the friend add message.
                clientChatMsg().appendText("Added ").appendText(nickname, ChatColor.GRAY).appendText(" as a friend.").send();
            } else{
                // Shows the friend add fetching message.
                clientChatMsg().appendText("Player ").appendText(name, ChatColor.GRAY).appendText(" is not in the server, fetching id...").send();

                // The uuid of the player.
                UUID uuid = ProfileHelper.INSTANCE.getUUID(name);

                // If no player has that name.
                if(uuid == null){

                    // Tells the user that the player does not exist..
                    clientChatMsg().appendText("Player ").appendText(name, ChatColor.GRAY).appendText(" does not exist.").send();

                    // Returns because uuid is null.
                    return;
                }

                // Return if the friend already exists.
                if(Graphite.INSTANCE.getFriendManager().get(uuid).isPresent()){

                    // Tells the user the player is already a friend.
                    clientChatMsg().appendText("Player ").appendText(nickname, ChatColor.GRAY).appendText(" is already a friend.").send();

                    return;
                }

                // Adds the player to the friend manager.
                Graphite.INSTANCE.getFriendManager().add(uuid, nickname);

                // Shows the friend add message.
                clientChatMsg().appendText("Added ").appendText(nickname, ChatColor.GRAY).appendText(" as a friend.").send();
            }
        }

    }

    /**
     * Adds the friend with the given name if
     */
    class RemoveFriendThread extends Thread {

        /**
         * The name of the friend to be added.
         */
        private String name;

        RemoveFriendThread(String name){
            this.name = name;
        }

        @Override
        public void run(){
            // Removes the friend if they can be easily found by nickname.
            if(Graphite.INSTANCE.getFriendManager().get(name).isPresent()){

                // The friend being removed.
                me.valkyrie.graphite.api.util.Friend friend = Graphite.INSTANCE.getFriendManager().get(name).get();

                // Removes the friend.
                Graphite.INSTANCE.getFriendManager().getFriendList().remove(friend);

                // Saves the friend manager.
                Graphite.INSTANCE.getFriendManager().save();

                // Shows the friend add message.
                clientChatMsg().appendText("Removed ").appendText(name, ChatColor.GRAY).appendText(" as a friend.").send();

                return;
            }

            // Checks if the user is in the game.
            if(minecraft.getNetHandler().func_175104_a(name) != null){

                // The player with the given name.
                NetworkPlayerInfo player = minecraft.getNetHandler().func_175104_a(name);

                // The friend being removed.
                me.valkyrie.graphite.api.util.Friend friend = Graphite.INSTANCE.getFriendManager().get(player.func_178845_a().getId()).get();

                // Removes the friend.
                Graphite.INSTANCE.getFriendManager().getFriendList().remove(friend);

                // Saves the friend manager.
                Graphite.INSTANCE.getFriendManager().save();

                // Shows the friend add message.
                clientChatMsg().appendText("Removed ").appendText(name, ChatColor.GRAY).appendText(" as a friend.").send();
            } else{

                // Shows the friend add fetching message.
                clientChatMsg().appendText("Player ").appendText(name, ChatColor.GRAY).appendText(" is not in the server, fetching id...").send();

                // The uuid of the player.
                UUID uuid = ProfileHelper.INSTANCE.getUUID(name);

                // If no player has that name.
                if(uuid == null){

                    // Tells the user that the player does not exist.
                    clientChatMsg().appendText("Player ").appendText(name, ChatColor.GRAY).appendText(" does not exist.").send();

                    // Returns because uuid is null.
                    return;
                }

                // The friend being removed.
                Optional<me.valkyrie.graphite.api.util.Friend> friend = Graphite.INSTANCE.getFriendManager().get(uuid);

                // If the player isn't a friend.
                if(!friend.isPresent()){

                    // Tells the user that the  player isn't a friend.
                    clientChatMsg().appendText("Player ").appendText(name, ChatColor.GRAY).appendText(" is not a friend").send();

                    // Returns because player isn't a friend.
                    return;
                }

                // Removes the friend.
                Graphite.INSTANCE.getFriendManager().getFriendList().remove(friend.get());

                // Saves the friend manager.
                Graphite.INSTANCE.getFriendManager().save();

                // Shows the friend add message.
                clientChatMsg().appendText("Removed ").appendText(name, ChatColor.GRAY).appendText(" as a friend.").send();
            }
        }

    }

}