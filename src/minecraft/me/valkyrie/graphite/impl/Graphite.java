package me.valkyrie.graphite.impl;

import java.io.File;

import me.valkyrie.graphite.api.event.EventFactory;
import me.valkyrie.graphite.api.event.EventManager;
import me.valkyrie.graphite.api.gui.WrappedScreen;
import me.valkyrie.graphite.api.helper.AntibotHelper;
import me.valkyrie.graphite.api.management.CommandManager;
import me.valkyrie.graphite.api.management.FriendManager;
import me.valkyrie.graphite.api.management.ModuleManager;
import me.valkyrie.graphite.api.management.PluginManager;
import me.valkyrie.graphite.impl.event.game.GuiChangeEvent;
import me.valkyrie.graphite.impl.gui.GameOverlay;
import me.valkyrie.graphite.impl.gui.screens.GraphiteScreen;
import me.valkyrie.graphite.impl.gui.tabgui.Tabgui;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiMainMenu;

/**
 * @author Zeb.
 * @since 5/17/2017.
 */
public enum Graphite {

    /**
     * The instance of the graphite client.
     */
    INSTANCE;

    /**
     * The version of graphite.
     */
    public final String VERSION = "0.7.2";

    /**
     * The directory where the client's data is stored.
     */
    private File directory;

    /**
     * The client's module manager.
     */
    private ModuleManager moduleManager;

    /**
     * The client's command manager.
     */
    private CommandManager commandManager;

    /**
     * The client's friend manager.
     */
    private FriendManager friendManager;

    /**
     * The client's plugin manager.
     */
    private PluginManager pluginManager;

    /**
     * The main menu.
     */
    private WrappedScreen mainMenu;

    /**
     * Initiates the client.
     *
     * @param gameDirectory The game's running directory.
     */
    public void initiate(File gameDirectory){

        // Sets the client directory.
        this.directory = new File(gameDirectory + File.separator + "graphite" + File.separator);

        // Attempts to make the client directory.
        if(!directory.exists())
            if(!directory.mkdirs())
                System.exit(0);

        // Sets the command manager.
        this.commandManager = new CommandManager(directory);

        // Sets the module manager.
        this.moduleManager = new ModuleManager(directory);

        // Sets the friend manager.
        this.friendManager = new FriendManager(directory);

        // Sets the plugin manager.
        this.pluginManager = new PluginManager(directory);

        // Registers all the modules.
        this.moduleManager.register();
        this.moduleManager.setup();

        // Sets up the friend manager.
        this.friendManager.setup();

        Tabgui.INSTANCE.init(directory);

        // Register's hud overlay.
        EventManager.INSTANCE.register(new GameOverlay());

        // Sets the main menu.
        this.mainMenu = new WrappedScreen(new GraphiteScreen());

        EventManager.INSTANCE.register(EventFactory.form(GuiChangeEvent.class, (GuiChangeEvent event) -> {

            if(event.getGuiScreen() instanceof GuiMainMenu)
                event.setGuiScreen(mainMenu);

        }));

        Minecraft.getMinecraft().displayGuiScreen(mainMenu);

        AntibotHelper.init();
    }

    /**
     * @return the client's running directory.
     */
    public File getDirectory(){
        return directory;
    }

    /**
     * @return the client's main menu.
     */
    public WrappedScreen getMainMenu(){
        return mainMenu;
    }

    /**
     * @return the client's module manager.
     */
    public ModuleManager getModuleManager(){
        return moduleManager;
    }

    /**
     * @return the client's command manager.
     */
    public CommandManager getCommandManager(){
        return commandManager;
    }

    /**
     * @return the client's friend manager.
     */
    public FriendManager getFriendManager(){
        return friendManager;
    }

    /**
     * @return the client's plugin manager.
     */
    public PluginManager getPluginManager(){
        return pluginManager;
    }

}
