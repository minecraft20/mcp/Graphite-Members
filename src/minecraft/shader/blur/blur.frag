#version 120

// Sampler, contains the texture of the Entity FBO.
uniform sampler2D DiffuseSamper;

// Contains the size of one texel.
uniform vec2 TexelSize;

// The radius of the blur.
uniform int radius;

void main() {
    vec4 blurred = vec4(0.0);
    float totalStrength = 0.0;
    float totalAlpha = 0.0;
    float totalSamples = 0.0;
    int r = radius;
    for(int x = -r; x <= r; x++){
        for(int y = -r; y <= r; y++){
            // The color at the offset texel.
            vec4 s = texture2D(DiffuseSamper, gl_TexCoord[0].st + vec2(x*TexelSize.x, y*TexelSize.y));

            // Accumulate average alpha
            totalAlpha = totalAlpha + s.a;

            // Accumulate smoothed blur
            float strength = 1.0 - abs(sqrt((x*x) + (y*y)) / radius);
            strength *= strength;

            if(s.r == 0 && s.g == 0 && s.b == 0){
                blurred = blurred + s*strength;
            }
            totalSamples = totalSamples + (1.0 * strength);
        }
    }
    gl_FragColor = vec4(blurred.rgb / (totalSamples), totalAlpha / (totalSamples * totalSamples));
}
