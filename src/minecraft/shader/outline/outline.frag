#version 120

// Sampler, contains the texture of the Entity FBO
uniform sampler2D DiffuseSamper;

// Contains the size of one texel
uniform vec2 TexelSize;

// The width of the outline.
uniform int width;

// If the outlin should be glowy. (Currently unused)
uniform bool glow;

/**
 *   Scans for fragments that can be outlined.
 */
bool outline(){
    // Color of the texel at this fragment
    vec4 centerCol = texture2D(DiffuseSamper, gl_TexCoord[0].st);

    // Returns false if the fragment is full alpha.
    if(centerCol.a > 0.0F){
        gl_FragColor = vec4(0, 0, 0, 0);
        return false;
    }

    float closest = width * 1.0F + 1.0F;
    vec3 rgb = vec3(0,0,0);

    // Loops through a square with the length of 2width
    for(int x = -width; x <= width; x++){
        for(int y = -width; y <= width; y++){
            // The color at the offset texel.
            vec4 col = texture2D(DiffuseSamper, gl_TexCoord[0].st + vec2(x*TexelSize.x, y*TexelSize.y));

            // Returns true if there is a solid fragment.
            if(col.a > 0){
                if(glow){
                    float currentDist = sqrt(x*x*1.0f + y*y*1.0f);

                    if(currentDist < closest){
                        closest = currentDist;
                    }

                    rgb = vec3(col.r , col.g, col.b);
                }else{
                    // Sets the color to full alpha.
                    gl_FragColor = vec4(col.rgb, 1);

                    return true;
                }
            }
        }
    }

    // Sets the color to no alpha.
    gl_FragColor = vec4(rgb.r, rgb.g, rgb.b, max(0, ((width*1.0F) - (closest - 1)) / (width*1.0F)));

    // Return false if nothing is found.
    return false;
}

/**
 * The main method.
 */
void main(){
    outline();
}
