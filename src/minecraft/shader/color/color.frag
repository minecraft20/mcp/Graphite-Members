#version 120

// The color of the outline.
uniform vec3 color;

void main() {
	gl_FragColor = vec4(color, 1);
}
